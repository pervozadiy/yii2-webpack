var optionVit = {
    red : {
        readOnly :true,
        width : 120,
        height: 120,
        fgColor: "#D42501",
        inputColor: "#D42501"
    },
    orange : {
        readOnly :true,
        width : 120,
        height: 120,
        fgColor: "#ebac00",
        inputColor: "#ebac00"
    },
    yellow : {
        readOnly :true,
        width : 120,
        height: 120,
        fgColor: "#D9CF02",
        inputColor: "#D9CF02"
    },
    green : {
        readOnly :true,
        width : 120,
        height: 120,
        fgColor: "#23970C",
        inputColor: "#23970C"
    }
};

function drawVitKnobs() {

    $('.content').find('.diagramVitamin').each(function () {
        let bl = $(this);

        let vit = bl.data('title');
        let val = bl.data('value');
        let color = bl.data('color');

        bl.append(
            '<input type="text" value="' + val +'" class="vitamin vitamin' + vit + '">' +
            '<p class="text-center">' + vit + '</p>'
        );

        let knobInput = bl.find('input');

        knobInput.knob(optionVit[color]);

        $({value: 0}).animate(
            {value: val},
            {
                duration: 1000,
                easing:'swing',
                step: function()
                {
                    knobInput.val(this.value).trigger('change');
                },
                complete: function() {
                    knobInput.val(val).trigger('change');
                }
            });

    });

}
