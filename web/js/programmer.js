var itemsComplexList = [];
var tableNewFrequency;

$('.content').on('change','.headMenuProgram',function () {
    let itemMenu = $(this).val();

    var url = '/programmer/get-items-menu-program';
    if(itemMenu == 5327){
        itemMenu = '0';
        url = '/programmer/get-items-menu-program-personal';
    } else if(itemMenu == 5343){
        itemMenu = '0';
        url = '/programmer/get-items-menu-program-personal-trash';
    }

    $('#treeProgrammator').data({url:url});

    if(itemMenu != ''){
        getMenuPrograms(itemMenu,url);

    } else {
        $('#treeProgrammator').html('');
    }
});

$('.content').on('tree.dblclick','#treeProgrammator',function(event) {
    if(event.node.dblclick){
        if(event.node.paid == true && event.node.buy == false){
            modalAlert(
                'danger',
                t_js('attention'),
                '<p>'+t_js('this_complex_was_compiled_by_developer_')+'</p>' +
                '<p>'+t_js('price_complex_0.5_purchase_out_once')+'</p>' +
                '<p>'+t_js('want_to_buy_it')+'</p>',
                '<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">'+t_js('no')+'</button>' +
                '<button type="button" class="btn btn-outline pull-right btnSuccessBuyProgram" data-id="'+event.node.id+'">'+t_js('yes')+'</button>'
            );
        } else {
            sendProgramInStorageCompositionComplex(event.node.id,event.node.owner);
        }
    }
});


$('#modalAlert').on('click','.btnSuccessBuyProgram',function () {
    let programId = $(this).data('id');

    $.ajax({
        url: '/programmer/buy-program',
        method: 'POST',
        data: {
            id: programId
        },
        success: function (msg) {
            modalAlert(msg.state,msg.title,msg.content,msg.footer);

            if(msg.state === 'success'){
                sendProgramInStorageCompositionComplex(programId,'company');

                getBalance();
            }

        }
    });
});

function getMenuPrograms(itemMenu,url) {

    $.ajax({
        url: url,
        method: 'GET',
        data: {
            node: itemMenu,
        },
        success: function (msg) {
            $('#treeProgrammator').tree("destroy");
            $('#treeProgrammator').tree({
                data: msg,
                usecontextmenu: true,
                onCreateLi: function(node, $li) {
                    if (node.buy == true) {
                        var $title = $li.find('.jqtree-title');
                        $title.addClass('font-bold');
                    }
                },
                autoOpen: false,
                dragAndDrop: false,
                closedIcon: '+',
                openedIcon: '-'
            });

            initContextmenu(itemMenu,url)
        }
    });
}

$('.content').on('tree.contextmenu','#treeProgrammator',function(event) {
        let node = event.node;

        $("#selectorRightClickProgramPersonal>li.editProgram" ).removeClass('hidden');

        if (node.menu_storage == false){
            $( "#selectorRightClickProgramPersonal>li.editProgram" ).addClass('hidden');
        }
    }
);

function initContextmenu (itemMenu,url) {
    if(url == '/programmer/get-items-menu-program-personal') {


        $('#treeProgrammator').jqTreeContextMenu($('#selectorRightClickProgramPersonal'), {
            "sendTrash": function (node) {
                sendProgramPersonalToTrash(node.id,itemMenu,url);
            },
            "editProgram": function (node) {
                editPersonalProgram(node.id);
            },
        });
    } if(url == '/programmer/get-items-menu-program-personal-trash') {
        $('#treeProgrammator').jqTreeContextMenu($('#selectorRightClickProgramPersonalTrash'), {
            "returnToPersonal": function (node) {
                returnProgramToPersonal(node.id,itemMenu,url);
            }
        });
    }
}

function editPersonalProgram(id){
    let blEdit = $('.content').find('.blAddNewProgram')

    $.ajax({
        url: '/programmer/get-full-info-personal-program',
        method: 'POST',
        data: {
            id: id,
        },
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {
            blEdit.find('input[name="newProgram[id]"]').val(msg.id)
            blEdit.find('input[name="newProgram[your_name_in_program]"]').val(msg.title)
            blEdit.find('input[name="newProgram[name_displayed_in_device]"]').val(msg.title_device)
            blEdit.find('select[name="newProgram[group]"]').val(msg.pid == 0 ? 'personal' : msg.pid)

            tableNewFrequency.rows().remove().draw();
            for(let itemF of msg.relationFrequencies){
                tableNewFrequency.row.add( [
                    tableNewFrequency.data().length + 1,
                    itemF.min_frequency,
                    itemF.max_frequency,
                    itemF.duration,
                    t_js('sec'),
                    itemF.antenna
                ] ).draw( false );
            }

        }
    });
}

function sendProgramPersonalToTrash(id,itemMenu,url) {

    $.ajax({
        url: '/programmer/send-program-personal-to-trash',
        method: 'POST',
        data: {
            id: id
        },
        success: function (msg) {
            if(msg.state == 'success') {
                getMenuPrograms(itemMenu, url);
            } else {
                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            }
        }
    });
}

function returnProgramToPersonal(id,itemMenu,url) {

    $.ajax({
        url: '/programmer/return-program-to-personal',
        method: 'POST',
        data: {
            id: id
        },
        success: function (msg) {
            if(msg.state == 'success') {
                getMenuPrograms(itemMenu, url);
            } else {
                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            }
        }
    });
}

function sendProgramInStorageCompositionComplex(programId,owner) {

    let url = '/programmer/get-info-program';
    if(owner == 'user'){
        url = '/programmer/get-info-personal-program';
    }

    $.ajax({
        url: url,
        method: 'POST',
        data: {
            id: programId,
        },
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {

            let num = parseInt($('#tableCompositionComplex').jsGrid('_itemsCount')) + 1;
            let formatTime = formattingTime(msg.info.totalTime);

            $("#tableCompositionComplex")
                .jsGrid("insertItem",
                    {
                        id: msg.info.id,
                        key: msg.info.key,
                        owner: msg.info.owner,
                        frequencies: msg.info.frequencies,

                        totalTime: msg.info.totalTime,
                        time: formatTime.total,

                        num: num,
                        name: msg.info.title,
                        deviation: msg.info.deviation,
                        deviationSec: msg.info.deviationSec,
                        pause:msg.info.pause
                    })
                .done(function() {});

        }
    });
}

function initiateJsGrid() {

    $("#tableCompositionComplex").jsGrid({
        width: "100%",
        height: "240px",

        autoload: true,
        inserting: false,
        editing: true,
        sorting: false,
        confirmDeleting: false,
        //paging: true,

        noDataContent: t_js('there_are_no_programs'),

        rowClass: function(item, itemIndex) {
            return "program-" + itemIndex;
        },
        data: [],
        fields: [
            { name: "id", title: "", type: "text", readOnly: true,  visible: false},
            { name: "key", title: "", type: "text", readOnly: true,  visible: false},
            { name: "owner", title: "", type: "text", readOnly: true,  visible: false},
            { name: "frequencies", title: "", type: "text", readOnly: true,  visible: false},

            { name: "totalTime", title: "", type: "text", readOnly: true,  visible: false},

            { name: "num", title: "№", type: "number", align: "center", readOnly: true,  width: 20},
            { name: "name", title: t_js('program'), type: "text", width: 120, readOnly: true},
            { name: "time", title: t_js('time'), type: "text", align: "center", width: 50, readOnly: true },
            { name: "deviation", title: t_js('deviation') + '%', type: "number", align: "center", width: 50,
                editTemplate: function(val){
                    let result = this.editControl = $("<input>").attr({type:"number",min:0,max:10,step:1});
                    result.val(val);
                    return result;
                },
                validate: function(value, item) {
                    if(value > 10){
                        item.deviationSec = 10
                    } else if(value < 0){
                        item.deviationSec = 0
                    }
                    return true;
                }
            },
            { name: "deviationSec", title: t_js('deviation') + ' ' + t_js('sec'), type: "number", align: "center", width: 50,
                editTemplate: function(val){
                    let result = this.editControl = $("<input>").attr({type:"number",min:0,max:7,step:1});
                    result.val(val);
                    return result;
                },
                validate: function(value, item) {
                    if(value > 7){
                        item.deviationSec = 7
                    } else if(value < 0){
                        item.deviationSec = 0
                    }
                    return true;
                }

            },
            { name: "pause", title: t_js('pause'), type: "number", align: "center", width: 50,
                editTemplate: function(val){
                    var $result = this.editControl = $("<input>").attr({type:"number",min:1,max:300,step:1});
                    $result.val(val);
                    return $result;

                }
            },
            {title: '<input type="checkbox" class="checkboxSelectAll" data-table-id="tableCompositionComplex">', align: "center",width: 20,
                itemTemplate: function(value, item) {
                    return $("<input>").attr("type", "checkbox")
                        .attr("checked", value || item.сheckedRow)
                        .on("change", function() {
                            item.сheckedRow = $(this).is(":checked");
                        });
                }
            }
        ],
        rowClick: function(args) {
            var $row = $(args.event.target).closest("tr");

            $row.addClass("selected-row").data({'ind':args.itemIndex});

            if(this._editingRow) {
                this.updateItem().done($.proxy(function() {
                    this.editing && this.editItem($row);
                }, this));
                return;
            }

            this.editing && this.editItem($row);
        },

        onRefreshed: function(args) {

            let $gridData = $("#tableCompositionComplex .jsgrid-grid-body tbody");

            $gridData.sortable({
                update: function(e, ui) {
                    // array of indexes
                    let programIndexRegExp = /\s*program-(\d+)\s*/;
                    let indexes = $.map($gridData.sortable("toArray", { attribute: "class" }), function(classes) {
                        return programIndexRegExp.exec(classes)[1];
                    });

                    // arrays of items
                    let items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });

                    items.forEach(function callback(currentValue, index, array) {
                        currentValue.num = index + 1;
                        args.grid.updateItem(currentValue);
                    });

                    $("#tableCompositionComplex").jsGrid("option", "data", []);
                    items.forEach(function(item) {
                        $("#tableCompositionComplex")
                            .jsGrid("insertItem",item)
                            .done(function() {
                                $("#tableComplexRecording").jsGrid('refresh');
                            });
                    });

                }
            });

            let items = args.grid.option("data");

            let totalTime = 0;
            items.forEach(function(item) {
                totalTime += item.totalTime;
            });

            let formatTime = formattingTime(totalTime);

            let blTime = $('.content').find('.compositionComplexTotalTime');

            blTime.data({'total-time':totalTime});

            if(formatTime.h > 0){
                blTime.find('.hr').removeClass('onlyHide');
            } else if(! blTime.find('.hr').hasClass('onlyHide')){
                blTime.find('.hr').addClass('onlyHide');
            }

            blTime.find('.hr span').text(formatTime.h);
            blTime.find('.min span').text(formatTime.m);
            blTime.find('.sec span').text(formatTime.s);
        }
    });


    $("#tableComplexRecording").jsGrid({
        width: "100%",
        height: "280px",

        inserting: false,
        editing: true,
        sorting: false,
        confirmDeleting: false,
        //paging: true,

        noDataContent: t_js('there_are_no_complexes'),

        rowClass: function(item, itemIndex) {
            return "complex-" + itemIndex;
        },

        data: [],

        fields: [

            { name: "infoProgram", title: "", type: "text", readOnly: true,  visible: false},

            { name: "totalTime", title: "", type: "text", readOnly: true,  visible: false},

            { name: "num", title: "№", type: "number", align: "center", readOnly: true,  width: 20},
            { name: "name", title: t_js('complex'), type: "text", width: 120, readOnly: true},
            { name: "time", title: t_js('time'), type: "text", readOnly: true, align: "center", width: 50 },
            { name: "cycles", title: t_js('cycles'), type: "number", align: "center", width: 50,
                editTemplate: function(val){

                    let ind = $("#tableComplexRecording").find('table tr.selected-row').data('ind');

                    let grid = this._grid;

                    let $result = this.editControl = $("<input>").attr({type:"number",min:1,max:10,step:1,onkeydown:'return false',class:'itemCycle'});
                    $result.val(val);

                    $result.on("change", function() {

                        let cycles = parseFloat($result.val()) ;
                        let timeCoef = parseInt(grid.option("fields")[6].editControl.val());

                        let totalTime = Math.round(grid.data[ind].totalTime * cycles * timeCoef);

                        let formatTime = formattingTime(totalTime);

                        grid.data[ind].time = formatTime.total;
                        grid.option("fields")[4].editControl.val(formatTime.total);
                        grid.data[ind].cycles = cycles;

                        calculationTimeComplexRecording();
                    });

                    return $result;
                }
            },
            { name: "timeCoef", title: t_js('coefficient_time'), type: "text", align: "center", width: 50,
                editTemplate: function(val){

                    let ind = $("#tableComplexRecording").find('table tr.selected-row').data('ind');

                    let grid = this._grid;

                    let $result = this.editControl = $("<input>").attr({type:"number",min:0.1,max:3,step:0.1});
                    $result.val(val);

                    $result.on("change", function() {

                        let timeCoef = parseFloat($result.val());
                        let cycles = parseInt(grid.option("fields")[5].editControl.val());

                        let totalTime = Math.round(grid.data[ind].totalTime * cycles * timeCoef);

                        let formatTime = formattingTime(totalTime);

                        grid.data[ind].time = formatTime.total;
                        grid.option("fields")[4].editControl.val(formatTime.total);
                        grid.data[ind].timeCoef = timeCoef;

                        calculationTimeComplexRecording();
                    });

                    return $result;
                }
            },
            {title: '<input type="checkbox" class="checkboxSelectAll" data-table-id="tableComplexRecording">', align: "center",width: 20,
                itemTemplate: function(value, item) {
                    return $("<input>").attr("type", "checkbox")
                        .attr("checked", value || item.сheckedRow)
                        .on("change", function() {
                            item.сheckedRow = $(this).is(":checked");
                        });
                }
            }

        ],

        rowClick: function(args) {
            let $row = $(args.event.target).closest("tr");

            $row.addClass("selected-row").data({'ind':args.itemIndex});

            if(this._editingRow) {
                this.updateItem().done($.proxy(function() {
                    this.editing && this.editItem($row);
                }, this));
                return;
            }

            this.editing && this.editItem($row);
        },

        onRefreshed: function(args) {
            let $gridData = $("#tableComplexRecording .jsgrid-grid-body tbody");

            $gridData.sortable({
                update: function(e, ui) {
                    // array of indexes
                    let complexIndexRegExp = /\s*complex-(\d+)\s*/;
                    let indexes = $.map($gridData.sortable("toArray", { attribute: "class" }), function(classes) {
                        let ind = complexIndexRegExp.exec(classes);
                        return ind[1];
                    });
                    // arrays of items
                    let items = $.map($gridData.find("tr"), function(row) {
                        return $(row).data("JSGridItem");
                    });

                    items.forEach(function callback(currentValue, index, array) {
                        currentValue.num = index + 1;
                        args.grid.updateItem(currentValue);
                    });
                }
            });

            calculationTimeComplexRecording();
        }
    });
}

$('.content').on('click','.deleteProgramCompositionComplex',function () {

    deleteItemsInTable("#tableCompositionComplex");

});

$('.content').on('click','.sortProgramCompositionComplex',function () {
    let sortType = $(this).data('sort');

    sortItemInTable("#tableCompositionComplex",sortType)
});

$('.content').on('click','.sortProgramComplexRecording',function () {
    let sortType = $(this).data('sort');

    sortItemInTable("#tableComplexRecording",sortType)
});



$('.content').on('click','.deleteProgramComplexRecording',function () {

    deleteItemsInTable("#tableComplexRecording");

});

function deleteProgramComplexRecording(ind) {
    let dataTable = $("#tableComplexRecording").jsGrid("option", "data");

    if(typeof(ind) !== "undefined"){
        $("#tableComplexRecording").jsGrid("deleteItem",dataTable[ind]);
    }

    updateIndexInTable("#tableComplexRecording");

}

$('.content').on('click','.editProgramComplexRecording',function () {
    let ind = $("#tableComplexRecording").find('table tr.selected-row').data('ind');

    if(typeof(ind) !== "undefined"){

        $("#tableCompositionComplex").jsGrid("option", "data", []);

        let rowData = $("#tableComplexRecording").jsGrid("option", "data");

        $('.content').find('.newComplexName').val(rowData[ind]['name']);


        rowData[ind]['infoProgram'].forEach(function (item) {
            let formatTime = formattingTime(item.totalTime);

            $("#tableCompositionComplex")
                .jsGrid("insertItem",
                    {
                        id: item.id,
                        key: item.key,
                        owner: ( typeof item.owner == 'undefined' ? '' : item.owner),
                        frequencies: item.frequencies,

                        totalTime:item.totalTime,
                        time: formatTime.total,

                        num: item.num,
                        name: item.name,
                        deviation: item.deviation,
                        deviationSec: item.deviationSec,
                        pause:item.pause
                    })
                .done(function() {});
        });

        deleteProgramComplexRecording(ind);
    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('complex_not_selected'),
            ''
        );
    }
});

$('.content').on('click','.complexAdd',function () {

    updateDateInTable("#tableCompositionComplex");
    updateIndexInTable("#tableCompositionComplex");

    updateDateInTable("#tableComplexRecording");

    let complexName = $('.content').find('.newComplexName').val();
    let dataCompositionComplex = $("#tableCompositionComplex").jsGrid("option", "data");

    let prepareDataCompositionComplex = [];
    for (var key in dataCompositionComplex) {
        prepareDataCompositionComplex[key] = dataCompositionComplex[key];
    }

    let totalTime = parseInt($('.content').find('.compositionComplexTotalTime').data('total-time'));

    if(complexName == ''){
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('name_complex_not_be_empty'),
            ''
        );

        return false;
    }

    if(prepareDataCompositionComplex.length !== 0){

        let repeatNameCpmplex = 0;
        let items = $("#tableComplexRecording").jsGrid("option", "data");
        items.forEach(function(item) {
            if(complexName === item.name){
                repeatNameCpmplex = 1;
            }
        });

        if(repeatNameCpmplex != 1){
            let num = parseInt($("#tableComplexRecording").jsGrid("_itemsCount")) + 1;
            let formatTime = formattingTime(totalTime);

            $("#tableComplexRecording")
                .jsGrid("insertItem",
                    {
                        infoProgram: prepareDataCompositionComplex,

                        totalTime: totalTime,

                        num: num,
                        name: complexName,
                        time: formatTime.total,
                        cycles: 1,
                        timeCoef: 1
                    })
                .done(function() {
                    // $("#tableCompositionComplex").jsGrid("option", "data", []);
                    $('.content').find('.newComplexName').val('');
                });
        } else {
            modalAlert(
                'danger',
                t_js('attention'),
                t_js('name_complex_already_use'),
                ''
            );
        }
    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('complex_is_not_made'),
            ''
        );
    }

});

$('.content').on('click','.btnFrequencyUpDown',function () {
    fFrequencyUpDown($(this));
});

function fFrequencyUpDown(btn){
    if(btn.hasClass('btn-info')){
        btn.removeClass('btn-info');
        btn.closest('.compositionComplex').find('input[name="frequencyUpDown"]').val('');
    } else {
        btn.closest('.compositionComplex').find('.btnFrequencyUpDown').each(function(){
            $(this).removeClass('btn-info');
        });
        btn.addClass('btn-info');
        btn.closest('.compositionComplex').find('input[name="frequencyUpDown"]').val(btn.data('sort'))
    }
}

$('.content').on('click','.btnReduceTime',function () {

    let reduceNum = $(this).data('num');

    let ind = $("#tableComplexRecording").find('table tr.selected-row').data('ind');

    if(typeof(ind) !== "undefined"){

        let infoComplex = $("#tableComplexRecording").jsGrid("option", "data");

        infoComplex[ind]['timeCoef'] = parseFloat((infoComplex[ind]['timeCoef'] / reduceNum).toFixed(2));

        let totalTime = Math.round(infoComplex[ind]['totalTime'] * infoComplex[ind]['cycles'] * infoComplex[ind]['timeCoef']);
        let formatTime = formattingTime(totalTime);

        infoComplex[ind]['time'] = formatTime.total;

        $("#tableComplexRecording").jsGrid("refresh");


    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('complex_not_selected'),
            ''
        );
    }
});

$('.content').on('click','.getComplexLifeBalance',function(){

    let listDevices = getListConnectTherapeuticDevices();
    if(listDevices.length == 0){
        modalAlert(
            'danger',
            t_js('attention')+'!',
            '<p>'+t_js('device_not_connected')+'</p>',
            ''
        );
        return false;
    } else if(typeof selectedDevice !== 'undefined'){
        setLogger(JSON.stringify({
            action:'get-complex',
            nameDevice:selectedDevice.device,
            method:selectedDevice.device,
            callbackFunction:'getComplexLifeBalance'
        }));
        sendHash({
            action:'get-complex',
            nameDevice:selectedDevice.device,
            method:selectedDevice.device,
            callbackFunction:'getComplexLifeBalance'
        });
        unsetDevice();
    } else if(listDevices.length == 1){
        setLogger(JSON.stringify({
            action:'get-complex',
            nameDevice:listDevices[0].device,
            method:listDevices[0].device,
            callbackFunction:'getComplexLifeBalance'
        }));
        sendHash({
            action:'get-complex',
            nameDevice:listDevices[0].device,
            method:listDevices[0].device,
            callbackFunction:'getComplexLifeBalance'
        });
    } else {
        selectDevice(listDevices,'.getComplexLifeBalance');
    }
});

function getComplexLifeBalance(request) {
    setLogger(request);

    request = JSON.parse(request);

    if(request.error == 'ok'){

        $.ajax({
            url: '/programmer/get-complex-from-life-balance',
            method: 'POST',
            data: {
                infoComplex: request.complex
            },
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function (msg) {

                if(msg.state != 'danger'){

                    $("#tableComplexRecording").jsGrid("option", "data", []);

                    let i = 0;
                    msg.data.forEach(function(item) {

                        let formatTime = formattingTime(item.totalTime);

                        i++;
                        $("#tableComplexRecording")
                            .jsGrid("insertItem",
                                {
                                    infoProgram: item.infoProgram,

                                    totalTime:item.totalTime,
                                    time: formatTime.total,

                                    num: i,
                                    name: item.name,
                                    cycles: item.cycles,
                                    timeCoef: item.timeCoef
                                })
                            .done(function() {
                                $("#tableComplexRecording").jsGrid('refresh');
                            });
                    });
                }

                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            }
        });
    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            request.error,
            ''
        );
    }
}

$('.content').on('click','.eliminateDuplicateFrequencies',function () {

    let ind = $("#tableComplexRecording").find('table tr.selected-row').data('ind');

    if(typeof(ind) !== "undefined"){

        $("#tableComplexRecording").jsGrid("updateItem");

        let infoComplex = $("#tableComplexRecording").jsGrid("option", "data");

        $.ajax({
            url: '/programmer/eliminate-duplicate-frequencies',
            method: 'POST',
            data: {
                infoComplex: JSON.stringify(infoComplex[ind])
            },
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function (msg) {

                let formatTime = formattingTime(msg.totalTime);

                $("#tableComplexRecording")
                    .jsGrid("updateItem", $(".complex-"+ind),{
                        infoProgram: msg.infoProgram,

                        totalTime:  msg.totalTime,
                        time: formatTime.total,

                        num: msg.num,
                        name: msg.name,
                        cycles: msg.cycles,
                        timeCoef: msg.timeCoef
                    }).done(function() {
                    $("#tableComplexRecording").jsGrid('refresh');
                });

            }
        });


    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('complex_not_selected'),
            ''
        );
    }
});

$('.content').on('click','.complexRecordOnDevice',function () {

    updateDateInTable("#tableComplexRecording");

    var data = {};
    data.infoComplex = $('.content').find("#tableComplexRecording").jsGrid("option", "data");

    if(data.infoComplex.length == 0) {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('complex_is_not_made'),
            ''
        );

        return false;
    }
    data.frequencyUpDown = $('.content').find('input[name="frequencyUpDown"]').val();

    let listDevices = getListConnectTherapeuticDevices();
    if(listDevices.length == 0){
        modalAlert(
            'danger',
            t_js('attention')+'!',
            '<p>'+t_js('device_not_connected')+'</p>',
            ''
        );
        return false;
    } else if(typeof selectedDevice !== 'undefined'){
        recordProgrammerComplexOnDevice(selectedDevice,data);
        unsetDevice();
    } else if(listDevices.length == 1){
        recordProgrammerComplexOnDevice(listDevices[0],data);
    } else {
        selectDevice(listDevices,'.complexRecordOnDevice');
    }

});

function selectDevice(listDevices,selector) {

    let blListDevices = '<p><div class="row">';

    for(let item of listDevices){
        blListDevices +=
            `<div class="col-md-4 text-center">
                <a href="javascript:void(0);" 
                     data-device="${item.device}" 
                     data-version="${item.version}" 
                     data-selector="${selector}" 
                     class="selectDevice">
                    
                     <img src="/images/devices/${item.device}-${item.version}.png" alt="">
                     <br>
                    
                     <span>${item.device}</span>
                 </a>
              
            </div>`;
    }

    blListDevices += '</div></p>';

    modalAlert(
        'warning',
        t_js('select_device'),
        blListDevices,
        ''
    );
}

$('#modalAlert').on('click','.selectDevice',function () {
    selectedDevice = {
        device:$(this).data('device'),
        version:$(this).data('version')
    };

    $($(this).data('selector')).trigger("click");

});

function unsetDevice() {
    setTimeout(() => delete selectedDevice, 500);
}

$('.content').on('click','.complexRecordOnDeviceViaCloud',function () {
    updateDateInTable("#tableComplexRecording");

    let data = {};
    data.infoComplex = $("#tableComplexRecording").jsGrid("option", "data");

    if(data.infoComplex.length > 0){

        data.frequencyUpDown = $('.content').find('input[name="frequencyUpDown"]').val();

        setCodeDeviceBalance('recordProgrammerComplexOnCloud',{'dateTime':getDateTimeNow(),'type':'program-complex','complex':data});

    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('complex_is_not_made'),
            ''
        );
    }
});



function recordProgrammerComplexOnDevice(infoDevice,data) {

    let dataRequest = {
        dateTime:getDateTimeNow(),
        infoDevice:infoDevice,
        data:JSON.stringify(data)
    };

    $.ajax({
        url: '/medical-complex/create-from-programmer',
        type: 'POST',
        data: dataRequest,
        beforeSend: function () {
            modalAlert(
                'warning',
                t_js('processing_data'),
                '<div class="height250 paddingTop20"><p><img src="../images/complex-calculation.gif" class="img-responsive"></p></div>',
                ''
            );
        },
        complete: function () {

        },
        success: function (msg) {
            setTimeout(function(){

                if(msg.state != 'danger') {
                    sendHash(msg.complex);
                    setLogger(JSON.stringify(msg.complex));

                    setFlPendingComplexRecording(true);

                } else {
                    modalAlert(msg.state, msg.title, msg.content, msg.footer);
                }

            }, 5000);
        }
    });
}

function recordProgrammerComplexOnCloud(data) {
    var dataRequest = {
        dateTime:getDateTimeNow(),
        data:JSON.stringify(data)
    };

    $.ajax({
        url: '/medical-complex/save-cloud-from-programmer',
        type: 'POST',
        data: dataRequest,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {

        },
        success: function (msg) {
            setTimeout(function(){
                $('#loading_box').hide();

                modalAlert(msg.state, msg.title, msg.content, msg.footer);

            }, 5000);
        }
    });
}

$('.content').on('change','.blAddNewProgram [name="newProgram[group]"]',function () {
    if($(this).val() == 'other'){
        $('.content').find('.newSubGroup').show();
    } else {
        $('.content').find('.newSubGroup').hide();
    }
});

$('.content').on('click','.blAddNewProgram .addNewFrequency',function () {
    let bl = $('.content').find('.blAddNewProgram');

    let frequencyMin = bl.find('[name="newProgram[frequency_min]"]').val();
    let frequencyMax = bl.find('[name="newProgram[frequency_max]"]').val();
    let frequencyTime = parseInt(bl.find('[name="newProgram[time]"]').val());
    let frequencyTimeUnit = bl.find('[name="newProgram[time_unit]"]').val();
    let frequencyAntenna = bl.find('[name="newProgram[antenna]"]').val();

    if(frequencyTimeUnit == 'min'){
        frequencyTime = parseInt(frequencyTime) * 60;
    }

    if(frequencyMin.length > 11 || frequencyMax.length > 11){
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('size_frequency_exceeds_permissible'),
            ''
        );

        return false;
    }

    if(frequencyMin != '' && frequencyTime != ''){
        tableNewFrequency.row.add( [
            tableNewFrequency.data().length + 1,
            frequencyMin,
            frequencyMax,
            frequencyTime,
            t_js('sec'),
            frequencyAntenna
        ] ).draw( false );

        bl.find('[name="newProgram[frequency_min]"]').val('');
        bl.find('[name="newProgram[frequency_max]"]').val('');
        bl.find('[name="newProgram[time]"]').val('');
        bl.find('[name="newProgram[time_unit]"]').val('sec');

    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('data_adding_frequency_is_not_filled'),
            ''
        );
    }

});

$('.content').on('click','.saveNewProgram',function () {
    let bl = $('.content').find('.blAddNewProgram');

    let group = bl.find('[name="newProgram[group]"]').val();
    let subgroup = '';
    if(group == 'other'){
        subgroup = bl.find('[name="newProgram[new_sub_group]"]').val();
    }

    let idProgramm = bl.find('[name="newProgram[id]"]').val();
    let nameProgramm = bl.find('[name="newProgram[your_name_in_program]"]').val();
    let nameProgrammDevice = bl.find('[name="newProgram[name_displayed_in_device]"]').val();

    if(nameProgramm == ''){
        nameProgramm = nameProgrammDevice;
    }

    let listFrequencies = [];
    for (let i = 0;i < tableNewFrequency.data().length;i++) {
        let rowTable = tableNewFrequency.row(i).data();

        listFrequencies.push({
            frequency : {
                min : rowTable[1],
                max : rowTable[2],
            },
            duration : rowTable[3],
            antenna : rowTable[5],
        });
    }

    if(listFrequencies.length != 0 && nameProgrammDevice != ''){
        $.ajax({
            url: '/programmer/save-personal-program',
            method: 'POST',
            data: {
                infoProgram: {
                    id: idProgramm,
                    name_in_program : nameProgramm,
                    name_in_device : nameProgrammDevice,
                    group : group,
                    subgroup :subgroup,
                    listFrequencies :listFrequencies
                }
            },
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function (msg) {
                if (msg.state === 'success') {
                    bl.find('[name="newProgram[id]"],[name="newProgram[your_name_in_program]"],[name="newProgram[name_displayed_in_device]"],[name="newProgram[new_sub_group]"]').val('');

                    tableNewFrequency.rows().remove().draw();

                    bl.find('[name="newProgram[group]"]').val('personal');

                    loadGroupNewProgram();

                    getMenuPrograms('0','/programmer/get-items-menu-program-personal');
                }

                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            }
        });
    } else {
        modalAlert(
            'danger',
            t_js('attention')+'!',
            '<p>'+t_js('program_name_and_frequencies_required_')+'</p>',
            ''
        );
    }

});

$('.content').on('click','.btnActivateCloud,.btnActivateCloud div',function () {
    if($('.content').find('.activateCloud').is(':checked')){
        modalAlert(
            'warning',
            t_js('attention')+'!',
            '<p>'+t_js('are_you_sure_want_disable_cloud_')+'</p>' +
            '<p>'+t_js('point_deleted_complex_')+'</p>' +
            '<p>'+t_js('button_will_be_not_available_')+'</p>' +
            '<p>'+t_js('confirm_deactivation_button_')+'</p>' +
            '<p>'+t_js('service_continue_work_')+'</p>',
            '<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">'+t_js('close')+'</button>' +
            '<button type="button" class="btn btn-outline pull-right btnDisableActivateCloud">'+t_js('disable')+'</button>'
        );
    } else {
        modalAlert(
            'warning',
            t_js('attention')+'!',
            '<p>'+t_js('you_activate_the_cloud_storage_where_all_')+'</p>' +
            '<p>'+t_js('number_stored_complexes_unlimited')+'</p>' +
            '<p>'+t_js('cost_service_50')+'</p>' +
            '<p>'+t_js('button_will_be_available_')+'</p>' +
            '<p>'+t_js('confirm_activation_click_')+'</p>',
            '<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">'+t_js('close')+'</button>' +
            '<button type="button" class="btn btn-outline pull-right btnSuccessActivateCloud">'+t_js('confirm')+'</button>'
        );
    }

});

$('.content').on('click','.btnActivateDoctorsPrograms,.btnActivateDoctorsPrograms div',function () {

    if($('.content').find('.DoctorsPrograms').is(':checked') == false){
        modalAlert(
            'warning',
            t_js('attention')+'!',
            '<p>'+t_js('window_will_open_for_contacting_technical_support_confirm_doctor_')+'</p>',
            '<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">'+t_js('close')+'</button>' +
            '<a href="https://support.businessprocess.biz/?title=4" class="btn btn-outline pull-right transitionInDefaultBrowser">'+t_js('contact_technical_support')+'</a>'
        );
    }

});

$('#modalAlert').on('click','.btnSuccessActivateCloud',function () {

    $(this).attr({disabled:'disabled'});

    $.ajax({
        url: '/device-user/active-cloud-complex',
        method: 'POST',
        data: {fl:1},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            modalAlert(msg.state,msg.title,msg.content,msg.footer);
            if(msg.status != 'danger'){
                $('.content').find('.saveProgramComplexRecording,.openCloudStorage').removeAttr('disabled');
                $('.content').find('.activateCloud').attr({checked:true});
            }
        }
    });
});

$('#modalAlert').on('click','.btnDisableActivateCloud',function () {

    $(this).attr({disabled:'disabled'});

    $.ajax({
        url: '/device-user/active-cloud-complex',
        method: 'POST',
        data: {fl:0},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            modalAlert(msg.state,msg.title,msg.content,msg.footer);
            if(msg.status != 'danger'){
                $('.content').find('.saveProgramComplexRecording,.openCloudStorage').attr({disabled:true});
                $('.content').find('.activateCloud').removeAttr('checked');
            }
        }
    });
});

function loadGroupNewProgram(){
    $.ajax({
        url: '/programmer/get-group-personal-programmes',
        method: 'GET',
        success: function(msg){
            let listGroup = $('.content').find('.groupNewProgram');
            listGroup.html('');

            $.each(msg, function(key, value) {
                listGroup
                    .append($("<option></option>")
                        .attr("value",value.key)
                        .text(value.value));
            });

            listGroup.val('personal');
        }
    });
}

$('.content').on('click','.saveProgramComplexRecording',function () {

    btnDisabled($(this));

    updateDateInTable("#tableComplexRecording");

    let data = {};
    data.infoComplex = $("#tableComplexRecording").jsGrid("option", "data");

    if(data.infoComplex.length > 0){

        data.frequencyUpDown = $('.content').find('input[name="frequencyUpDown"]').val();

        let dataRequest = {
            dateTime:getDateTimeNow(),
            data:JSON.stringify(data)
        };

        $.ajax({
            url: '/programmer/save-cloud-complex',
            type: 'POST',
            data: dataRequest,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function (msg) {

                modalAlert(msg.state, msg.title, msg.content, msg.footer);

            }
        });
    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('complex_is_not_made'),
            ''
        );
    }
});

$('.content').on('click','.openCloudStorage',function () {

    btnDisabled($(this));

    $.ajax({
        url: '/programmer/get-cloud-complex',
        type: 'GET',
        success: function (msg) {
            let templTable =
                '<table id="listCloudComplexes" class="table table-bordered table-striped margin">' +
                '<thead>' +
                '<tr>' +
                '<th>' + t_js('date') + '</th>' +
                '<th>' + t_js('full_name')  + '</th>' +
                '<th>' + t_js('complex') + '</th>' +
                '<th></th>' +
                '<th></th>' +
                '</tr>' +
                '</thead>' +
                '<tbody></tbody>' +
                '</table>';

            modalAlert(
                'warning',
                t_js('complexes_in_cloud'),
                templTable,
                ''
            );

            $('#modalAlert').find('#listCloudComplexes').DataTable({
                'paging'      : true,
                'destroy'     : true,
                'lengthChange': false,
                'searching'   : true,
                'ordering'    : true,
                'info'        : false,
                'autoWidth'   : false,
                'language': {
                    "search": t_js('search'),
                    'paginate': {
                        'previous': t_js('prev'),
                        'next':     t_js('next')
                    }
                },
                'sDom': "<'row'<'col-sm-12'l><'col-sm-12'f>r>t<'row'<'span'i><'col-sm-12'p>>"
            });

            $(document).on('shown.bs.modal', function (e) {
                let table =  $('#modalAlert').find('#listCloudComplexes');

                table.dataTable().api().clear();
                table.dataTable().api().rows.add(msg);
                table.dataTable().api().draw();
            });

        }
    });
});

// get modal success delete complex
$('#modalAlert').on('click','.btnDeleteCloudComplex', function () {
    let complexID = $(this).data('complex');

    modalAlert(
        'warning',
        t_js('attention')+'!',
        '<p>'+t_js('are_you_sure_you_want_delete_complex')+'</p>',
        '<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">'+t_js('close')+'</button>' +
        '<button type="button" class="btn btn-outline pull-right btnSuccessDeleteCloudComplex" data-complex-id = "'+complexID+'">'+t_js('remove')+'</button>'
    );

});

// success delete complex
$('#modalAlert').on('click','.btnSuccessDeleteCloudComplex', function () {
    var complexID = $(this).data('complex-id');

    $.ajax({
        url: '/programmer/soft-delete-complex',
        type: 'POST',
        data: {complexId: complexID},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            modalAlert(msg.state,msg.title,msg.content,msg.footer);
        }
    });
});

$('#modalAlert').on('click','.btnEditCloudComplex',function () {
    var complexID = $(this).data('complex');

    $.ajax({
        url: '/programmer/get-complex-from-cloud',
        type: 'POST',
        data: {complexId: complexID},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            if(typeof msg.infoComplex !== undefined && msg.infoComplex.length > 0){
                var i = parseInt($('#tableCompositionComplex').jsGrid('_itemsCount'));

                $('.content').find('input[name="frequencyUpDown"]').val();

                if(typeof msg.frequencyUpDown !== undefined) {
                    fFrequencyUpDown($('.content').find('.btnFrequencyUpDown[data-sort="'+msg.frequencyUpDown+'"]'));
                }

                msg.infoComplex.forEach(function(item) {

                    let formatTime = formattingTime(item.totalTime);

                    i++;
                    $("#tableComplexRecording")
                        .jsGrid("insertItem",
                            {
                                infoProgram: item.infoProgram,

                                totalTime:item.totalTime,
                                time: formatTime.total,

                                num: i,
                                name: item.name,
                                cycles: item.cycles,
                                timeCoef: item.timeCoef
                            })
                        .done(function() {
                            $("#tableComplexRecording").jsGrid('refresh');
                        });
                })
            }

            $('#modalAlert').modal('hide');
        }
    });
});

$('.content').on('click','.getRecipeFromVegaSelectionFrequencies',function () {
    btnDisabled($(this));

    var recipeNum = $(this).closest('.blVegaRecipeForImport').find('input:checked').val();

    $.ajax({
        url: '/vega-frequencies/get-recipe',
        type: 'POST',
        data: {recipeNum: recipeNum},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            if (msg.state === 'danger') {
                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            } else {
                var bl = $('.content').find('.blAddNewProgram');
                msg.listFrequencies.forEach(function(item) {
                    bl.find('[name="newProgram[listFrequency]"]')
                        .append(
                            '<option value="'+item.frequencyMin+'" data-frequency-min="'+item.frequencyMin+'" data-frequency-max="" data-frequency-time="'+item.frequencyTime+'" data-frequency-antenna="'+item.frequencyAntenna+'">' +
                            item.frequencyMin +
                            '</option>'
                        );
                });
            }
        }
    });
});

$('.content').on('click','.addProgramToComplex',function(){
    let bl = $('.content').find('.blAddNewProgram');

    let nameProgrammDevice = bl.find('[name="newProgram[name_displayed_in_device]"]').val();

    let listFrequencies = [];
    for (let i = 0;i < tableNewFrequency.data().length;i++) {
        let rowTable = tableNewFrequency.row(i).data();

        listFrequencies.push({
            frequency : {
                min : rowTable[1],
                max : rowTable[2],
            },
            duration : rowTable[3],
            antenna : rowTable[5],
        });
    }

    if(listFrequencies.length != 0 && nameProgrammDevice != ''){
        $.ajax({
            url: '/programmer/add-personal-program-to-complex',
            method: 'POST',
            data: {
                infoProgram: {
                    name_in_device : nameProgrammDevice,
                    listFrequencies :listFrequencies
                }
            },
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function (msg) {
                if (msg.state === 'success') {
                    bl.find('[name="newProgram[name_displayed_in_device]"]').val('');

                    tableNewFrequency.rows().remove().draw();

                    let num = parseInt($('#tableCompositionComplex').jsGrid('_itemsCount')) + 1;
                    let formatTime = formattingTime(msg.info.totalTime);

                    $("#tableCompositionComplex")
                        .jsGrid("insertItem",
                            {
                                id: msg.info.id,
                                key: msg.info.key,
                                owner: msg.info.owner,
                                frequencies: msg.info.frequencies,

                                totalTime:msg.info.totalTime,
                                time: formatTime.total,

                                num: num,
                                name: msg.info.title,
                                deviation: msg.info.deviation,
                                deviationSec: msg.info.deviationSec,
                                pause: msg.info.pause
                            })
                        .done(function() {});
                }

                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            }
        });
    } else {
        modalAlert(
            'danger',
            t_js('attention')+'!',
            '<p>'+t_js('program_name_and_frequencies_required_')+'</p>',
            ''
        );
    }
});

$('.content').on('keyup', '.searchProgramsForProgrammer', function () {

    var searchVal = $(this).val();
    var searchTypel = $(this).data('type');

    if (searchVal.length >= 3) {

        $.ajax({
            url: '/programmer/search-programs',
            data: {s:searchVal,type:searchTypel},
            method: 'get',
            success: function (msg) {
                $('#treeProgrammator').tree("destroy");
                $('#treeProgrammator').tree({
                    data: msg,
                    onCreateLi: function(node, $li) {
                        if (node.buy == true) {
                            var $title = $li.find('.jqtree-title');
                            $title.addClass('font-bold');
                        }
                    },
                    autoOpen: false,
                    dragAndDrop: false,
                    closedIcon: '+',
                    openedIcon: '-'
                });
            }
        });


    } else {
        $('#treeProgrammator').tree("destroy");
    }


});

$('.content').on('keyup','#tableComplexRecording',function (e) {
    pressTableComplexRecording(e)
});
$('.content').on('keydown','#tableComplexRecording',function (e) {
    pressTableComplexRecording(e)
});
$('.content').on('keypress','#tableComplexRecording',function (e) {
    pressTableComplexRecording(e)
});

function pressTableComplexRecording(e){

    if (e.keyCode == 27)  {  // Esc
        $("#tableComplexRecording").jsGrid("cancelEdit");
    }

    if (e.keyCode == 13)  {  // Enter
        $("#tableComplexRecording").jsGrid("updateItem");
    }

    if (e.keyCode == 46)  {  // Del
        ind = $("#tableComplexRecording").find('table tr.selected-row').data('ind');
        $("#tableComplexRecording").jsGrid("deleteItem",$(".complex-"+ind));

        var gridData = $("#tableComplexRecording .jsgrid-grid-body tbody");

        var items = $.map(gridData.find("tr"), function(row) {
            return $(row).data("JSGridItem");
        });
        items.forEach(function callback(item, index, array) {
            item.num = index + 1;
            $("#tableComplexRecording").jsGrid("updateItem",item);
        });
    }
    calculationTimeComplexRecording();
}

function calculationTimeComplexRecording() {
    let items = $("#tableComplexRecording").jsGrid("option", "data");

    let totalTime = 0;
    items.forEach(function(item) {
        totalTime += item.totalTime * item.timeCoef * item.cycles;
    });

    let formatTime = formattingTime(totalTime);

    let blTime = $('.content').find('.durationAllComplexTotalTime');

    blTime.data({'total-time':totalTime});

    if(formatTime.h > 0){
        blTime.find('.hr').removeClass('onlyHide');
        blTime.find('.hr span').text(formatTime.h);
    }

    blTime.find('.min span').text(formatTime.m);
    blTime.find('.sec span').text(formatTime.s);
}


$('.content').on('keyup','#tableCompositionComplex',function (e) {
    pressTableCompositionComplex(e)
});
$('.content').on('keydown','#tableCompositionComplex',function (e) {
    pressTableCompositionComplex(e)
});
$('.content').on('keypress','#tableCompositionComplex',function (e) {
    pressTableCompositionComplex(e)
});

function pressTableCompositionComplex(e){
    if (e.keyCode == 27)  {  // Esc
        $("#tableCompositionComplex").jsGrid("cancelEdit");
    }

    if (e.keyCode == 13)  {  // Enter
        $("#tableCompositionComplex").jsGrid("updateItem");
    }

    if (e.keyCode == 46)  {  // Del
        ind = $("#tableCompositionComplex").find('table tr.selected-row').data('ind');
        $("#tableCompositionComplex").jsGrid("deleteItem",$(".program-"+ind));

        let gridData = $("#tableCompositionComplex .jsgrid-grid-body tbody");

        let items = $.map(gridData.find("tr"), function(row) {
            return $(row).data("JSGridItem");
        });

        items.forEach(function callback(item, index, array) {
            item.num = index + 1;
            $("#tableCompositionComplex").jsGrid("updateItem",item);
        });
    }
}

function initiateDataTableProgrammer() {

    $('#listSeanceProgrammator').DataTable({
        'paging'      : true,
        'destroy'     : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : false,
        'info'        : false,
        'autoWidth'   : false,
        'language': {
            'paginate': {
                'previous': 'Назад',
                'next':     'Далее'
            }
        }
    });

    tableNewFrequency = $('#selectNewFrequency').DataTable({
        'paging'      : false,
        'destroy'     : true,
        'lengthChange': false,
        'searching'   : false,
        'info'        : false,
        'select'      : true,
        'autoWidth'   : false,
        "scrollY"     : "160px",
        columnDefs: [
            { orderable: false, className: 'reorder', targets: 0 },
            { orderable: false, targets: '_all' }
        ]
    });
};

$('.content').on( 'click', '#selectNewFrequency tbody tr', function () {
    if ( $(this).hasClass('selected') ) {
        $(this).removeClass('selected');
    }
    else {
        tableNewFrequency.$('tr.selected').removeClass('selected');
        $(this).addClass('selected');
    }
} );

$('.content').on('click','.btnDeleteNewFrequency',function(){

    tableNewFrequency.row('.selected').remove().draw(true);

    for (let i = 0;i < tableNewFrequency.data().length;i++) {
        tableNewFrequency.row(i).data()[0] = i+1;
        tableNewFrequency.$('tr')[i].firstElementChild.innerHTML = i+1;
    }

    tableNewFrequency.page(0).draw(false);
});

$('.content').on('click','.brtNewFrequencyUp',function () {
    // Move the row up
    let tr = tableNewFrequency.$('tr.selected');
    moveRow(tr, 'up');
});

$('.content').on('click','.brtNewFrequencyDown',function () {
    // Move the row down
    let tr = tableNewFrequency.$('tr.selected');
    moveRow(tr, 'down');
});

function moveRow(row, direction) {
    let index = tableNewFrequency.row(row).index();

    let order = -1;
    if (direction === 'down') {
        order = 1;
        if ( index == tableNewFrequency.data().length-1) {
            return;
        }                
    }

    if ( (index == 0) && (direction === 'up')) {
        return;
    }

    let data1 = tableNewFrequency.row(index).data();
    data1[0] =  parseInt(data1[0]) + parseInt(order);

    let data2 = tableNewFrequency.row(index + order).data();
    let data2Index = parseInt(data2[0]) + parseInt(-order);
    data2[0] = data2Index;

    tableNewFrequency.row(index).data(data2);
    tableNewFrequency.row(index + order).data(data1);

    for (let i = 0;i++;i < tableNewFrequency.data().count()) {
        tableNewFrequency.row(i).data()[0] = i;
    }

    tableNewFrequency.page(0).draw(false);

    tableNewFrequency.$('tr.selected').removeClass('selected');
    tableNewFrequency.row(index + order).nodes().to$().addClass( 'selected' );
}

$('.content').on('click','.readComplexFromFile',function () {
    $('#inputReadFromSite').click();
});

$('.content').on('change','#inputReadFromSite',function () {

    let fd = new FormData();
    fd.append( 'file', $(this)[0].files[0]);

    $.ajax({
        url: '/programmer/read-from-file',
        data: fd,
        processData: false,
        contentType: false,
        type: 'POST',
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            $('.content').find('#inputReadFromSite').val('');

            modalAlert(msg.state, msg.title, msg.content, msg.footer);

            if (msg.state === 'success') {

                $("#tableComplexRecording").jsGrid("option", "data", []);

                var i = 0;
                msg.complexInfo.infoComplex.forEach(function(item) {

                    let formatTime = formattingTime(item.totalTime);

                    i++;
                    $("#tableComplexRecording")
                        .jsGrid("insertItem",
                            {
                                infoProgram: item.infoProgram,

                                totalTime:item.totalTime,
                                time: formatTime.total,

                                num: i,
                                name: item.name,
                                cycles: item.cycles,
                                timeCoef: item.timeCoef
                            })
                        .done(function() {
                            $("#tableComplexRecording").jsGrid('refresh');
                        });
                })
            }
        }
    });
});

$('.content').on('click','.writeComplexOnFile',function () {

    btnDisabled($(this));

    updateDateInTable("#tableComplexRecording")

    let data = {};
    data.infoComplex = $("#tableComplexRecording").jsGrid("option", "data");

    if(data.infoComplex.length > 0){

        data.frequencyUpDown = $('.content').find('input[name="frequencyUpDown"]').val();

        let form = document.createElement('form');
        form.setAttribute('method', 'POST');
        form.setAttribute('action', '/programmer/write-on-file');

        let hiddenField = document.createElement('input');
        hiddenField.setAttribute('type', 'hidden');
        hiddenField.setAttribute('name', 'data');
        hiddenField.setAttribute('value', JSON.stringify({dateTime:getDateTimeNow(),data:data}));
        form.appendChild(hiddenField);
        document.body.appendChild(form);
        form.submit();

    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('complex_is_not_made'),
            ''
        );
    }
});

$('.content').on('click','.sendComplexOnEmail',function () {

    btnDisabled($(this));

    let emailPatient = $(this).closest('.input-group').find('input').val();
    let pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,6}\.)?[a-z]{2,6}$/i;
    if((emailPatient == '') || (!pattern.test(emailPatient))) {
        modalAlert('danger',t_js('error'),'<p>'+t_js('you_have_entered_an_incorrect_email')+'</p>','');
        return false;
    }

    updateDateInTable('#tableComplexRecording');

    let data = {};
    data.infoComplex = $("#tableComplexRecording").jsGrid("option", "data");

    if(data.infoComplex.length > 0){

        data.frequencyUpDown = $('.content').find('input[name="frequencyUpDown"]').val();

        data = JSON.stringify(data);

        $.ajax({
            url: '/programmer/send-complex-file-on-email',
            type: 'POST',
            data: {
                dateTime:getDateTimeNow(),
                data:data,
                email:emailPatient
            },
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function (msg) {

                modalAlert(msg.state, msg.title, msg.content, msg.footer);

            }
        });

    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('complex_is_not_made'),
            ''
        );
    }
});

$('.content').on('click','.loadComplexForCode',function () {

    let code = $(this).closest('.compositionComplex').find('input').val();

    if(code == ''){
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('code_not_be_empty'),
            ''
        );

        return false;
    }

    $.ajax({
        url: '/programmer/load-for-code',
        type: 'POST',
        data: {code:code},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            modalAlert(msg.state, msg.title, msg.content, msg.footer);

            if (msg.state === 'success') {

                $("#tableComplexRecording").jsGrid("option", "data", []);

                let i = 0;
                msg.complexInfo.infoComplex.forEach(function(item) {

                    let formatTime = formattingTime(item.totalTime);

                    i++;
                    $("#tableComplexRecording")
                        .jsGrid("insertItem",
                            {
                                infoProgram: item.infoProgram,

                                totalTime:item.totalTime,
                                time: formatTime.total,

                                num: i,
                                name: item.name,
                                cycles: item.cycles,
                                timeCoef: item.timeCoef
                            })
                        .done(function() {
                            $("#tableComplexRecording").jsGrid('refresh');
                        });
                })
            }
        }
    });
});

$('#modalAlert').on('click','.btnSuccessBuyPrograms',function () {
    btnDisabled($(this));

    let programsId = $(this).data('list');

    $.ajax({
        url: '/programmer/buy-programs',
        method: 'POST',
        data: {
            list: programsId
        },
        success: function (msg) {
            modalAlert(msg.state,msg.title,msg.content,msg.footer);

            getBalance();
        }
    });
});

$('.content').on('keypress','.validateRuEn',function () {

    let inputVal = $(this);
    let that = this;

    setTimeout(function() {
        let checkInp =  /[^0-9A-Za-zа-яА-ЯёЁ\- ]/g.exec(that.value);
        if(checkInp != null) {
            that.value = that.value.replace(checkInp, '');
            inputVal.popover({
                container: 'body',
                content: t_js('name_must_be_latin_cyrillic'),
                placement: 'right'
            }).popover('show');
        }
    }, 0);
});

$('.content').on('click','.checkboxSelectAll',function () {
    let tableId = "#"+$(this).data('table-id');

    let data = $(tableId).jsGrid("option", "data");
    let newData = [];

    let flCheck = false;
    if($(this).is(':checked')){
        flCheck = true;
    }

    for(let item of data){
        if(flCheck === true){
            item.сheckedRow = true;
        } else {
            delete item.сheckedRow;
        }

        newData.push(item);
    }

    updateListDataInTable(tableId,newData)
});

function updateDateInTable(tableId) {
    let blTabl = $('.content').find(tableId);

    let ind = blTabl.find('table tr.selected-row').data('ind');
    if(typeof(ind) !== "undefined") {
        blTabl.jsGrid("updateItem");
    }
}

function updateIndexInTable(tableId) {
    let gridData = $(tableId + " .jsgrid-grid-body tbody");

    let items = $.map(gridData.find("tr"), function(row) {
        return $(row).data("JSGridItem");
    });

    items.forEach(function callback(item, index, array) {
        item.num = index + 1;
        $(tableId).jsGrid("updateItem",item);
    });

    $(tableId).jsGrid("loadData");
}

function updateListDataInTable(tableId,data) {
    $(tableId).jsGrid("option", "data", []);
    data.forEach(function(item) {
        $(tableId)
            .jsGrid("insertItem",item)
            .done(function() {
                $(tableId).jsGrid('refresh');
            });
    });
}

function deleteItemsInTable(tableId) {
    let dataTable = $(tableId).jsGrid("option", "data");

    let flDelete = false;

    let arrayDelete = [];
    dataTable.forEach(function(item) {
        if(item.сheckedRow === true){
            arrayDelete.push(item);
        }
    });


    if(arrayDelete.length > 0){
        arrayDelete.forEach(function(item){
            $(tableId).jsGrid("deleteItem",item);
            flDelete = true;
        });
    }

    if(flDelete === false){
        let ind = $(tableId).find('table tr.selected-row').data('ind');
        if(typeof(ind) !== "undefined"){
            $(tableId).jsGrid("deleteItem",dataTable[ind]);
            flDelete = true;
        }
    }

    if(flDelete === true){
        updateIndexInTable(tableId);
    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('no_items_selected_for_deletion'),
            ''
        );
    }
}

function setRowEditInTable(tableId,ind){
    let gridData = $(tableId +" .jsgrid-grid-body tbody");

    gridData.find('tr').each(function(index){
        if(index == ind){
            $(this).addClass('jsgrid-selected-row').click();
        }
    })

}

function sortItemInTable(tableId,sortType){
    let ind = checkSelectedRowInTable(tableId);

    if(ind !== false){

        let dataCompositionComplex = $(tableId).jsGrid("option", "data");

        let dataSort = [];
        for (var key in dataCompositionComplex) {
            dataSort[key] = dataCompositionComplex[key];
        }

        let newInd = '';
        if(sortType == 'up' && ind != 0){
            newInd = ind - 1;
        } else if(sortType == 'down' && (ind + 1) != dataSort.length){
            newInd = ind + 1;
        }

        if(newInd !== ''){
            arrayMove(dataSort,ind,newInd);

            updateListDataInTable(tableId,dataSort);

            setRowEditInTable(tableId,newInd);
        }
    }
}

function checkSelectedRowInTable(tableId) {
    let ind = $(tableId).find('table tr.selected-row').data('ind');

    if(typeof(ind) == "undefined"){
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('no_items_selected'),
            ''
        );

        return false
    }

    return ind;
}