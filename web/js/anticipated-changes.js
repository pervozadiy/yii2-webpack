$('.content').on('click','.showRemoveItemACBtn',function () {
    let bl = $(this).closest('.col-md-6');

    $(this).addClass('onlyHide');

    bl.find('.removeItemAC').each(function () {
        $(this).removeClass('onlyHide')
    });
});

$('.content').on('click','.removeItemAC',function () {
    btnDisabled($(this));

    let bl = $(this).closest('span');
    let itemId = $(this).data('id');
    let testId = $(this).closest('.col-md-6').data('test-id');

    $.ajax({
        url: '/anticipated-changes/edit-test',
        method: 'POST',
        data: {
            diagnostic_test_id: testId,
            diagnoses_weighting_id: itemId
        },
        success: function (msg) {
            if(msg){
                bl.remove();
            }
        }
    });
});