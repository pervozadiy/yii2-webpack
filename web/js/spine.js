
$('.content').on('click','.radioBtnSpine',function () {
    let selectedItemSpine = $(this).val();
    checkedItem($(this).closest('.row'),selectedItemSpine);
    showItemSpine(selectedItemSpine);
});

$('.content').on('click','.imgSpine span',function () {
    let selectedItemSpine = $(this).data('spine');

    checkedItem($(this).closest('.row'),selectedItemSpine);

    showItemSpine(selectedItemSpine);
});



function showItemSpine(selectedItemSpine)
{
    let ItemSpine = listSpine[selectedItemSpine];

    let videoСontainer = $('.content').find(".imgItemSpine video");
    videoСontainer.find('source[type="video/webm"]').attr({src:ItemSpine.alias + '/' + ItemSpine.id + '/' + ItemSpine.img});
    videoСontainer[0].load();

    $('.contentItemSpine .selectedLabel').html(ItemSpine.title);
    $('.contentItemSpine .selectedDepartment').html(ItemSpine.deprtment);
    $('.contentItemSpine .selectedOrgans').html(ItemSpine.organs);
    $('.contentItemSpine .selectedEffectsValue').html(ItemSpine.effect);
}

function checkedItem(bl,numItem){
    bl.find('input[value="'+numItem+'"]').each(function(){
        $(this).prop({"checked":true});
    });
}