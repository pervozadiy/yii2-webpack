const audioFiles = {
    success:'./images/audio/success.mp3',
    error:'./images/audio/error.mp3'
};

function runAudio(type){
    let loaded = false;
    let aud = new Audio();
    aud.addEventListener('loadeddata', function() {
        loaded = true;
        aud.play();
    }, false);
    aud.src=audioFiles[type]
}