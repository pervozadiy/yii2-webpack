dataMeasure = {};

function requestFromProgram(request){
    device = $('meta[name="device"]').attr("content");

    setLogger(request);

    request = JSON.parse(request);

    if(request.error!='ok'){

        clearInterval(timerId);
        clearInterval(checkStartDeviceTimeout);

        blcontrolDevice = $('.content').find('.controlDevice');
        blcontrolDevice.find('[data-toggle="on"]').removeAttr('disabled');
        blcontrolDevice.find('[data-toggle="off"]').attr({disabled:'disabled'});

        displaysErrorFromDevices({name:request.error,numberMeasure:(parseInt(request.numberMeasure) + 1)});

        realtime = 'stop';

        $('.content').find('.videoItemSystemAndOrgans video').trigger('pause');

        sendHash({action:'stop',method:'express-test',nameDevice:device});

        return false;
    } else {

        numberMeasure = parseInt(request.numberMeasure) + 1;

        showItemSystemAndOrgans(numberMeasure);

        dataMeasure[numberMeasure] = {
            data:request.data,
            numberMeasure:numberMeasure
        };

        let progressTest = numberMeasure * 2.13;
        // console.log(progressTest)
        $('.content').find('.knob').val(progressTest).trigger('change');

        if(numberMeasure == 47){

            clearInterval(timerId);

            setLogger(dataMeasure);

            sendHash({action:'stop',nameDevice:device});

            setLogger(JSON.stringify({action:'stop',nameDevice:device}));


            blcontrolDevice = $('.content').find('.controlDevice');
            blcontrolDevice.find('[data-toggle="on"]').removeAttr('disabled');
            blcontrolDevice.find('[data-toggle="off"]').attr({disabled:'disabled'});
            realtime = 'stop';
            $('.content').find('.videoItemSystemAndOrgans video').trigger('pause');

            sendDataTestForServer(dataMeasure);

            dataMeasure = {};
        }

    }
}

function sendDataTestForServer(allDataMeasure) {


    dataMeasure = {};

    data = {};
    data.measure = allDataMeasure;
    data.patient = patientId;

    data.typeExpressTest = typeExpressTest;
    data.testedDrug = testedDrug;
    data.testedDrugId = testedDrugId;
    data.device = $('meta[name="device"]').attr("content");
    data.dateTime = getDateTimeNow();


    //setLogger(JSON.stringify(data));

    $.ajax({
        url: '/express-test/create',
        type: 'POST',
        data: data,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {

            setLogger(JSON.stringify(msg));


            if(msg.state != 'danger'){
                if($('.content').find('table').is("#listSeanceExpress")){
                    getTestForExpressTest(msg.data.patientId);
                    modalAlert(msg.state,msg.title,msg.content,msg.footer);
                } else if($('.content').find('table').is("#listSeanceDrugSelection")){
                    getTestForDrugSelection(msg.data.patientId);
                    modalAlert(msg.state,msg.title,msg.content,msg.footer);
                } else if($('.content').find('table').is("#listSeanceProductSelection")){
                    getTestForProductSelection(msg.data.patientId,'calculateTest');
                }

                runAudio('success');

                $('.content').find('.knob').val(0).trigger('change');
            } else {
                modalAlert(msg.state,msg.title,msg.content,msg.footer);
            }


        },
        error: function (jqXHR, exception) {
            setLogger(JSON.stringify(jqXHR));
            setLogger('request failed :'+exception);
        }
    });

    return false;

}

function checkStartDevice() {
    device = $('meta[name="device"]').attr("content");

    checkStartDeviceTimeout = setTimeout(function () {

        if(typeof(dataMeasure[1]) == "undefined"){
            sendHash({action:'stop',method:'express-test',nameDevice:device});

            setLogger(JSON.stringify({action:'stop',method:'express-test',nameDevice:device}));


            clearInterval(timerId);

            blcontrolDevice = $('.content').find('.controlDevice');
            blcontrolDevice.find('[data-toggle="on"]').removeAttr('disabled');
            blcontrolDevice.find('[data-toggle="off"]').attr({disabled:'disabled'});

            displaysErrorFromDevices({name:'test_error'});

            realtime = 'stop';

            $('.content').find('.videoItemSystemAndOrgans video').trigger('pause');

            return false;
        }
    },8000);

    return true;
}


$('.content').on('click','.btnHandleTest',function () {
    var blTest = $(this).closest('tr');
    var idTest = $(this).data('test');

    $.ajax({
        url: '/calculation-test/calculation-diagnostic-test',
        type: 'POST',
        data: {id:idTest,dateTime:getDateTimeNow()},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            //$('#loading_box').hide();
        },
        success: function (msg) {
            setTimeout(function(){

                $('#loading_box').hide();

                modalAlert(msg.state,msg.title,msg.content,msg.footer);

                if(msg.state !== 'danger'){
                    blTest.find('.statusTest').text(t_js('prepared_by')).removeClass('label-danger').addClass('label-success');
                    blTest.find('.btnHandleTest').attr({disabled:'disabled'}).removeClass('btnHandleTest');
                    blTest.find('.btnRecordComplex').removeAttr('disabled');
                    blTest.find('.process').text(t_js('processed'));

                    getBalance();
                }
            }, 5000);
        }
    });
});

$('.content').on('click','.btnHandleTestDrug',function () {
    var blTest = $(this).closest('tr');
    var idTest = $(this).data('test');

    $.ajax({
        url: '/calculation-test/calculation-diagnostic-test',
        type: 'POST',
        data: {id:idTest,dateTime:getDateTimeNow()},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            //$('#loading_box').hide();
        },
        success: function (msg) {
            setTimeout(function(){

                $('#loading_box').hide();

                modalAlert(msg.state,msg.title,msg.content,msg.footer);

                if(msg.state != 'danger'){
                    blTest.find('.statusTest').text(t_js('processed')).removeClass('label-danger').addClass('label-success');
                    blTest.find('.btnHandleTestDrug').attr({disabled:'disabled'}).removeClass('btnHandleTestDrug');
                    blTest.find('.process').text(t_js('processed'));

                    getBalance();
                }
            }, 5000);
        }
    });
});

$('.content').on('click','.btnRecordComplex',function () {

    if(!$(this).attr('disabled')) {

        let flConnectDevice = checkConnectDevice('life-balance');
        if(flConnectDevice === false ){
            return false;
        }

        blTest = $(this).closest('tr');
        var idTest = $(this).data('test');

        $.ajax({
            url: '/medical-complex/create',
            type: 'POST',
            data: {id: idTest,dateTime:getDateTimeNow()},
            beforeSend: function () {
                modalAlert(
                    'warning',
                    t_js('processing_data'),
                    '<div class="height250 paddingTop20"><p><img src="../images/complex-calculation.gif" class="img-responsive"></p></div>',
                    ''
                );
            },
            complete: function () {

            },
            success: function (msg) {
                setTimeout(function(){

                    blTest.find('.process').text(t_js('complex_is_recorded'));

                    //TODO:KAA delete old app
                    if(msg.complex.callbackFunction == 'checkDeviceForRecordComplex'){
                        modalAlert(msg.state, msg.title, msg.content, msg.footer);

                        if(msg.state != 'danger') {
                            sendHash(msg.complex);
                            setLogger(JSON.stringify(msg.complex));

                            setFlPendingComplexRecording(true);

                            waitingDeviceResponse();

                        }
                    } else {
                        if(msg.state != 'danger') {
                            sendHash(msg.complex);
                            setLogger(JSON.stringify(msg.complex));

                            setFlPendingComplexRecording(true);

                        } else {
                            modalAlert(msg.state, msg.title, msg.content, msg.footer);
                        }
                    }

                    setLogger(JSON.stringify(msg.complex));
                }, 5000);
            }
        });
    }
});


// function checkDeviceForRecordComplex(request){
//
//     if (typeof(flWaitingDeviceResponse) !== "undefined"){
//         clearInterval(flWaitingDeviceResponse);
//     }
//
//     setLogger(request);
//     request = JSON.parse(request);
//
//     if(request.error!='ok'){
//         modalAlert('danger',t_js('error'),'<p>'+t_js('device_not_found')+'</p>','');
//     } else {
//         modalAlert('success',t_js('recording_complex'),
//             '<p>'+t_js('complex_sent_recording')+'.</p>' +
//             '<div class="progress progress-striped active progressBarRecordComplex">' +
//                 '<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">' +
//                     '<span class="sr-only"></span>' +
//                 '</div>' +
//             '</div>','');
//
//         startProgressBarRecordComplex();
//     }
// }


$('.content').on('change','.selectedContainer',function () {
    var valueContainer = $(this).val();

    if(valueContainer == 'electronic-version'){
        $('.content').find('.selectedCompany').removeAttr('disabled');
        $('.content').find('.selectedProduct').removeAttr('disabled');
        $('.content').find('.testedDrug').attr({disabled:'disabled'});
    } else {
        $('.content').find('.selectedCompany').attr({disabled:'disabled'});
        $('.content').find('.selectedProduct').attr({disabled:'disabled'});
        $('.content').find('.testedDrug').removeAttr('disabled');
    }

});

$('.content').on('change','.selectedCompany',function () {
    valCompany = $(this).val();

    if(valCompany != ''){
        $.ajax({
            url: '/drugs/list-drugs-for-firm',
            type: 'POST',
            data: {firmId:valCompany},
            success: function (msg) {

                $('.content').find('.selectedProduct').html('');
                $.each(msg, function(key, value) {
                    $('.content')
                        .find('.selectedProduct')
                        .append($('<option>', { value : key })
                        .text(value));
                });

            }
        });
    }

});

function showItemSystemAndOrgans(selectedItemSystemAndOrgans) {

    ItemSystemAndOrgans = listSystemAndOrgans[selectedItemSystemAndOrgans];

    $('.contentItemSystemAndOrgans .selectedLabel').html(ItemSystemAndOrgans.title);

}

function getRandomData(totalPoints) {

    if (data.length > totalPoints)
        data = data.slice(1);

    // Do a random walk
   // for(var i=0;i<totalPoints;i++){
        if(realtime == 'on') {
            randomValue = Math.random();
            if (randomValue < 0.2) {
                randomValue = Math.random();
            }

            var y = randomValue * 100;

            if (y < 40) {
                y = 41;
            } else if (y > 100) {
                y = 99;
            }
        } else {
            y = 0;
        }

        y = Math.round(y);

        $('.progressElectricalResistivity .progress-bar').css({width:'0%'});
        $('.progressElectricalResistivity .progress-bar').css({width:y+'%'});
        $('.progressElectricalResistivity label span').text(y);

        data.push(y);
   // }

    // Zip the generated y values with the x values
    var res = [];
    for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]]);
    }

    return res;
}

function getTestForExpressTest(selectedPatientId) {
    var selectedPatientId = $('meta[name="patientId"]').attr("content");
    var device = $('meta[name="device"]').attr("content");
    $.ajax({
        url: '/tests/get-table-tests-for-express-test?selectedPatientId='+selectedPatientId+'&device='+device,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(data){
            table =  $('.content').find('#listSeanceExpress');

            table.dataTable().api().clear();
            table.dataTable().api().rows.add(data);
            table.dataTable().api().draw();
        }
    });
}

function getTestForDrugSelection(selectedPatientId) {
    var selectedPatientId = $('meta[name="patientId"]').attr("content");
    var device = $('meta[name="device"]').attr("content");
    $.ajax({
        url: '/tests/get-table-tests-for-drug-selection?selectedPatientId='+selectedPatientId+'&device='+device,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(data){
            table =  $('.content').find('#listSeanceDrugSelection');

            table.dataTable().api().clear();
            table.dataTable().api().rows.add(data);
            table.dataTable().api().draw();
        }
    });
}

$('.content').on('click','.btnDeleteTest',function () {

    var testID = $(this).data('id');
    var patientID = $(this).data('patient-id');
    var funtionGetTest = $(this).data('function-test');

    modalAlert(
        'warning',
        t_js('attention')+'!',
        '<p>'+t_js('are_you_sure_you_want_delete_test')+'</p>',
        '<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">'+t_js('close')+'</button>' +
        '<button type="button" class="btn btn-outline pull-right btnSuccessDeleteTest" data-id = "'+testID+'" data-patient-id = "'+patientID+'" data-funtion-get-test = "'+funtionGetTest+'">'+t_js('remove')+'</button>'
    );
});

$('#modalAlert').on('click','.btnSuccessDeleteTest',function () {
    var testID = $(this).data('id');
    var patinetID = $(this).data('patient-id');
    var funtionGetTest = $(this).data('funtion-get-test');

    $.ajax({
        url: '/tests/soft-delete',
        type: 'POST',
        data: {testId: testID},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){

            if(msg.state === 'success'){
                window[funtionGetTest](patinetID);
            }

            modalAlert(msg.state,msg.title,msg.content,msg.footer);

        }
    });
});