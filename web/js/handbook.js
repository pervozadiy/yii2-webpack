$(document).ready(function() {
    $('.content').on('click','.blMenuHandbook a',function () {

        contentId = $(this).data('id');

        changeActive(contentId);

        $.ajax({
            url: '/handbook/get-content?id='+contentId,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){
                $('.content').find('.contentHandbook').html(msg);

            }
        });
    });

});

function changeActive(contentId) {
    $('.content').find('.blMenuHandbook li').each(function () {
       $(this).removeClass('active');

       if($(this).find('a').data('id') == contentId){
           $(this).addClass('active');
       }

    });
}