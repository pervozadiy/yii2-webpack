$('.content').on('click','.dynamicAddtoReport',function () {

    $.ajax({
        url: '/dynamic/save-data-report',
        type: 'POST',
        data: {
            dataPlot: JSON.stringify(interactive_plot.getData()),
            dataTest: $('.content').find('.dynSessionCheckbox:checked').map(function(){
                return this.value;
            }).get(),
            countDayPrediction:$('.content').find('#ex1').val()
        },
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {
            modalAlert(msg.state,msg.title,msg.content,msg.footer);
        }
    });
});

$('.content').on('change','#ex1', function(slideEvt) {

    interactive_plot.unhighlight();

    var maxDinSlider = $("#ex1").bootstrapSlider('getAttribute', 'max');

    var tmpDynData = interactive_plot.getData();

     for (var j = 0; j < 47; j++) {

        tmpDynData[tmpDynData.length-1].data[j][1] =  averageDynData[j][1]+ Math.round(slideEvt.value.newValue * ((tmpDynData[0].data[j][1] - tmpDynData[1].data[j][1])/maxDinSlider));

        if (tmpDynData[tmpDynData.length-1].data[j][1] < 0) {
            tmpDynData[tmpDynData.length-1].data[j][1] = 0;
        } else if (tmpDynData[tmpDynData.length-1].data[j][1] > 100) {
            tmpDynData[tmpDynData.length-1].data[j][1] = 100;
        }

    }

    interactive_plot.setData(tmpDynData);
    interactive_plot.draw();

});

$('.content').on('click','.predictionMode',function () {

    if (this.checked) {
        $("#ex1").bootstrapSlider("enable");
        $("#ex1").bootstrapSlider('setValue', 0 ,true,true);
        $("#ex1").trigger({
            'type': 'change',
            'value': {
                'newValue' : 0
            }
        });
        $("#dynDays").parent().removeClass('hidden');
    } else {
        $("#ex1").bootstrapSlider("disable");

        createPlot();
    }

});

$('.content').on('change','.dynSessionCheckbox',function () {

    var checkedTest = $('.content').find('.dynSessionCheckbox:checked')

    if(checkedTest.length > 5){
        modalAlert('danger',t_js('attention'),'<p>'+t_js('select_no_more_than_5_tests')+'</p>','');
        $(this).iCheck('uncheck');
        return false;
    }

    var color = $(this).data('color');
    var isCheked = $(this).iCheck('update')[0].checked;
    if(isCheked === true && color === ""){
        $(this).data({color:arrayColors.pop()});
    } else if (isCheked === false && color !== ""){
        arrayColors.push(color);
        $(this).removeData('color');
    }

    createPlot();

});


function getDataDynamic() {

    dataDyn = [];
    var series = 0;

    $('.dynSessionCheckbox').each(function (i) {

        var res = [];
        $(this).removeData('series');

        if (this.checked) {

            // если сеанс выбран то надо запихнуть данные в серию см testData

            // вот тут можешь выдернуть аяксом данные по сеану, id сеанса можно записать сразу в инпут при составлении списка сеансов вверху, а потом тут дёргать  нарпимер $(this).data('seansId')

            // $(this).data('color',newColor) список цветов вот так будешь проставлять, их просто загонишь в базу и функцией будешь выдёргивать случайный, когда данные дёргать будешь и проставлять цвета кликнутым инпутам

            var testId = $(this).val();

            res = [];
            for (var prop in testData[testId]) {
                res.push([prop, testData[testId][prop]]);
            }
            $(this).data('series', series);

            if (series == 0) {
                // самый первый выбранный элемент будет использоваться для прогнозирования, поэтому сохраняем его
                averageDynData = res;
            }

            series++;
            dataDyn.push(res);
        }

    });

    if (series>1) {

        var res = [];

        // если количество выбранных сеансов больше 1 то добавляем пустую серию(пустой массив), её дальше будем использовать для режима прогнозирования, она будет последним элементов в объёекте серий

        for (var j = 0; j < 47; j++) {

            res.push([j+1, undefined]);
        }

        dataDyn.push(res);

        $(".predictionMode").prop("disabled",false);
        $(".predictionMode").prop("checked",false);

        // и просчитываем количество дней для прогнозирования
        var dates = $(document).find('.dynSessionCheckbox[type=checkbox]:checked');

        var dateDyn1 = new Date(dates.first().data('date'));
        var dateDyn2 = new Date(dates.last().data('date'));
        var daysLag = Math.ceil(Math.abs(dateDyn1.getTime() - dateDyn2.getTime()) / (1000 * 3600 * 24));

        $("#ex1").bootstrapSlider('setAttribute', 'max', daysLag);
        $("#ex1").bootstrapSlider('setValue', 0);
        $("#ex1").bootstrapSlider('refresh');
        $("#dynDays").parent().removeClass('hidden');
        $("#dynDays").text(daysLag);

    } else {

        $(".predictionMode").prop("checked",false);
        $(".predictionMode").prop("disabled",true);
        $("#ex1").bootstrapSlider("disable");
        $("#dynDays").parent().addClass('hidden');

    }

    // вот тут для пирога выдёргиваем данные какой нить функцией

    var tmpPieData = [];

    if (series == 0) {
        // ни одного сеанса не выбрано показываем серый пирожок
        tmpPieData = dataDynPieClear;
    } else {
        // иначе выдёргиваем каким нитть аяксов в формате dataDynPie данные
        tmpPieData = dataDynPie;
    }

    //и прорисовываем его
    interactive_pie.setData(tmpPieData);
    interactive_pie.draw();


}


function createPlot(){
    $.ajax({
        url: '/dynamic/get-data-test',
        type: 'POST',
        data: {
            id:$('.content').find('.dynSessionCheckbox:checked').map(function(){
                return this.value;
            }).get()
        },
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {

            testData = msg.infoTest;
            dataDynPie = msg.predominanceDeterioratingParameters;

            getDataDynamic();

            interactive_plot.unhighlight();

            interactive_plot.setData(dataDyn);
            interactive_plot.draw();

            var tmpDynData = interactive_plot.getData();

            var j=0;
            $('.dynSessionCheckbox').each(function () {

                $(this).parent().css("color","");

                if (this.checked) {
                    $(this).css("color", "");
                    tmpDynData[j].color = $(this).data('color');

                    $(this).parent().css("color",$(this).data('color'));
                    j++;
                }

            });


            if (j>1) {
                tmpDynData[tmpDynData.length-1].color = "#1200fd";
                tmpDynData[tmpDynData.length-1].lines.lineWidth = 5;
                tmpDynData[tmpDynData.length-1].points.radius = 4;
            }

            interactive_plot.setData(tmpDynData);
            interactive_plot.draw();

        }
    });
}