
$('.content').on('click','.blAurogram .number,.tableAurogramValue td',function () {
    let selectedItemAurogram = $(this).data('siganl-id');
    let selectedPartAurogram = $(this).data('part');

    showItemAurogramInfo(selectedItemAurogram);

    showItemAurogramDescription(selectedItemAurogram,selectedPartAurogram);
});

function showItemAurogramInfo(selectedItemAurogram)
{
    let ItemAurogram = listAurogramInfo[selectedItemAurogram];

    let videoСontainer = $('.content').find(".imgItemAurogram video");
    videoСontainer.find('source[type="video/webm"]').attr({src:ItemAurogram.video_url});
    videoСontainer[0].load();

    $('.contentItemAurogram .selectedLabel').html(ItemAurogram.title);
}

function  showItemAurogramDescription(selectedItemAurogram,selectedPartAurogram) {
    let description = listAurogramDiseases[selectedPartAurogram][selectedItemAurogram];

    if(description != ''){
        $('.contentItemAurogram .selectedDescription').html(description);
    } else {
        $('.contentItemAurogram .selectedDescription').html('');
    }

}