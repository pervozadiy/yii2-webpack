$('.content').on('click','.checkSocketDevice',function () {

    let blLogs = $('.content').find('.socketLogs')

    let url = $(this).closest('.well').find('input[name="url"]').val()
    let serialNumber = $(this).closest('.well').find('input[name="serial_number"]').val()

    if(serialNumber && url){

        let socket = io.connect(url)

        socket.emit('auth',{serial_number:serialNumber})

        socket.on('request',function (data) {

            let logInfo = `data: ${JSON.stringify(data)}<br><hr>`;

            blLogs.append(logInfo)
        })

    } else {
        alert('Серийный номер или линк не введен')
    }

});

$('.content').on('click','.checkSocketServer',function () {

    let blLogs = $('.content').find('.socketLogs')

    let serialNumber = $(this).closest('.row').find('input').val()

    if(serialNumber){
        $.ajax({
            url: '/test/socket-server',
            method: 'POST',
            data: {
                'serialNumber': serialNumber
            },
            success: function (msg) {
                let logInfo = `data: ${JSON.stringify(msg)}<br><hr>`;

                blLogs.append(logInfo)
            }
        });

    } else {
        alert('Серийный номер не введен')
    }

});