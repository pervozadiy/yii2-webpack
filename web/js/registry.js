class Registry{
    constructor(){
        this.mainSelector = '.content'
    }

    init = () => {
        // set patient for diagnostic with table patiens
        $(this.mainSelector).on('click','.enterPatient', (e) => {
            $(e.currentTarget).closest('tbody').find('tr').each(function () {
                $(this).removeClass('info');
            });
            $(e.currentTarget).closest('tr').addClass('info');
            let patinetID = $(e.currentTarget).closest('tr').data('id');
            setDiagnosticPatient(patinetID);
        });

        // get modal success delete patient
        $(this.mainSelector).on('click','.btnDeletePatient', (e) => {
            btnDisabled($(e.currentTarget))

            let patinetID = $(e.currentTarget).closest('tr').data('id');

            modalAlert(
                'warning',
                `${t_js('attention')}!`,
                `<p>${t_js('are_you_sure_you_want_delete_patient')}</p>`,
                `<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
                    ${t_js('close')}
                </button>
                <button type="button" class="btn btn-outline pull-right btnSuccessDeletePatient" data-patient-id = "${patinetID}">
                    ${t_js('remove')}
                </button>`
            );

        });

        // success delete patient
        $('#modalAlert').on('click','.btnSuccessDeletePatient', (e) => {
            let patinetID = $(e.currentTarget).data('patient-id');


            $.ajax({
                url: '/patients/soft-delete',
                type: 'POST',
                data: {patientId: patinetID},
                beforeSend: function () {
                    $('#loading_box').show();
                },
                complete: function () {
                    $('#loading_box').hide();
                },
                success: (msg) => {

                    if(msg.state === 'success'){
                        let form =  $(this.mainSelector).find('.formPatients');

                        form.trigger('reset');
                        form.find('input[name="Patients[id]"]').val('');
                        form.find('textarea[name="Patients[comment]"]').val('');
                        form.find('.blAge .valueAge').text('');
                        form.find('.linkAnketa').data({'href':''}).attr({'disabled':'disabled'});

                        form.find('input[type="radio"]').each(function(){
                            $(this).removeAttr('checked');
                        });

                        let table =  $(this.mainSelector).find('#listTestingPatient');
                        let rowTable = table.find('tr[data-id="'+patinetID+'"]');
                        table.dataTable().api().row(rowTable).remove().draw();

                        if(msg.unsetActivePatient != ''){
                            $('.use-patient .name-patient').data({'patient-id':''}).text(msg.unsetActivePatient);
                        }

                    }

                    modalAlert(msg.state,msg.title,msg.content,msg.footer);

                }
            });


        });

        // get info for update patient
        $(this.mainSelector).on('click','.btnEditPatient', (e) => {
            let patinetID = $(e.currentTarget).closest('tr').data('id');

            this.editPatient(patinetID);
        });

        // check information about patient in other doctors
        $(this.mainSelector).on('focusout','.formPatients #patients-email,.formPatients #patients-phone', (e) => {
            let form = $(e.currentTarget).closest('form');
            let email = form.find('#patients-email').val();
            let phone = form.find('#patients-phone').val();

            if(email !== '' || phone !== ''){
                $.ajax({
                    url: '/patients/check-patients-anketa',
                    type: 'POST',
                    data: {email:email,phone:phone},
                    success: (msg) => {
                        if(msg.valueKeyAnketa !== ''){
                            form.find('.loadPatientsWithAnketa')
                                .removeAttr('disabled')
                                .data({value:msg.valueKeyAnketa});
                        } else {
                            form.find('.loadPatientsWithAnketa').attr({disabled:true}).data({value:''});
                        }
                    }
                });
            }
        });

        // save and update patient
        $(this.mainSelector).on('submit','.formPatients',function (e) {
            e.preventDefault();

            let form = $(e.currentTarget);
            let data = form.serialize();

            if(form.find('#patients-email').val() === '' && form.find('#patients-phone').val() === ''){
                document.getElementById("patients-email").setCustomValidity(t_js('email_or_phone_must_be_filled'));

                return false;
            } else {
                document.getElementById("patients-email").setCustomValidity("");
            }

            let url = '/patients/create';
            if(form.find('input[name="Patients[id]"]').val() != ''){
                url = '/patients/update';
            }

            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                beforeSend: function () {
                    $('#loading_box').show();
                },
                complete: function () {
                    $('#loading_box').hide();
                },
                success: (msg) => {
                    if(msg.state === 'success'){
                        if(msg.action === 'update'){
                            $('#listTestingPatient tbody').find('tr[data-id="'+msg.Patients.id+'"]').remove();
                        }

                        $('#listTestingPatient tbody').prepend(
                            `<tr data-id="${msg.Patients.id}">
                                <td class="enterPatient">
                                    ${msg.Patients.first_name} ${msg.Patients.last_name}
                                </td>
                                <td class="enterPatient"></td>
                                <td><a href="javascript:void(0);" class="btnEditPatient"><i class="fa fa-pencil"></i></a></td>
                                <td><a href="javascript:void(0);" class="btnDeletePatient"><i class="fa fa-trash"></i></a></td>
                            </td>`
                        );

                        form.trigger('reset');
                        form.find('input[name="Patients[id]"]').val('');
                        form.find('textarea[name="Patients[comment]"]').val('');
                        form.find('.blAge .valueAge').text('');
                        form.find('.linkAnketa').data({'href':''}).attr({'disabled':'disabled'});
                    }

                    modalAlert(msg.state,msg.title,msg.content,msg.footer);

                }
            });
        });

        // calculating birthday
        $(this.mainSelector).on('change','#patients-birthday',(e) => {
            let age = this.ageMan($(e.currentTarget).val());

            if(age > 105){
                $(e.currentTarget).val('');

                modalAlert(
                    'danger',
                    t_js('error'),
                    `<p>${t_js('age_can_not_exceed_105_years')}</p>`,
                    ''
                );
            } else {
                $('.blAge .valueAge').text(age);
            }

        });

        // send email on anketa
        $(document).on('click','.loadPatientsWithAnketa',(e) => {

            btnDisabled($(e.currentTarget));

            let valueKeyAnketa = $(e.currentTarget).data('value');

            if((valueKeyAnketa === '')) {
                modalAlert(
                    'danger',
                    t_js('error'),
                    `<p>${t_js('data_must_not_be_empty')}</p>`,
                    ''
                );
            } else{
                this.loadPatientsWithAnketa(valueKeyAnketa);
            }
        });

        // send email on anketa from modal
        $(document).on('click','.loadPatientsWithAnketaFromModal', (e) => {
            this.loadPatientsWithAnketa($(e.currentTarget).data('value'));
        });

        // update anketa patient
        $(document).on('submit','.formPatientsAnketa', (e) => {
            e.preventDefault();

            let form = $(e.currentTarget);
            let data = form.serialize();

            $.ajax({
                url: '/patients/save-anketa',
                type: 'POST',
                data: data,
                beforeSend: function () {
                    $('#loading_box').show();
                },
                complete: function () {
                    $('#loading_box').hide();
                },
                success: (msg) => {

                    modalAlert(msg.state,msg.title,msg.content,msg.footer);

                    if(msg.state === 'success'){
                        form.find('input[name="check[key]"]').val(msg.key);
                    }

                }
            });
        });

        // prohibition of keyboard input for birthday
        $(this.mainSelector).on('keydown','.banKeyUpInput',(e) => {
            e.preventDefault();
        });

        //
        $('#modalAlert').on('submit','.formEnterCode', (e) => {
            e.preventDefault();

            let form = $(e.currentTarget);
            let data = form.serialize();

            $.ajax({
                url: '/patients/confirmation-load-anketa',
                type: 'POST',
                data: data,
                beforeSend: function () {
                    $('#loading_box').show();
                },
                complete: function () {
                    $('#loading_box').hide();
                },
                success: (msg) => {

                    modalAlert(msg.state,msg.title,msg.content,msg.footer);

                    if(msg.state == 'success'){
                        this.editPatient(msg.patientId);
                    }


                }
            });
        })
    };

    /**
     * load data patient for form cart`s patient
     *
     * @param valueKeyAnketa
     */
    loadPatientsWithAnketa = (valueKeyAnketa) => {
        $.ajax({
            url: '/patients/load-patient-from-anketa',
            type: 'POST',
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            data: {valueKeyAnketa:valueKeyAnketa},
            success: (msg) => {
                if(msg.state == 'success'){
                    msg.content +=
                        `<p>${t_js('enter_code')}</p>
                        <form class="formEnterCode">
                            <div class="form-group row">
                                <div class="col-md-8">
                                    <input type="text" class="form-control codeConfirmation" required name="code">
                                </div>
                                <div class="col-md-4">
                                    <button type="submit" class="btn btn-block btn-success">${t_js('confirm')}</button>
                                </div>
                            </div>
                        </form>
                    `
                }
                modalAlert(msg.state,msg.title,msg.content,msg.footer);
            }
        });
    };

    /**
     * load data patient in form for edit
     *
     * @param patinetID
     */
    editPatient = (patinetID) => {
        $.ajax({
            url: '/patients/get-patient?id='+patinetID,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: (msg) => {
                let formPatient = $(this.mainSelector).find('.formPatients');

                if(msg.state === 'success'){

                    formPatient.find('input[name="Patients[id]"]').val(msg.Patient.id);
                    formPatient.find('input[name="Patients[first_name]"]').val(msg.Patient.first_name);
                    formPatient.find('input[name="Patients[last_name]"]').val(msg.Patient.last_name);
                    formPatient.find('input[name="Patients[gender]"]').removeAttr('checked').filter('[value="'+msg.Patient.gender+'"]').attr('checked',true);

                    if(formPatient.find("input").is('[name="Patients[smoke]"]')){
                        formPatient.find('input[name="Patients[smoke]"]').attr('checked',(msg.Patient.smoke === 1 ? true : false));
                    }

                    if(formPatient.find("input").is('[name="Patients[vegetarian]"]')){
                        formPatient.find('input[name="Patients[vegetarian]"]').attr('checked',(msg.Patient.vegetarian === 1 ? true : false));
                    }

                    formPatient.find('input[name="Patients[birthday]"]').val(msg.Patient.birthday);
                    formPatient.find('input[name="Patients[phone]"]').val(msg.Patient.phone);
                    formPatient.find('input[name="Patients[address]"]').val(msg.Patient.address);
                    formPatient.find('input[name="Patients[profession]"]').val(msg.Patient.profession);
                    formPatient.find('textarea[name="Patients[comment]"]').text(msg.Patient.comment);

                    formPatient.find('input[name="Patients[email]"]').val(msg.Patient.email);

                    if(msg.Patient.anketaLink !== ''){
                        formPatient.find('.linkAnketa').data({'href':msg.Patient.anketaLink}).removeAttr('disabled');
                    } else {
                        formPatient.find('.linkAnketa').data({'href':''}).attr({'disabled':'disabled'});
                    }

                    let age = this.ageMan(msg.Patient.birthday);

                    $('.blAge .valueAge').text(age);

                } else {
                    modalAlert(msg.state,msg.title,msg.content,msg.footer);
                }

            }
        });
    };

    /**
     * calculation age patient
     *
     * @param birthday
     * @returns {string}
     */
    ageMan = (birthday) => {

        let dateNow = new Date();
        let dateBirthday = new Date(birthday);
        let totalDays = Math.ceil(Math.abs(dateBirthday.getTime() - dateNow.getTime()) / (1000 * 3600 * 24));

        let dateCurrent = new Date();
        let utimeTarget = dateCurrent.getTime() + totalDays * 86400 * 1000;
        let dateTarget = new Date(utimeTarget);

        let diffYear  = parseInt(dateTarget.getUTCFullYear() - dateCurrent.getUTCFullYear());
        let diffMonth = parseInt(dateTarget.getUTCMonth() - dateCurrent.getUTCMonth());
        let diffDay   = parseInt(dateTarget.getUTCDate() - dateCurrent.getUTCDate());

        let daysInMonth = [31, (dateTarget.getUTCFullYear()%4?29:28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        let dateString = "";
        while(true)
        {
            dateString = "";
            dateString += (diffYear > 0 ? diffYear + "" : "");

            if(diffMonth < 0){
                diffYear -= 1;
                diffMonth += 12;
                continue;
            }

            if(diffDay<0){
                diffMonth -= 1;
                diffDay += daysInMonth[((11+dateTarget.getUTCMonth())%12)];
                continue;
            }

            break;
        }
        return dateString;

    };

    /**
     * get list tests
     *
     * @param selectedPatientId
     */
    getTestForRegistry = (selectedPatientId) => {
        let device = $('meta[name="device"]').attr("content");
        let table =  $(this.mainSelector).find('#listSeanceRegistry');

        $.ajax({
            url: `/tests/get-table-tests-for-registry?selectedPatientId=${selectedPatientId}&device=${device}`,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: (data) => {
                table.dataTable().api().clear();
                table.dataTable().api().rows.add(data);
                table.dataTable().api().draw();
            }
        });
    }
}


let registry = new Registry();
$(document).ready(function () {
    registry.init();
});

