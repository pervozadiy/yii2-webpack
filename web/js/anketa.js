$('.content').on('change','#checkFirstName',function () {

    var checkValue = document.getElementById("checkFirstName");

    var orginalValue = $('.content').find('#patients-first_name').val();

    if(orginalValue != $(this).val()){
        checkValue.setCustomValidity(t_js('first_name_do_not_match')+' «('+orginalValue+')»');
    } else {
        checkValue.setCustomValidity("");
    }

});

$('.content').on('change','#checkSecondName',function () {

    var checkValue = document.getElementById("checkSecondName");

    var orginalValue = $('.content').find('#patients-last_name').val();

    if(orginalValue != $(this).val()){
        checkValue.setCustomValidity(t_js('second_name_do_not_match')+' «('+orginalValue+')»');
    } else {
        checkValue.setCustomValidity("");
    }

});

$('.content').on('change','#patients-email,#patients-phone',function () {
    checkFormAnketa();
});


function checkFormAnketa() {
    var form = $('.content').find('form');
    
    if(form.find('#patients-email').val() === '' && form.find('#patients-phone').val() === ''){
        document.getElementById("patients-email").setCustomValidity(t_js('email_or_phone_must_be_filled'));
    } else {
        document.getElementById("patients-email").setCustomValidity("");
    }
    
}