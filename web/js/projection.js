$('.content').on('change','.mainListTestForProjection',function () {
    $.ajax({
        url: '/projection/view',
        type: 'GET',
        data: {mainTestId:$('.mainListTestForProjection').val()},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {
            $('.content-wrapper .content').html(msg);
        }
    });
});

$('.content').on('change','.mainProjectionImageType',function () {
    changeImageType();

    $.ajax({
        url: '/projection/set-img-type',
        type: 'POST',
        data: {projectionImgType:$(this).val()},
        success: function (msg) {

        }
    });
});

$('.content').on('keyup','.addedDeseases>li>input',function (e) {
    if(e.keyCode === 13){
        $(this).trigger("enterKey");
    }
});

$('.addedDeseases>li>input').bind('enterKey',function(e){
    $(this).parent().find('.editPrjDesease').removeClass('fa-check');
    $(this).parent().find('.editPrjDesease').addClass('fa-edit');
    $(this).removeClass('editing');
    $(this).prop('disabled', true);
});

$('.content').on('click','.deletePrjDesease',function () {
    var infoDisease = $(this).closest('li');

    $.ajax({
        url: '/projection/remove-disease',
        type: 'POST',
        data: {id:infoDisease.data('id')},
        success: function (msg) {
            infoDisease.remove();
        }
    });
});

$('.content').on('click','.showMapZone',function () {

    var patientGender = 'M';
    if ($("meta[name=patientGender]").attr("content") === '2' ) {
        patientGender = 'W';
    }

    if ($(this).is(":checked")) {
        var fullFront = '<img class="backgroundFull" src="images/projection/'+patientGender+'/front/full.png">';
        var fullBack = '<img class="backgroundFull" src="images/projection/'+patientGender+'/back/full.png">';
        var fullHead = '<img class="backgroundFull" src="images/projection/'+patientGender+'/head/full.png">';

        $('.layersFront').append(fullFront);
        $('.layersBack').append(fullBack);
        $('.layersHead').append(fullHead);
    }
    else {
        $('.backgroundFull').remove();
    }

});

$('.content').on('click','.addToReportPrj',function () {

    var infoDisease = $('.content').find('#currentPrjTtile p');

    var title = infoDisease.data('title');
    var area = infoDisease.data('area');
    var point = infoDisease.data('point');

    var testId = $('.content').find('.mainListTestForProjection').val();

    var blDeseases = $('.content').find('.addedDeseases');

    var flAdd = 1;

    blDeseases.find('li').each(function () {
       if($(this).data('area') === area && $(this).data('point') === point){
           modalAlert('danger',t_js('attention'),'<p>'+t_js('disease_is_added_already')+'</p>','');

           flAdd = 0;
       }
    });


    if(flAdd === 1){
        $.ajax({
            url: '/projection/add-disease',
            type: 'POST',
            data: {testId:testId,title:title,area:area,point:point},
            success: function (msg) {
                if(msg.state === 'success'){
                    var li =
                        '<li data-id="'+msg.id+'" data-area="'+area+'"  data-point="'+point+'">' +
                        '<input type="text" value="'+title+'" disabled>' +
                        '<i class="fa fa-fw fa-close deletePrjDesease pull-right"></i>' +
                        '<i class="fa fa-fw fa-edit editPrjDesease pull-right"></i>' +
                        '</li>';

                    blDeseases.append(li);
                }

            }
        });
    }

});

$('.content').on('click','.editPrjDesease',function () {

    var inpt = $(this).parent().find('input');

    if ($(this).hasClass('fa-check')) {
        $(this).removeClass('fa-check');
        $(this).addClass('fa-edit');
        inpt.removeClass('editing');
        inpt.prop('disabled', true);

        var testId = $('.content').find('.mainListTestForProjection').val();
        var title = inpt.val();
        var infoDisease = $(this).closest('li');
        var area = infoDisease.data('area');
        var point = infoDisease.data('point');

        $.ajax({
            url: '/projection/update-disease',
            type: 'POST',
            data: {testId:testId,title:title,area:area,point:point},
            success: function (msg) {

            }
        });

    } else {
        $(this).addClass('fa-check');
        $(this).removeClass('fa-edit');
        inpt.addClass('editing');

        inpt.prop('disabled', false);
        inpt.focus();
    }

});

$('.content').on('click','.blProjection area',function () {
    var area = $(this).data('area');
    var point = $(this).data('point');
    var title = $(this).data('title');

    var patientGender = 'M';
    if ($("meta[name=patientGender]").attr("content") === '2' ) {
        patientGender = 'W';
    }


    let videoСontainer = $('.content').find(".currentPrjVid video");
    videoСontainer.find('source[type="video/webm"]').attr({src: document.location.origin + '/images/projection/'+patientGender+'/video/'+area+'/'+point+'.webm'});
    videoСontainer.find('source[type="video/mp4"]').attr({src:document.location.origin + '/images/projection/'+patientGender+'/video/'+area+'/'+point+'.mp4'});
    videoСontainer[0].load();


    $('.prjTtile').remove();

    // тут если несколько названий в цикле собираешь пэшки по всем точкам а видосик показываем любой Женя сказал
    // для этого при сборки запули в area какие нить data  а тут чеерез this их выдерни

    var p = '<p class="text-center prjTtile" data-title="'+title+'" data-area="'+area+'" data-point="'+point+'" >'+title+'</p>';

    $('#currentPrjTtile').append(p);

    $('.addToReportPrj').removeClass('hidden');
});

function changeImageType() {

    var patientGender = 'M';
    if ($("meta[name=patientGender]").attr("content") === '2' ) {
        patientGender = 'W';
    }

    var blProjection = $('.content').find('.blProjection');

    if ($('.content').find('.mainProjectionImageType option:selected').val() === '1') {
        blProjection.find( ".frontMainImage>.backgroundPrj" ).attr('src','images/projection/'+patientGender+'/front/0.png');
        blProjection.find( ".backMainImage>.backgroundPrj" ).attr('src','images/projection/'+patientGender+'/back/0.png');
        blProjection.find( ".headMainImage>.backgroundPrj" ).attr('src','images/projection/'+patientGender+'/head/0.png');
    }
    else {
        blProjection.find( ".frontMainImage>.backgroundPrj" ).attr('src','images/projection/'+patientGender+'/front/shema.png');
        blProjection.find( ".backMainImage>.backgroundPrj" ).attr('src','images/projection/'+patientGender+'/back/shema.png');
        blProjection.find( ".headMainImage>.backgroundPrj" ).attr('src','images/projection/'+patientGender+'/head/shema.png');
    }
}