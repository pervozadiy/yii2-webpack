// переменные на фолль

var allDataVoll = [];
var tmpDataVoll = [];
var realNewPoint = false; 
var interactive_plot_voll;

var currentTrace;

// переменные на индикатор ОП
var gauge;
var currentOP = 0;
var opInterval = 0;
var lastOP = {
    timeStamp: 'N/A',
    value: 0
};
var isModalOpen = false;


var opSeans = [];

var plotOptionVoll = {
    grid  : {
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor  : '#f3f3f3',
        backgroundColor: {colors:["#cf0801","#e99f01","#DDCF02","#73c501","#018310","#0084a2","#005ed4","#0121d3","#3c05da","#5e09dd"]}
    },
    series: {
        lines : {show:true, lineWidth: 3},shadowSize: 5
    },
    colors: ["#ffeb46","#759ecd"],
    yaxis : {
        min : 0,
        max : 100,
        font: {
                size: 1,
                color: "#FFF"
            }
    },
    xaxis : {
        show: null,
        font: {
                size: 1,
                color: "#FFF"
            },
        max:247,
        min:0
    }
};

function updateGradVoll(arg) {

    var argArray = [];
    var average = 0;
    var averageIndex = 0;
    var argMax = 0;

    for (let index = 0; index < arg.length-1; index++) {
        if (arg[index][1]>0) {
            argArray.push(arg[index][1]);
            average += Math.round(arg[index][1]);
            averageIndex++;
            if (arg[index][1]>argMax) {
                argMax = Math.round(arg[index][1]);
            }
        }
    }
    
    var argMin = Math.round(average/averageIndex);
    var minValue = 188 - argMin*1.88;
    var maxValue = 188 - argMax*1.88;
    var diffValue = argMax-argMin;
    var diffValuePos = 188 - diffValue*1.88;

    $('.blueGrade').css("top", maxValue+'px' );
    $('.redGrade').css( "top",  minValue+'px' );
    //  $('.centerGrade').css( "top",  ((minValue+maxValue)/2) +'px' )
    $('.centerGrade').css( "top",  diffValuePos +'px' )

    $('.leftLegend').html(Math.round(argMax));
    // $('.centerLegend').html(Math.round(((argMax+argMin)/2)));
   $('.centerLegend').html(Math.round(diffValue));
    $('.rightLegend').html(Math.round(argMin));


    $('#speedBlue').removeClass();
    $('#speedRed').removeClass();

    $('#speedBlue').addClass('speedBlue'+argMin);
    $('#speedRed').addClass('speedRed'+argMin);
    $('#speedText').html(argMin);
}

$('.content').on('click','.clearCurrentTrace',function () {

    $('.ulTrace').empty();

    $('.traceEP>.row>div').removeClass('inTrace');

});

$('.content').on('click','.removeCurrentTrace',function () {
    var trackId = $('.content').find('#trackId').val();

    if(trackId === ''){
        modalAlert('danger',t_js('attention'),'<p>'+t_js('no_track_selected')+'</p>','');
        return false;
    }

    $.ajax({
        url: '/voll-track/remove-track',
        method: 'POST',
        data:{
            id:trackId
        },
        success: function(msg){
            if(msg.state !== 'danger'){
                loadContent('/voll-track/editor-track');
            }

            modalAlert(msg.state,msg.title,msg.content,msg.footer);
        }
    });


});

$('.content').on('click','.ulTrace>li',function () {

    var side = $(this).data("side");
    var position = $(this).data("position");
    var channel = $(this).data("channel");
    var traceEl = $(this);

    var previous = $('.traceEP>.row>div').filter(function() {

        if ($(this).data("side")==side && $(this).data("position")==position  && $(this).parent().data( "channel" )==channel ) {
            return $(this);
        }
    });

    previous.removeClass('inTrace');

    traceEl.remove();

});

$('.content').on('click','.traceEP>.row>div',function () {

    var channelDotSrc;

    if ($( this ).data( "side" )) {

        if (!$( this ).hasClass('inTrace')) {
            $( this ).addClass('inTrace');
            $('.ulTrace').append('<li data-channel="'+$( this ).parent().data( "channel" )+'" data-side="'+$( this ).data( "side" )+'" data-position="'+$( this ).data( "position" )+'">'+$( this ).parent().data( "channel" )+' '+$( this ).data( "side" )+' '+$( this ).data( "position" )+'</li>');
        }

        $('#organPic').attr("src","../images/electropuncture/voll/organs/M/"+$( this ).data( "side" )+"_"+$( this ).parent().data( "channel" )+"_"+$( this ).data( "position" )+".gif");

        $('#currentDot').removeClass();
        $('#currentDot').addClass($( this ).data( "side" )+"_"+$( this ).parent().data( "channel" )+"_"+$( this ).data( "position" ));

        if ($( this ).data( "dotpic" )) {
            channelDotSrc = "../images/electropuncture/voll/dots/"+$( this ).parent().data( "channel" )+"_"+$( this ).data( "side" )+"_"+$( this ).data( "dotpic" )+".png"

        } else {
            channelDotSrc = "../images/electropuncture/voll/dots/"+$( this ).parent().data( "channel" )+"_"+$( this ).data( "side" )+".png"
        }

        $('#channelPic').attr("src",channelDotSrc);

        $('.currentDotChannel').removeClass('currentDotChannel');
        $( this ).addClass('currentDotChannel');

        $('.currentChannel').removeClass('currentChannel');
        $( this ).parent().addClass('currentChannel');

        $('.rowNumbers>div').removeClass('currentPosition');

        var numberCollum = '.rowNumbers>div:nth-child(';

        if ($( this ).data( "side" )=='R') {
            numberCollum += 15-$( this ).data( "position" );
            numberCollum +=')'
            $(numberCollum).addClass('currentPosition');

        } else {
            numberCollum += 15+$( this ).data( "position" );
            numberCollum +=')'
            $(numberCollum).addClass('currentPosition');
        }
    }

});

function setColorAndDataPoint() {

    $('.currentDotChannel').data('value-max',$('.leftLegend').html());
    $('.currentDotChannel').data('value-last',$('.rightLegend').html());
    $('.currentDotChannel').data('value-avg',$('.centerLegend').html());
    
    var valMax = $('.currentDotChannel').data('value-max');

    $('.currentDotChannel').removeClass().addClass('currentDotChannel');

    if ( valMax >= 90) {
        $('.currentDotChannel').addClass('vollBgRed');
    } else if (valMax >= 80) {
        $('.currentDotChannel').addClass('vollBgOrange');
    } else if (valMax >= 66) {
        $('.currentDotChannel').addClass('vollBgYellow');
    } else if (valMax >= 50) {
        $('.currentDotChannel').addClass('vollBgGreen');
    } else if (valMax >= 35) {
        $('.currentDotChannel').addClass('vollBgBlue');
    } else if (valMax >= 20) {
        $('.currentDotChannel').addClass('vollBgDarkBlue');
    } else if (valMax < 20) {
        $('.currentDotChannel').addClass('vollBgPurple');
    }

    var heightDot = valMax - parseInt($('.currentDotChannel').data('value-last'));

    if ( heightDot <= 5) {
        $('.currentDotChannel').addClass('vollBgHeight1');
    } else if (heightDot >= 6 && heightDot <= 11) {
        $('.currentDotChannel').addClass('vollBgHeight2');
    } else if (heightDot >= 12 && heightDot <= 21) {
        $('.currentDotChannel').addClass('vollBgHeight3');
    } else if (heightDot > 21) {
        $('.currentDotChannel').addClass('vollBgHeight4');
    }

}

function getDataVollTest(request) {
    setLogger(request);
    request = JSON.parse(request);  

    if (request.newPoint == "true") {
        // переход на новую точку
        
        globalCurrentTrackPoint++;

        if (typeof globalVollTrack[globalCurrentTrackPoint].channel !=='undefined') {
            var areaEPTrack = $('.content').find('.areaEP');

            areaEPTrack.find('[data-channel="'+globalVollTrack[globalCurrentTrackPoint].channel+'"]').find('[data-side="'+globalVollTrack[globalCurrentTrackPoint].side+'"][data-position="'+globalVollTrack[globalCurrentTrackPoint].position+'"]').click();

         }
         tmpDataVoll = [];
         realNewPoint = false;

    } else {

        if (request.numberMeasure == 0) {
            realNewPoint = false;
            interactive_plot_voll = $.plot('#interactive_plot_voll', [0,0] , plotOptionVoll);
            allDataVoll = []; 
            tmpDataVoll = [];
        }

            tmpDataVoll.push(request.data[0]);
          
       // for (var i = 0; i < request.data.length; i++) {
            if (tmpDataVoll.length >3) {
                allDataVoll.push([allDataVoll.length, request.data[0]]);
            } 
       // }    

        if ((request.numberMeasure > 0) && (request.numberMeasure < 250)) {

            if (tmpDataVoll.length >3) {
                if (!isModalOpen) {
                    interactive_plot_voll.setData([allDataVoll]);
                    interactive_plot_voll.draw();
                    updateGradVoll(allDataVoll); 
                    setColorAndDataPoint();
                } else {
                    if (request.numberMeasure < 150) {
                        updateOP(allDataVoll,request.numberMeasure);
                    }
                }
            }

        }

    }
}

$('.content').on('click','.getTrackVoll',function () {
    var trackItem = $(this);

    var trackId = trackItem.data('track-id');

    var trackInfo = getTrackVoll(trackId);

    if(trackInfo.trackId){
        trackItem.closest('ul').find('li').each(function () {
            $(this).removeClass('active');
            if($(this).find('a').data('track-id') == trackInfo.trackId){
                $(this).addClass('active');
            }
        });

        globalVollTrack = trackInfo.track;
        globalCurrentTrackPoint = 0;

        setLogger(JSON.stringify(globalVollTrack));
    }

});

function setDefaultTrack(){
    var trackId = 2;
    var trackItem = $('.content').find('.menuVollTrack [data-track-id="'+trackId+'"]');

    var trackInfo = getTrackVoll(trackId,true);

    if(trackInfo.trackId){
        trackItem.closest('ul').find('li').each(function () {
            $(this).removeClass('active');
            if($(this).find('a').data('track-id') == trackInfo.trackId){
                $(this).addClass('active');
            }
        });

        globalVollTrack = trackInfo.track;

        setLogger(JSON.stringify(globalVollTrack));
    }
}

$('.content').on('click','.saveVollTest',function () {

    var dataTest = [];
    $('.content').find('.areaEP .channel').each(function () {
        var channel = $(this).data('channel');

        $(this).find('.point').each(function () {
           var side = $(this).data('side');
           var position = $(this).data('position');
           var max = $(this).data('value-max');
           var last = $(this).data('value-last');
           var avg = $(this).data('value-avg');

           dataTest.push({
               channel: channel,
               side: side,
               position: position,
               max: max,
               last: last,
               avg: avg
           });
        });
    });

    if(dataTest.length>0){
        $.ajax({
            url: '/voll-tests/add-test',
            method: 'POST',
            data:{
                dateTime:getDateTimeNow(),
                points:JSON.stringify(dataTest),
                conclusion:$('.content').find('.vollTestcConclusion').val()},
            success: function(msg){
                if(msg.state != 'danger'){
                    loadContent('/electropuncture/voll');
                }

                modalAlert(msg.state,msg.title,msg.content,msg.footer);
            }
        });
    }

});

$('.content').on('click','.saveVollTrack',function () {

    var dataPoint = [];

    var trackMenu = $('.content').find('.menuVollTrack');
    var titleTrack = $('.content').find('#traceName').val();
    var idTrack = $('.content').find('#trackId').val();

    if(titleTrack == ''){
        modalAlert('danger',t_js('attention'),'<p>'+t_js('route_name_empty')+'</p>','');
        return false;
    }

    $('.content').find('.ulTrace li').each(function () {
        dataPoint.push({
            'channel' : $(this).data('channel'),
            'side' : $(this).data('side'),
            'position' : $(this).data('position')
        });
    });

    if(dataPoint.length>0){
        $.ajax({
            url: '/voll-track/save-track',
            method: 'POST',
            data:{
                id:idTrack,
                title:titleTrack,
                track:JSON.stringify(dataPoint)
            },
            success: function(msg){
                if(msg.state != 'danger'){
                    $('.content').find('#trackId').val(msg.trackId);

                    if(idTrack==''){
                        trackMenu.prepend('<li><a href="javascript:void(0);" data-track-id="'+msg.trackId+'" class="getTrackVollEditor">'+titleTrack+'</a></li>');

                        trackMenu.find('li').each(function () {
                            $(this).removeClass('active');
                            if($(this).find('a').data('track-id') == msg.trackId){
                                $(this).addClass('active');
                            }
                        });
                    }
                }

                modalAlert(msg.state,msg.title,msg.content,msg.footer);
            }
        });
    } else {
        modalAlert('danger',t_js('attention'),'<p>'+t_js('no_points_are_added_rack')+'</p>','');
        return false;
    }
});

$('.content').on('click','.getTrackVollEditor',function () {
    var trackItem = $(this);

    var trackId = trackItem.data('track-id');

    var trackInfo = getTrackVoll(trackId);

    if(trackInfo.trackId){

        trackItem.closest('ul').find('li').each(function () {
            $(this).removeClass('active');
            if($(this).find('a').data('track-id') == trackInfo.trackId){
               $(this).addClass('active');
            }
        });

        $('.content').find('#trackId').val(trackInfo.trackId);
        $('.content').find('#traceName').val(trackInfo.trackTitle);


        var listTrack = $('.content').find('.ulTrace');

        listTrack.html('');

        var listPointsTrack = $('.content').find('.traceEP');
        for (var key in trackInfo.track) {
            let tempChannel = trackInfo.track[key]['channel'];
            let tempSide = trackInfo.track[key]['side'];
            let tempPosition = trackInfo.track[key]['position'];

            listTrack.append('<li data-channel="'+tempChannel+'" data-side="'+tempSide+'" data-position="'+tempPosition+'">'+tempChannel+' '+tempSide+' '+tempPosition+'</li>');

            listPointsTrack.find('[data-channel="'+tempChannel+'"] [data-side="'+tempSide+'"][data-position="'+tempPosition+'"]').addClass('inTrace');
        }
    }

    console.log(trackInfo);
});

function getTrackVoll(trackId,hideModalAlert) {
    var response = {};

    if(trackId != ''){
        $.ajax({
            url: '/voll-track/get-track',
            method: 'POST',
            data:{id:trackId},
            async: false,
            success: function(msg){

                if(hideModalAlert != true){
                    modalAlert(msg.state,msg.title,msg.content,msg.footer);
                }

                if(msg.state != 'danger'){
                    response = {
                        trackId:msg.trackId,
                        trackTitle:msg.trackTitle,
                        track:msg.track
                    };
                }
            }
        });

        return response;
    }
}

$('.content').on('shown.bs.modal','#op-modal',function () { 
    var opts = {
        lines: 10,
        angle: -0.32,
        lineWidth: 0.09,
        radiusScale: 1,
        pointer: {
            length: 0.4,
            strokeWidth: 0.029,
            color: '#77202A'
        },
        colorStart: '#6FADCF',
        colorStop: '#8FC0DA',
        strokeColor: '#E0E0E0',
        generateGradient: true,
        staticLabels: {
            font: "10px sans-serif",
            labels: [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
            color: "#77202A",
            fractionDigits: 0
        },
        renderTicks: {
            divisions: 10,
            divWidth: 2.1,
            divLength: 0.60,
            divColor: '#77202A',
            subDivisions: 10,
            subLength: 0.52,
            subWidth: 1,
            subColor: '#FFFFFF'
        },
        staticZones: [
                    {strokeStyle: "#390038", min: 1, max: 40},
                    {strokeStyle: "#014988", min: 41, max: 60},
                    {strokeStyle: "#5F93C5", min: 61, max: 79},
                    {strokeStyle: "#028000", min: 80, max: 82},
                    {strokeStyle: "#FFDF00", min: 83, max: 90},
                    {strokeStyle: "#CC2004", min: 91, max: 100}
                ]
        };
    var target = document.getElementById('gaugeOP');
    gauge = new Gauge(target);
    gauge.setOptions(opts);
    gauge.maxValue = 100;
    gauge.animationSpeed = 32;
    gauge.set(0);
    //updateOP([[65,65]],149);
    updateOPSeans();
    $('#speedTextOP').html(0);
    $('.content').find('#textOP').html(textsOP[0]);
    isModalOpen = true;
});

function updateOPSeans() {
    $('.currentOP').text(lastOP.timeStamp + ' - ' + lastOP.value);
    $('#opTooltip').attr('title',textsOP[opInterval]);
}

function updateOP(arg, measure) {

    var argMax = 0;

    for (let index = 0; index < arg.length-1; index++) {
        if (arg[index][1]>0) {
            if (arg[index][1]>argMax) {
                argMax = Math.round(arg[index][1]);
            }
        }
    }

    currentOP = argMax;
    gauge.set(currentOP);
    $('#speedTextOP').html(currentOP);

    if ( currentOP >= 91) {
        opInterval = 6;
    } else if (currentOP >= 83) {
        opInterval = 5;
    } else if (currentOP >= 80) {
        opInterval = 4;
    } else if (currentOP >= 61) {
        opInterval = 3;
    } else if (currentOP >= 41) {
        opInterval = 2;
    } else if (currentOP >= 1) {
        opInterval = 1;
    }

    $('.content').find('.lastValueOP').text(' ('+currentOP+')');

    if (measure == 149) {
        var ts = new Date();

        lastOP.timeStamp = 
        (ts.getDate() > 9 ? ts.getDate() : ('0' + ts.getDate())) + '/'
        + ((ts.getMonth() + 1) > 9 ? (ts.getMonth() + 1) : ('0' + (ts.getMonth() + 1))) + '/'
        + ts.getFullYear() + ' '
        + (ts.getHours() > 9 ? ts.getHours() : ('0' + ts.getHours())) + ':'
        + (ts.getMinutes() > 9 ? ts.getMinutes(): ('0' + ts.getMinutes())) + ':'
        + (ts.getSeconds() > 9 ? ts.getSeconds() : ('0' + ts.getSeconds()));

        lastOP.value = currentOP;
        $('.content').find('#textOP').html(textsOP[opInterval]);
        updateOPSeans();
    }
}

$('.content').on('hidden.bs.modal','#op-modal',function () { 
    isModalOpen = false;
});


$('.content').on('click','.btnPatientCard',function () {
    $.ajax({
        url: 'voll-tests/get-modal-patient-card',
        method: 'GET',
        success: function(msg){
            if(msg.state === 'danger'){
                modalAlert(msg.state,msg.title,msg.content,msg.footer);
            } else {
                let modalPatientCard = $('.content').find('#patient-card-modal');

                modalPatientCard.find('#patientCardDeseaseHistiory').val(msg.medical_history);
                modalPatientCard.find('#patientCardDiagnosisList').val(msg.diagnosis).select2();
                modalPatientCard.find('#patientCardNote').val(msg.note);

                modalPatientCard.modal('show');
            }
        }
    });
});


$('.content').on('click','.patientCardSave',function () {
    var form = $('.content').find('.formVegaPatientCard');
    var data = form.serialize();

    $.ajax({
        url: 'voll-tests/save-patient-card',
        type: 'POST',
        data: data,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            modalAlert(msg.state,msg.title,msg.content,msg.footer);
        }
    });
});