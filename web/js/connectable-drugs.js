$(document).ready(function() {

    // save settings firm
    $('.content-wrapper .content').on('submit','.formConnectableDrugs',function (e) {
        e.preventDefault();

        var form = $(this);
        var data = form.serialize();

        $.ajax({
            url: 'connectable-drugs/view',
            type: 'POST',
            data: data,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){
                $('.content-wrapper .content').html(msg);
            }
        });
    });

});

