var timerIntervalBrt;

var interactive_plot_brt;


$('.content').on('dblclick','.brtMultiple',function () {
    var selectedItem = $(this).find(':selected');
    var value = selectedItem.val();
    var type = selectedItem.data('brttype');

    var flAddItem = 1;

    $('.content').find('.listProgramsBRT tbody tr').each(function () {
        if($(this).find('input[name="programBrt[value][]"]').val() == value){
            flAddItem = 0;
            modalAlert('danger',t_js('attention'),'<p>'+t_js('this_program_was_add')+'!</p>','');
        }
    });

    if($('.content').find('.listProgramsBRT tbody tr').length == 5){
        flAddItem = 0;
        modalAlert('danger',t_js('attention'),'<p>'+t_js('can_not_more_than_5_programs')+'!</p>','');
    }

    if(flAddItem == 1){
        var item =
            '<tr>' +
                '<td>' +
                    type +
                    '<input type="hidden" name="programBrt[value][]" value="' + value + '">' +
                    '<input type="hidden" name="programBrt[type][]" value="' + type + '">' +
                '</td>' +
                '<td>' +
                    '<select class="form-control brt-select" name="programBrt[time][]">' +
                        '<option value="0.5">0.5</option>' +
                        '<option value="1">1</option>' +
                        '<option value="2">2</option>' +
                        '<option value="3">3</option>' +
                        '<option value="4" selected="selected">4</option>' +
                        '<option value="5">5</option>' +
                        '<option value="6">6</option>' +
                        '<option value="7">7</option>' +
                        '<option value="8">8</option>' +
                        '<option value="9">9</option>' +
                        '<option value="10">10</option>' +
                        '<option value="11">11</option>' +
                        '<option value="12">12</option>' +
                        '<option value="13">13</option>' +
                        '<option value="14">14</option>' +
                        '<option value="15">15</option>' +
                        '<option value="16">16</option>' +
                        '<option value="17">17</option>' +
                        '<option value="18">18</option>' +
                        '<option value="19">19</option>' +
                        '<option value="20">20</option>' +
                    '</select>' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="removeItemProgramBrt"><i class="fa fa-trash"></i></a>' +
                '</td>' +
            '</tr>';

        $('.content').find('.listProgramsBRT tbody').append(item);

        getTotalTimePrograms($('.content').find('.listProgramsBRT tbody select[name="programBrt[time][]"]').last());
    }
});

$('.content').on('click','.removeItemProgramBrt',function () {
   $(this).closest('tr').remove();
    getTotalTimePrograms('');
});

$('.content').on('change','select[name="programBrt[time][]"]',function () {
    getTotalTimePrograms($(this));
});

function getTotalTimePrograms(selectTime) {
    var totalTime = 0;
    $('.content').find('select[name="programBrt[time][]"]').each(function () {
        totalTime += parseFloat($(this).find(':selected').val());
    });

    if(totalTime > 20){
        modalAlert('danger',t_js('attention'),'<p>'+t_js('time_limit_exceeded')+'!</p>','');
        selectTime.val('0.5');
        getTotalTimePrograms(selectTime);
        return false;
    }

    $('.content').find('.listProgramsBRT .totalTime span').text(totalTime);
    $('.content').find('#curMinutes').text(totalTime);

    if ((totalTime - Math.floor(totalTime)) == 0.5) {
        $('.content').find('#curMinutes').text(Math.floor(totalTime));
        $('.content').find('#curSeconds').text('30');
    } else {
        $('.content').find('#curSeconds').text('0');
    }

    return totalTime;
}

function getBrtPrograms() {
    var programs = [];

    $('.content').find('tbody tr').each(function () {
        var idxMeridian = $(this).find('input[name="programBrt[value][]"]').val();
        var timeMeridian = parseFloat($(this).find('select[name="programBrt[time][]"]').val()) * 60;

        programs.push({idxMeridian:idxMeridian,timeMeridian:timeMeridian});
    });

    return programs;
}

var data = [], totalPoints = 100;

function getPlotData(val) {

    if (data.length > 100)
        data = data.slice(1)
        data.push(val)

    var res = []
    for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]]);
    }

    return res;
}

function startBrtTimer(duration) {
    var timer = duration, minutes, seconds;

    if (typeof timerIntervalBrt !== 'undefined'){
        clearInterval(timerIntervalBrt);
    }

    var pointControl = 0;

    timerIntervalBrt = setInterval(function () {
        minutes = parseInt(timer / 60, 10);
        seconds = parseInt(timer % 60, 10);

        minutes = minutes < 10 ? "0" + minutes : minutes;
        seconds = seconds < 10 ? "0" + seconds : seconds;

        $('#curMinutes').text(minutes);
        $('#curSeconds').text(seconds);

        if(pointControl === 20 && data.length == 0){
            modalAlert('danger',t_js('error'),'<p>'+t_js('session_interrupted_because_there_no_feedback_from_the_body_restart_session')+'!</p>','');
            actionStopBRT();
        }
        pointControl++;

        if (--timer < 0) {
            modalAlert('success',t_js('attention'),'<p>'+t_js('session_is_closed')+'!</p>','');
            actionStopBRT();
            finishBrt();
        }
    }, 1000);


}

function getDataBrtTest(request) {
    setLogger(request);
    request = JSON.parse(request);

    if(request.error == 'ok'){
        $('.brt-dot').addClass('hidden');
        $('.brt'+request.r).removeClass('hidden');
        $('.brt'+request.l).removeClass('hidden');
        $('#curMeridian').text(request.meridian);

        interactive_plot_brt.setData([getPlotData(request.value)]);
        interactive_plot_brt.draw();

    } else if(request.error == 'finish') {
        modalAlert('success',t_js('attention'),'<p>'+t_js('session_is_closed')+'!</p>','');

        actionStopBRT();

        finishBrt();
    } else {
        displaysErrorFromDevices({name:request.error,numberMeasure:request.numberMeasure});

        actionStopBRT();
    }
}

function finishBrt() {
    $.ajax({
        url: '/patients/set-block-protection-express-test',
        success: function(msg){
            //modalAlert(msg.state,msg.title,msg.content,msg.footer);
        }
    });
}

function getControlDataBrt(request){
    setLogger(request);
  
    request = JSON.parse(request); 

    if (request.error == "ok") {
        $('#start').prop('disabled', false);
        $('.brtAutomaticSelection').prop('disabled', false);
        $('.brtHarmonization').prop('disabled', false);
        $('.brt-dot').addClass('hidden');
        modalAlert('success',t_js('attention'),'<p>'+t_js('instrument_was_successfully_monitored')+'!</p>','');
        $('.controlButton').prop('disabled', false);
    } else {
        displaysErrorFromDevices({name:request.error,numberMeasure:request.numberMeasure});

        $('.controlButton').prop('disabled', false);
        $('.brt-dot').addClass('hidden');
    }
}

/**
 * check active device
 */
function activeControlButton(){
    var flActiveDevice = $('meta[name="deviceState"]').attr('content');

    if(flActiveDevice === '1'){
        $('.content').find('.controlButton').removeAttr('disabled');
    } else {
        $('.content').find('.controlButton').attr({disabled:true});
    }
}

$('.content').on('click','.brtHarmonization',function () {
    $('.brt-dot').addClass('hidden');

    var device = $('meta[name="device"]').attr("content");
    var deviceUserId = $('meta[name="deviceNumber"]').attr("content");
    var patientId = $('meta[name="patientId"]').attr("content");

    if(patientId==''){
        modalAlert('danger',t_js('error'),'<p>'+t_js('patient_was_not_selected_for_testing')+'!</p>','');
        return false;
    }

    startBrtTimer(1200);

    var programsHarmonization = [
        {idxMeridian:1,timeMeridian:300},
        {idxMeridian:2,timeMeridian:300},
        {idxMeridian:3,timeMeridian:300},
        {idxMeridian:4,timeMeridian:300}
    ];

    sendHash({action:'start',method:'brt',nameDevice:device,numberDevice:deviceUserId,data:programsHarmonization,callbackFunction:'getDataBrtTest'});

    //interactive_plot_brt.getOptions().grid.markings.push({ xaxis: { from: 60, to: 60 }, yaxis: { from: 0, to: 20 }, color: "#77202A", lineWidth: 3 });
    //interactive_plot_brt.setupGrid();
    //interactive_plot_brt.draw();

    $('.stopBRT').prop('disabled', false);

    setLogger(JSON.stringify({action:'start',method:'brt',nameDevice:device,numberDevice:deviceUserId,data:programsHarmonization,callbackFunction:'getDataBrtTest'}));

});

$('.content').on('click','.controlButton',function () {

    $('.brt-dot').removeClass('hidden');

    var device = $('meta[name="device"]').attr("content");
    var deviceUserId = $('meta[name="deviceNumber"]').attr("content");

    sendHash({action:'control',method:'brt',nameDevice:device,numberDevice:deviceUserId,callbackFunction:'getControlDataBrt'});

    $('.controlButton').prop('disabled', true);

    setLogger(JSON.stringify({action:'control',method:'brt',nameDevice:device,numberDevice:deviceUserId,callbackFunction:'getControlDataBrt'}));

});

$('.content').on('click','.brtAutomaticSelection',function () {
    $('.brt-dot').addClass('hidden');
    var testId = $('.content').find('.calculationTest').val();

    if(testId == ''){
        modalAlert('danger',t_js('error'),'<p>'+t_js('to_compile_complex_no_test_was_selected')+'!</p>','');
        return false;
    }

    $.ajax({
        url: '/brt/automatic-selection-programs',
        method: 'POST',
        data:{id:testId},
        success: function(msg){
            if(msg.state != 'danger'){

                var item = '';

                msg.programs.forEach(function (element) {
                    var optionTime = '<option value="0.5">0.5</option>';
                    for(var iTime=1;iTime<=20;iTime++){
                        var selected = '';
                        if(iTime == element.time){
                            selected = ' selected="selected"';
                        }

                        optionTime += '<option value="'+iTime+'" '+selected+'>'+iTime+'</option>';
                    }

                    item +=
                        '<tr>' +
                            '<td>' +
                                element.title +
                                '<input type="hidden" name="programBrt[value][]" value="' + element.id + '">' +
                                '<input type="hidden" name="programBrt[type][]" value="' + element.title + '">' +
                            '</td>' +
                            '<td>' +
                                '<select class="form-control brt-select" name="programBrt[time][]">' +
                                    optionTime +
                                '</select>' +
                            '</td>' +
                            '<td>' +
                                '<a href="javascript:void(0);" class="removeItemProgramBrt"><i class="fa fa-trash"></i></a>' +
                            '</td>' +
                        '</tr>';
                });

                $('.content').find('.listProgramsBRT tbody').html('').append(item);

                getTotalTimePrograms($('.content').find('.listProgramsBRT tbody select[name="programBrt[time][]"]').last());
            }

            modalAlert(msg.state,msg.title,msg.content,msg.footer);
        }
    });
});

//REALTIME TOGGLE
$('.content').on('click','#realtime .btn',function () {
    blcontrolDevice = $(this).closest('.controlDevice');

    var device = $('meta[name="device"]').attr("content");
    var deviceUserId = $('meta[name="deviceNumber"]').attr("content");
    var patientId = $('meta[name="patientId"]').attr("content");

    if ($(this).data('toggle') === 'on') {
        if(patientId==''){
            modalAlert('danger',t_js('error'),'<p>'+t_js('patient_was_not_selected_for_testing')+'!</p>','');
            return false;
        }

        var brtPrograms = getBrtPrograms();

        if (brtPrograms.length == 0 ) {
            modalAlert('danger',t_js('error'),'<p>'+t_js('for_testing_select_meridian')+'!</p>','');
            return false;
        }

        sendHash({action:'start',method:'brt',nameDevice:device,numberDevice:deviceUserId,data:brtPrograms,callbackFunction:'getDataBrtTest'});

        var currentTime = $('#curMinutes').text() * 60 + parseInt($('#curSeconds').text());
        startBrtTimer(currentTime);

        setLogger(JSON.stringify({action:'start',method:'brt',nameDevice:device,numberDevice:deviceUserId,data:brtPrograms,callbackFunction:'getDataBrtTest'}));


        $(this).attr({disabled:true});
        blcontrolDevice.find('[data-toggle="off"]').removeAttr('disabled');
    }
    else {
        actionStopBRT();
    }
});

function actionStopBRT() {
    data = [];

    blcontrolDevice = $('.content').find('.controlDevice');

     var deviceUserId = $('meta[name="deviceNumber"]').attr("content");
     var device = $('meta[name="device"]').attr("content");

     sendHash({action:'stop',method:'brt',nameDevice:device,numberDevice:deviceUserId});

     setLogger(JSON.stringify({action:'stop',method:'brt',nameDevice:device,numberDevice:deviceUserId}));

     $('.brt-dot').addClass('hidden');
     if (typeof timerIntervalBrt !== 'undefined'){
         clearInterval(timerIntervalBrt);
     }

     blcontrolDevice.find('[data-toggle="off"]').attr({disabled:true});
     blcontrolDevice.find('[data-toggle="on"]').removeAttr('disabled');

    getTotalTimePrograms($('.content').find('.listProgramsBRT tbody select[name="programBrt[time][]"]').last());
}


function brtStartMagic() {

    activeControlButton()

    interactive_plot_brt = $.plot('#interactive_brt', [0,0], {
        grid  : {
            borderColor: '#f3f3f3',
            borderWidth: 1,
            tickColor  : '#f3f3f3',
            markings:  []
            // [ 
            //     { xaxis: { from: 20, to: 20 }, yaxis: { from: 0, to: 20 }, color: "#77202A", lineWidth: 3 },
            //     { xaxis: { from: 40, to: 40 }, yaxis: { from: 0, to: 20 }, color: "#77202A", lineWidth: 3 },
            //     { xaxis: { from: 60, to: 60 }, yaxis: { from: 0, to: 20 }, color: "#77202A", lineWidth: 3 },
            //     { xaxis: { from: 80, to: 80 }, yaxis: { from: 0, to: 20 }, color: "#77202A", lineWidth: 3 }
            // ]
        },
        series: {
            shadowSize: 0, // Drawing is faster without shadows
            color     : '#3c8dbc'
        },
        lines : {
            fill : true, //Converts the line chart to area chart
            color: '#3c8dbc'
        },
        yaxis : {
            min : 0,
            max : 100,
            show: true
        },
        xaxis : {
            show: true,
            min : 0,
            max : 100
        }            
    }); 

};