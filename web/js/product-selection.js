function getTestForProductSelection(selectedPatientId,flCalculat) {

    var device = $('meta[name="device"]').attr("content");

    $.ajax({
        url: '/tests/get-table-tests-for-product-selection?selectedPatientId='+selectedPatientId+'&device='+device,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(data){
            var table =  $('.content').find('#listSeanceProductSelection');

            table.dataTable().api().clear();
            table.dataTable().api().rows.add(data);
            table.dataTable().api().draw();

            if(flCalculat == 'calculateTest'){
                calculationProductTest(table.find('.btnHandleTestProduct').first());
            }
        }
    });
}

$('.content').on('click','.btnShowCalculateTestProduct',function () {
    showCalculateTestProduct($(this).data('test'));
});

$('.content').on('click','.btnHandleTestProduct',function () {
    calculationProductTest($(this));
});

$('.content').on('change','.mainListTestForProductSelection',function () {
    $.ajax({
        url: '/product-selection/view-test',
        type: 'GET',
        data: {mainTestId:$('.mainListTestForProductSelection').val()},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {
            $('.content-wrapper .content').html(msg);
        }
    });
});

function calculationProductTest(btn) {
    var idTest = btn.data('test');

    $.ajax({
        url: '/calculation-test/calculation-diagnostic-test',
        type: 'POST',
        data: {id:idTest,dateTime:getDateTimeNow()},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            //$('#loading_box').hide();
        },
        success: function (msg) {
            setTimeout(function(){
                $('#loading_box').hide();

                modalAlert(msg.state,msg.title,msg.content,msg.footer);

                if(msg.state != 'danger'){
                    getTestForProductSelection($('meta[name="patientId"]').attr("content"));

                    showCalculateTestProduct(idTest);
                }
            }, 5000);
        }
    });
}

function showCalculateTestProduct(idTest) {
    $.ajax({
        url: '/product-selection/get-test',
        type: 'POST',
        data: {id:idTest},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {


            let blProduct = $('.content').find('.productMultiple');

            let blNegatively =  $('.content').find('.blProduct-negatively');
            let blNeutrally =  $('.content').find('.blProduct-neutrally');
            let blPositively =  $('.content').find('.blProduct-positively');

            let listProduct = $('.content').find('.productMultiple option');
            let countListProduct = listProduct.length;
            var i = 0;
            var timerId = setInterval(function(){
                let optionInfo =  listProduct.eq(i);

                if(i % 10 == 0) {
                    blProduct.scrollTop(optionInfo.offset().top);
                }

                let optionId = optionInfo.val();

                if(typeof msg['negatively'][optionId] != "undefined"){
                    blNegatively.append($('<option>', { value : optionId }).text(msg['negatively'][optionId]));
                    blNegatively.scrollTop(blNegatively.find("option[value="+optionId+"]").offset().top);
                } else if(typeof msg['neutrally'][optionId] != "undefined"){
                    blNeutrally.append($('<option>', { value : optionId }).text(msg['neutrally'][optionId]));
                    blNeutrally.scrollTop(blNeutrally.find("option[value="+optionId+"]").offset().top);
                } else if(typeof msg['positively'][optionId] != "undefined"){
                    blPositively.append($('<option>', { value : optionId }).text(msg['positively'][optionId]));
                    blPositively.scrollTop(blPositively.find("option[value="+optionId+"]").offset().top);
                }


                i++;
                if(countListProduct == i){
                    $('.content').find('.productMultiple').scrollTop(0)
                    clearInterval(timerId)
                }
            }, 20);
        }
    });
}