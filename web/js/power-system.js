
$('.content').on('click','.powerSystemImg span',function () {
    let labelPowerSystem = $(this).data('label');
    let statePowerSystem = $(this).data('state');
    showItemPowerSystem(labelPowerSystem,statePowerSystem);
});

$('.content').on('click','.powerSystemIllustration span',function () {
    let labelPowerSystem = $(this).data('label');
    showItemPowerSystem(labelPowerSystem,'');
});

function showItemPowerSystem(selectedItemPowerSystem,statePowerSystem) {

    let ItemPowerSystem = listPowerSystem[selectedItemPowerSystem];

    $('.contentItemPowerSystem .selectedLabel span').html(ItemPowerSystem.title);

    $('.contentItemPowerSystem .selectedDescription').html(ItemPowerSystem.description);

    if(statePowerSystem == ''){
        statePowerSystem = $('.content').find('.powerSystemImg.powerSystemMainTest span[data-label="'+selectedItemPowerSystem+'"]').data('state');
    }

    if(statePowerSystem != ''){
        let descriptionState = $('.content').find('.definitionPowerSystem td[data-state-description="'+statePowerSystem+'"]').text();
        $('.contentItemPowerSystem .selectedState span').html(descriptionState);
    } else {
        $('.contentItemPowerSystem .selectedState span').html('-');
    }
}

