$('.content').on('click','.btnDisplayListDrugSelectionTest',function () {

    var dataTestId = [];
    var dataTestName = [];

    $('.content').find('.blInfoDrugSelectionTest').html('');

    $('.content').find('.listDrugSelectionTest input:checked').each(
        function () {
            var testId = $(this).val();
            dataTestId.push(testId);

            var testName = $(this).closest('label').find('span').text();
            dataTestName.push(testName);
        }
    );

    $.ajax({
        url: '/drug-selection/get-info-drug-selection',
        type: 'POST',
        data: {dataTestId:dataTestId,dataTestName:dataTestName},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {

            var infoDrug = '';

            msg.forEach(function(item) {
                infoDrug +=
                    '<div class="row drProgress" data-test-id = "'+item.testId+'">' +
                        '<div class="col-md-5">' +
                            item.testName +
                        '</div>' +
                        '<div class="col-md-6">' +
                            '<div class="progress progress-sm">' +
                                '<div class="progress-bar progress-bar-green drugReport-progress" role="progressbar" data-progress-value="'+item.percentageUtility+'">' +
                                '</div>' +
                            '</div>' +
                        '</div>' +
                        '<div class="col-md-1">' +
                            '<span class="badge badgeValue bg-green">0</span>' +
                        '</div>' +
                    '</div>';


                if(item.error.state == 'danger'){
                    modalAlert(item.error.state,item.error.title,item.error.content,item.error.footer);
                }
            });

            $('.content').find('.blInfoDrugSelectionTest').append(infoDrug);

            animateInfoDrugSelectionTest();
        }
    });


});

function animateInfoDrugSelectionTest(){
    $('.content').find('.blInfoDrugSelectionTest .drugReport-progress').each(function () {
        var progresBl = $(this);
        var progressValue = progresBl.data('progress-value');

        progresBl.animate({width:progressValue+'%'},{duration: 1000});

        progresBl.closest('.drProgress').find('.badgeValue').prop('Counter',0).animate({
            Counter: progressValue
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
}
