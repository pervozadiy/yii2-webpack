var baseFrequencyMin = '';
var baseFrequencyMax = '';

// переменные на подбор частот
var oldResVegaFreq = [];
var allDataVegaFreq = [];
var interactive_plot_vega_frequencies;
var pointsVegaFreq = 100;
var baseFreqMarkings = [];
var backgroundColorsPlot = {
    colors: ["#cf0801", "#e99f01", "#DDCF02", "#73c501", "#018310", "#0084a2", "#005ed4", "#0121d3", "#3c05da", "#5e09dd"]
}
var backgroundTicksPlot = [0, 20, 40, 60, 80, 100]

var plotOptionVegaFreq = {
    grid: {
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor: '#f3f3f3',
        backgroundColor: backgroundColorsPlot,
        markings:  baseFreqMarkings
    },
    series: {
        lines: {
            show: true,
            lineWidth: 3
        },
        shadowSize: 5
    },
    colors: ["#ffeb46", "#759ecd"],
    yaxis: {
        min: 0,
        max: pointsVegaFreq,
        font: {
            size: 10,
            color: "#333"
        },
        ticks: backgroundTicksPlot
    },
    xaxis: {
        show: null,
        font: {
            size: 1,
            color: "#FFF"
        },
        max: 250,
        min: 0
    }
};

var plotOptionOldVegaFreq = {
    grid: {
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor: '#f3f3f3',
        backgroundColor: backgroundColorsPlot,
        markings:  baseFreqMarkings
    },
    series: {
        lines: {
            show: true,
            lineWidth: 3
        },
        shadowSize: 5
    },
    colors: ["#759ecd", "#ffeb46"],
    yaxis: {
        min: 0,
        max: pointsVegaFreq,
        font: {
            size: 10,
            color: "#333"
        },
        ticks: backgroundTicksPlot
    },
    xaxis: {
        show: null,
        font: {
            size: 1,
            color: "#FFF"
        },
        max: 250,
        min: 0
    }
};

function updateGrad(arg) {

    var argArray = [];
    var average = 0;
    var averageIndex = 0;
    var argMax = 0;

    for (let index = 0; index < arg.length - 1; index++) {
        if (arg[index][1] > 0) {
            argArray.push(arg[index][1]);
            average += Math.round(arg[index][1]);
            averageIndex++;
            if (arg[index][1] > argMax) {
                argMax = Math.round(arg[index][1]);
            }
        }
    }

    var argMin = Math.round(average / averageIndex);
    var minValue = 188 - argMin * 1.88;
    var maxValue = 188 - argMax * 1.88;
    var diffValue = argMax - argMin;
    var diffValuePos = 188 - diffValue * 1.88;

    $('.blueGrade').css("top", maxValue + 'px')
    $('.redGrade').css("top", minValue + 'px')
    //  $('.centerGrade').css( "top",  ((minValue+maxValue)/2) +'px' )
    $('.centerGrade').css("top", diffValuePos + 'px')

    $('.leftLegend').html(Math.round(argMax));
    // $('.centerLegend').html(Math.round(((argMax+argMin)/2)));
    $('.centerLegend').html(Math.round(diffValue));
    $('.rightLegend').html(Math.round(argMin));
}

function frequencySelectionActivation() {
    sendHash({action:'activation',method:'selection-frequency',nameDevice:device,numberDevice:deviceUserId});
    setLogger(JSON.stringify({action:'activation',method:'selection-frequency',nameDevice:device,numberDevice:deviceUserId}));
}

$('.content').on('click','.ulFreqElement .programRemove',function () {
    $(this).closest('.ulFreqElement').remove();
});

function addProgramFrequencyOnStorage(id) {
    var list = {};

    var listPrograms = $('.content').find('.freqArea');

    listPrograms.find('.ulFreqElement').each(function (e) {
        list[e] = $(this).data('program-id');
    });

    $.ajax({
        url: '/vega-frequencies/add-program-frequency-on-storage',
        method: 'POST',
        data: {
            id: id,
            list: list
        },
        success: function (msg) {
            if (msg.state === 'danger') {
                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            } else {

                var blFrequencies = '';
                msg.frequencies.forEach(function(item) {
                    blFrequencies +=
                        '<li data-duration="'+item.duration+'" data-frequency-id = "'+item.frequencyId+'" data-frequency-label = "'+item.frequency+'">'+
                            item.frequency+
                        '</li>';
                });


                var blProgramFrequency =
                    '<div class="ulFreqElement" data-program-id="' + msg.programId + '" data-program-key="' + msg.programKey + '">' +
                        '<div class="row">' +
                            '<div class="col-md-12">' +
                                '<span class="titleProgramFrequency">' + msg.programTitle + '</span>' +
                                '<a href="javascript:void(0);" class="programRemove">' +
                                    '<i class="fa fa-fw fa-close"></i>' +
                                '</a>' +
                            '</div>' +
                        '</div>' +
                        '<div class="row">' +
                            '<div class="col-md-12">' +
                                '<ul class="list-inline ulFrequencies">' +
                                    blFrequencies +
                                '</ul>' +
                            '</div>' +
                        '</div>' +
                    '</div>';

                listPrograms.append(blProgramFrequency);
            }

        }
    });
}


$('.content').on('click','.baseFrequency',function () {
    sendHash({action:'base',method:'selection-frequency',callbackFunction:'getDataSelectionBaseFrequency',nameDevice:device,numberDevice:deviceUserId});
    setLogger(JSON.stringify({action:'base',method:'selection-frequency',callbackFunction:'getDataSelectionBaseFrequency',nameDevice:device,numberDevice:deviceUserId}));

    oldResVegaFreq = [];
    allDataVegaFreq = [];
});

function getDataSelectionBaseFrequency(request) {
    setLogger(request);
    request = JSON.parse(request);
    baseFreqMarkings = []

    backgroundColorsPlot = {
        colors: ["#cf0801", "#e99f01", "#DDCF02", "#73c501", "#018310", "#0084a2", "#005ed4", "#0121d3", "#3c05da", "#5e09dd"]
    }
    backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
        
    if ($(".typeVegaPlot").is(":checked")) {
        backgroundColorsPlot = {
            colors: ["#ffffff", "#ffffff"]
        }
        backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
    }

    plotOptionOldVegaFreq.grid.backgroundColor = backgroundColorsPlot
    plotOptionVegaFreq.grid.backgroundColor = backgroundColorsPlot
    plotOptionOldVegaFreq.yaxis.ticks = backgroundTicksPlot
    plotOptionVegaFreq.yaxis.ticks = backgroundTicksPlot

    var tmpAllVegaData = [];

    if (request.numberMeasure == 0) {
        interactive_plot_vega_frequencies = $.plot('#interactive_plot_vega_frequencies', [0, 0], plotOptionVegaFreq);
        allDataVegaFreq = [];
    }

    allDataVegaFreq.push([allDataVegaFreq.length, request.data[0]]);

    if ((request.numberMeasure > 0) && (request.numberMeasure < 250)) {

        for (let i = 0; i < allDataVegaFreq.length; i++) {
            tmpAllVegaData.push([i, allDataVegaFreq[i][1]*pointsVegaFreq/100]);
        }
        interactive_plot_vega_frequencies.setData([tmpAllVegaData]);
        interactive_plot_vega_frequencies.draw();
        updateGrad(tmpAllVegaData);
    }

    // добавляем коридор
    if (request.numberMeasure == 249) { 

        // просчитываем пики и кидаем маркингсы
        var firstGrow = 0
        var firstDown = 0
        var downsGraph = [] // ямы
        var growsGraph = [] // пики
        var directionUp = true

        for (let i = 1; i < tmpAllVegaData.length; i++) {
            if ((tmpAllVegaData[i][1] < tmpAllVegaData[i-1][1]) && (firstGrow == 0))  {
                firstGrow = i
            } 

            if ((tmpAllVegaData[i][1] > tmpAllVegaData[i-1][1]) && (firstGrow != 0) && (firstDown == 0)) {
                firstDown = i
                downsGraph.push(tmpAllVegaData[i-1][1])
                directionUp = true
            } 

            if ((i > firstDown) && (firstDown != 0)) {
                if (directionUp) {
                    if (tmpAllVegaData[i][1] < tmpAllVegaData[i-1][1]) {
                        growsGraph.push(tmpAllVegaData[i-1][1])
                        directionUp = false
                    }
                } else {
                    if (tmpAllVegaData[i][1] > tmpAllVegaData[i-1][1]) {
                        downsGraph.push(tmpAllVegaData[i-1][1])
                        directionUp = true
                    }
                }
            }
        }

        baseFrequencyMax = 0;
        for(let i = 0; i < growsGraph.length; i++) {
            baseFrequencyMax += growsGraph[i];
        }
        baseFrequencyMax = Math.round(baseFrequencyMax / growsGraph.length);
        baseFrequencyMin = 0;
        for(let i = 0; i < downsGraph.length; i++) {
            baseFrequencyMin += downsGraph[i];
        }
        baseFrequencyMin = Math.round(baseFrequencyMin / growsGraph.length);

        baseFreqMarkings = [{ xaxis: { from: 0, to: 250 }, yaxis: { from: baseFrequencyMin, to: baseFrequencyMax }, color: "#222D32", lineWidth: 5 }]

        interactive_plot_vega_frequencies.getOptions().grid.markings.push({ xaxis: { from: 0, to: 250 }, yaxis: { from: baseFrequencyMin, to: baseFrequencyMax }, color: "#222D32", lineWidth: 5 });
        interactive_plot_vega_frequencies.setupGrid();
        interactive_plot_vega_frequencies.draw();
    }

}

$('.content').on('click','.ulFrequencies li',function () {
    var currentFreq = $('.content').find('#currentFreq');

    var deviceUserId = $('meta[name="deviceNumber"]').attr("content");
    var device = $('meta[name="device"]').attr("content");

    var frequencyInfo = $(this);

    var frequencyLabel = frequencyInfo.data('frequency-label');
    var frequencyId = frequencyInfo.data('frequency-id');
    var programFrequencyId = frequencyInfo.closest('.ulFreqElement').data('program-id');

    if(baseFrequencyMin != '' && baseFrequencyMax!=''){

        if(currentFreq.val() != frequencyLabel){
            currentFreq.data({
                'program-id' : programFrequencyId,
                'frequency-id' : frequencyId
            }).val(frequencyLabel);

            sendHash({action:'start',method:'selection-frequency',frequency:frequencyLabel,callbackFunction:'getDataSelectionFrequency',nameDevice:device,numberDevice:deviceUserId});
            setLogger(JSON.stringify({action:'start',method:'selection-frequency',frequency:frequencyLabel,callbackFunction:'getDataSelectionFrequency',nameDevice:device,numberDevice:deviceUserId}));
        }

    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('first_get_base_frequency'),
            ''
        );
    }

});

$('.content').on('click','.deleteFrequency',function () {
    var deviceUserId = $('meta[name="deviceNumber"]').attr("content");
    var device = $('meta[name="device"]').attr("content");

    var blActiveFrequency = $(this).closest('.compositionComplex');

    var blActiveFrequencyInput = blActiveFrequency.find('#currentFreq');

    if(blActiveFrequencyInput.val() !== ''){
        var frequencyId = blActiveFrequencyInput.data('frequency-id');
        var programId = blActiveFrequencyInput.data('program-id');

        $('.content').find('.freqArea .ulFreqElement[data-program-id="'+programId+'"] .ulFrequencies [data-frequency-id="'+frequencyId+'"]').remove();

        console.log();

        let countFreq = $('.content').find('.freqArea .ulFreqElement[data-program-id="'+programId+'"] .ulFrequencies li').length;
        if(countFreq == 0){
            $('.content').find('.freqArea .ulFreqElement[data-program-id="'+programId+'"]').remove();
        }

        blActiveFrequency.find('#currentFreq').data({
            'program-id' : '',
            'frequency-id' : ''
        }).val('');

        sendHash({action:'stop',method:'selection-frequency',nameDevice:device,numberDevice:deviceUserId});
        setLogger(JSON.stringify({action:'stop',method:'selection-frequency',nameDevice:device,numberDevice:deviceUserId}));
    }
});

$('.content').on('click','.clearStorageFrequency',function () {
    var deviceUserId = $('meta[name="deviceNumber"]').attr("content");
    var device = $('meta[name="device"]').attr("content");

    $('.content').find('.freqArea').html('');

    sendHash({action:'stop',method:'selection-frequency',nameDevice:device,numberDevice:deviceUserId});
    setLogger(JSON.stringify({action:'stop',method:'selection-frequency',nameDevice:device,numberDevice:deviceUserId}));
});

$('.content').on('keyup','.searchProgramsFrequencies',function () {
    var val = $(this).val();

    if (val.length >= 3) {

        $.ajax({
            url: '/vega-frequencies/search-programs-frequencies?s=' + val,
            method: 'get',
            success: function (msg) {
                $('#treeVegaFrequencies').tree('loadData', msg);
            }
        });

    } else {
        $('#treeVegaFrequencies').tree('loadDataFromUrl', $('#treeVegaFrequencies').data('url'));
    }
});

function getDataSelectionFrequency(request) {
    setLogger(request);
    request = JSON.parse(request);

    backgroundColorsPlot = {
        colors: ["#cf0801", "#e99f01", "#DDCF02", "#73c501", "#018310", "#0084a2", "#005ed4", "#0121d3", "#3c05da", "#5e09dd"]
    }
    backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
        
    if ($(".typeVegaPlot").is(":checked")) {
        backgroundColorsPlot = {
            colors: ["#ffffff", "#ffffff"]
        }
        backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
    }

    plotOptionOldVegaFreq.grid.backgroundColor = backgroundColorsPlot
    plotOptionVegaFreq.grid.backgroundColor = backgroundColorsPlot
    plotOptionOldVegaFreq.yaxis.ticks = backgroundTicksPlot
    plotOptionVegaFreq.yaxis.ticks = backgroundTicksPlot

    var tmpAllVegaData = [];
    var tmpoldResVegaFreq = [];

    if (request.numberMeasure == 0) {

        if (allDataVegaFreq.length > 0) {
            oldResVegaFreq = allDataVegaFreq;
            interactive_plot_vega_frequencies = $.plot('#interactive_plot_vega_frequencies', [{
                data: oldResVegaFreq
            }, {
                data: [0, 0]
            }], plotOptionOldVegaFreq);
        } else {
            interactive_plot_vega_frequencies = $.plot('#interactive_plot_vega_frequencies', [0, 0], plotOptionVegaFreq);
        }
        allDataVegaFreq = [];
    }

    allDataVegaFreq.push([allDataVegaFreq.length, request.data[0]]);

    if ((request.numberMeasure > 0) && (request.numberMeasure < 250)) {

        for (let i = 0; i < allDataVegaFreq.length; i++) {
            tmpAllVegaData.push([i, allDataVegaFreq[i][1]*pointsVegaFreq/100]);
        }

        if (oldResVegaFreq.length > 0) {

            for (let i = 0; i < oldResVegaFreq.length; i++) {
                tmpoldResVegaFreq.push([i, oldResVegaFreq[i][1]*pointsVegaFreq/100]);
            }

            interactive_plot_vega_frequencies.setData([{
                data: tmpoldResVegaFreq
            }, {
                data: tmpAllVegaData
            }]);
            interactive_plot_vega_frequencies.draw();
        } else {
            interactive_plot_vega_frequencies.setData([tmpAllVegaData]);
            interactive_plot_vega_frequencies.draw();
        }

        updateGrad(tmpAllVegaData);

        interactive_plot_vega_frequencies.getOptions().grid.markings.push({ xaxis: { from: 0, to: 250 }, yaxis: { from: baseFrequencyMin, to: baseFrequencyMax }, color: "#222D32", lineWidth: 5 });
        interactive_plot_vega_frequencies.setupGrid();
        interactive_plot_vega_frequencies.draw();


    }

}

function addProgramFrequencyFromStorageInReceipt(dataStorage,blReceiptId){

    var blReceipt = $('.content').find('#'+blReceiptId);

    var dataReceipt =
        '<div class="item">' +
            '<span class="titleProgram" data-program-id="'+dataStorage.closest('.ulFreqElement').data('program-id')+'" data-program-key="'+dataStorage.closest('.ulFreqElement').data('program-key')+'">' +
                dataStorage.text() +
            '</span>' +
            '<a href="javascript:void(0);" class="addReceiptItemInStorageFrequenciesProgram" title="'+t_js('add_receipt_item_in_storage')+'">' +
                '<i class="fa fa-fw fa-mail-reply"></i>' +
            '</a>' +
            '<a href="javascript:void(0);" class="removeItemInAreaFrequenciesProgram" title="'+t_js('remove')+'">' +
                '<i class="fa fa-fw fa-close"></i>' +
            '</a>' +
            '<ul class="list-inline frequenciesProgram">' +
                '<li>(' +
                dataStorage.closest('.ulFreqElement').find('.ulFrequencies').html() +
                '<li>)' +
            '</ul>' +
        '</div>';

    blReceipt.append(dataReceipt);

    if (blReceiptId == 'receiptAreaFrequency1' || blReceiptId == 'receiptAreaFrequency2') {
        updateRecipeFrequenciesProgramPatient();
    }
}

$('.content').on('click','.removeItemInAreaFrequenciesProgram',function () {
    var blItem = $(this).closest('.item');

    blItem.remove();

    updateRecipeFrequenciesProgramPatient();
});

function updateRecipeFrequenciesProgramPatient() {

    var recipes = {
        1 : [],
        2 : []
    };

    for(var i=1;i<=2;i++){
        $('.content').find('#receiptAreaFrequency'+ i + ' .item').each(function () {

            let item =  $(this);
            let frequencies = [];

            item.find('.frequenciesProgram li').each(function () {
                if(typeof $(this).data('frequency-id') != 'undefined'){
                    frequencies.push({
                        id: $(this).data('frequency-id'),
                        frequency: $(this).data('frequency-label'),
                        duration: $(this).data('duration'),

                    });
                }

            });

            recipes[i].push({
                id: item.find('.titleProgram').data('program-id'),
                key: item.find('.titleProgram').data('program-key'),
                title: item.find('.titleProgram').text(),
                frequencies: frequencies
            })
        });
    }

    $.ajax({
        url: '/vega-frequencies/update-recipe',
        method: 'POST',
        data: {recipes: recipes},
        success: function (msg) {
            if(msg.state === 'danger'){
                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            }
        }
    });
}

$('.content').on('click','.addReceiptItemInStorageFrequenciesProgram',function () {
    var bl = $(this).closest('.item');
    var data = {
        id: bl.find('.titleProgram').data('program-id'),
        key: bl.find('.titleProgram').data('program-key'),
        title: bl.find('.titleProgram').text(),
        frequencies: []
    };

    bl.find('.frequenciesProgram li').each(function () {
        if(typeof $(this).data('frequency-id') != 'undefined'){
            data.frequencies.push({
                id: $(this).data('frequency-id'),
                frequency: $(this).data('frequency-label'),
                duration: $(this).data('duration')
            });
        }
    });

    addProgramFrequenciesOnStorage(data);
});

$('.content').on('click','.addAllReceiptFrequencyItemsInStorage',function () {
    $(this).closest('div.col-md-12').find('.receiptArea .item').each(function () {
        var bl = $(this).closest('.item');
        var data = {
            id: bl.find('.titleProgram').data('program-id'),
            key: bl.find('.titleProgram').data('program-key'),
            title: bl.find('.titleProgram').text(),
            frequencies: []
        };

        bl.find('.frequenciesProgram li').each(function () {
            if(typeof $(this).data('frequency-id') != 'undefined'){
                data.frequencies.push({
                    id: $(this).data('frequency-id'),
                    frequency: $(this).data('frequency-label'),
                    duration: $(this).data('duration')
                });
            }
        });

        addProgramFrequenciesOnStorage(data);
    });
});

function addProgramFrequenciesOnStorage(data) {

    flUseProgram = 0;
    $('.content').find('.freqArea .ulFreqElement').each(function () {
       if($(this).data('program-id') === data.id){
            flUseProgram = 1;
           return false;
       }
    });

    if(flUseProgram === 1){
        console.log('bolt');
        return false;
    }

    var frequencies = '';
    data.frequencies.forEach(function(frequency) {
        frequencies += '<li data-duration="'+frequency.duration+'" data-frequency-id="'+frequency.id+'" data-frequency-label="'+frequency.frequency+'">'+frequency.frequency+'</li>';
    });

    var program =
        '<div class="ulFreqElement" data-program-id="'+data.id+'"  data-program-key="'+data.key+'">' +
            '<div class="row">' +
                '<div class="col-md-12">' +
                    '<span class="titleProgramFrequency">'+data.title+'</span>' +
                    '<a href="javascript:void(0);" class="programRemove"><i class="fa fa-fw fa-close"></i></a>' +
                '</div>' +
            '</div>' +
            '<div class="row">' +
                '<div class="col-md-12">' +
                    '<ul class="list-inline ulFrequencies">' +
                        frequencies +
                    '</ul>' +
                '</div>' +
            '</div>' +
        '</div>';

    $('.content').find('.freqArea').append(program);
}

$('.content').on('click','.addInReportFrequency',function () {
    var itemReport = '';

    $('.content').find('.freqArea .ulFreqElement').each(function () {

        var programId = $(this).data('program-id');
        var programKey = $(this).data('program-key');
        var programTitle = $(this).find('.titleProgramFrequency').text();

        var programFrequencies = '';
        $(this).find('.ulFrequencies li').each(function () {

            let frequencyId = $(this).data('frequency-id');
            let frequencyTitle = $(this).data('frequency-label');
            let frequencyDuration = $(this).data('duration');

            programFrequencies +=
                '<li>' +
                    '<input type="hidden" name="FrequencyTest['+programId+'][frequencies]['+frequencyId+'][id]" value="'+frequencyId+'">' +
                    '<input type="hidden" name="FrequencyTest['+programId+'][frequencies]['+frequencyId+'][title]" value="'+frequencyTitle+'">' +
                    '<input type="hidden" name="FrequencyTest['+programId+'][frequencies]['+frequencyId+'][duration]" value="'+frequencyDuration+'">' +
                    frequencyTitle;
        });

        itemReport +=
            '<tr>' +
                '<td>' +
                    '<input type="hidden" name="FrequencyTest['+programId+'][id]" value="'+programId+'">' +
                    '<input type="hidden" name="FrequencyTest['+programId+'][key]" value="'+programKey+'">' +
                    '<input type="hidden" name="FrequencyTest['+programId+'][title]" value="'+programTitle+'">' +
                    programTitle +
                '</td>' +
                '<td>' +
                    '<ul class="list-inline">' +
                        programFrequencies +
                    '</ul>' +
                '</td>' +
                '<td>' +
                    '<textarea name="FrequencyTest['+programId+'][description]" class="form-control no-border no-background no-padding" cols="30" rows="2" disabled></textarea>' +
                '</td>' +
                '<td>' +
                    '<a href="javascript:void(0);" class="editDescrItemFrequencyTest">' +
                        '<i class="fa fa-pencil"></i>' +
                    '</a>' +
                    '<a href="javascript:void(0);" class="remoteItemFrequencyTest">' +
                        '<i class="fa fa-fw fa-close"></i>' +
                    '</a>' +
                '</td>' +
            '</tr>';
    });

    $('.content').find('.tableFrequencyTest tbody').html(itemReport)
});

$('.content').on('click','.remoteItemFrequencyTest',function () {
    $(this).closest('tr').remove();
});

$('.content').on('click','.editDescrItemFrequencyTest',function () {
    $(this).hide().closest('tr').find('textarea').removeAttr('disabled');
});

$('.content').on('click','.saveFrequencyTest',function () {
    var form = $('.content').find('.formFrequencyTest');
    var data = form.serialize();

    console.log(data);

    $.ajax({
        url: '/vega-frequencies/save-frequency-test',
        method: 'POST',
        data: {data: data},
        success: function (msg) {
            modalAlert(msg.state, msg.title, msg.content, msg.footer);
            if(msg.state === 'success'){
                form.find('tbody').html('');
            }
        }
    });
});

$('.content').on('change', '.flComparisonVegaFrequencyTests', function () {
    if ($(this).is(':checked')) {
        setListVegaTest('.comparableListFrequencyTest[data-part="2"]');
        $('.content').find('.comparableListFrequencyTest[data-part="2"]').removeAttr('disabled');
    } else {
        $('.content').find('.comparableListFrequencyTest[data-part="2"]').attr({
            disabled: 'disabled'
        });
    }
});

function setListVegaTest(el) {
    $.ajax({
        url: '/vega-frequencies/get-dates-tests',
        success: function (msg) {
            var list = $('.content').find(el);
            list.empty().append($('<option>', {
                value: ''
            }).text(t_js('test_session')));
            for (var key in msg) {
                list.prepend($('<option>', {
                    value: key
                }).text(msg[key]));
            }
        }
    });
}

$('.content').on('change', '.comparableListFrequencyTest', function () {
    var testId = $(this).val();
    var part = $(this).data('part');

    $('.content').find('.tableFrequencyTestСomparison'+part+' tbody').html('');

    if (testId != '') {
        $.ajax({
            url: '/vega-frequencies/get-test',
            method: 'POST',
            data: {
                id: testId
            },
            success: function (msg) {
                if(msg.lenght != 0 ){
                    var itemReport = '';

                    msg.forEach(function(item) {

                        var programFrequencies = '';

                        let frequencies = jQuery.parseJSON(item.frequencies);

                        for(var k in frequencies) {
                            programFrequencies +=
                                '<li data-id="'+frequencies[k].id+'">' +
                                    '<input type="hidden" name="FrequencyTest['+item.vega_frequencies_test_id+'][frequencies]['+frequencies[k].id+'][id]" value="'+frequencies[k].id+'">' +
                                    '<input type="hidden" name="FrequencyTest['+item.vega_frequencies_test_id+'][frequencies]['+frequencies[k].id+'][title]" value="'+frequencies[k].title+'">' +
                                    '<input type="hidden" name="FrequencyTest['+item.vega_frequencies_test_id+'][frequencies]['+frequencies[k].id+'][duration]" value="'+frequencies[k].duration+'">' +
                                    frequencies[k].title;
                        }


                        itemReport +=
                            '<tr data-id="'+item.vega_frequencies_test_id+'">' +
                                '<td>' +
                                    '<input type="hidden" name="FrequencyTest['+item.vega_frequencies_test_id+'][id]" value="'+item.vega_frequencies_test_id+'">' +
                                    '<input type="hidden" name="FrequencyTest['+item.vega_frequencies_test_id+'][key]" value="'+item.key+'">' +
                                    '<input type="hidden" name="FrequencyTest['+item.vega_frequencies_test_id+'][title]" value="'+item.exciter+'">' +
                                    item.exciter +
                                '</td>' +
                                '<td>' +
                                    '<ul class="list-inline">' +
                                        programFrequencies +
                                    '</ul>' +
                                '</td>' +
                                '<td>' +
                                    '<textarea name="FrequencyTest['+item.vega_frequencies_test_id+'][description]" class="form-control no-border no-background no-padding" cols="30" rows="2" disabled>'+item.description+'</textarea>' +
                                '</td>' +
                            '</tr>';
                    });


                    $('.content').find('.tableFrequencyTestСomparison'+part+' tbody').html(itemReport);
                }

            }
        });
    }
});

$('.content').on('click', '.addNowSessionFrequencyTest', function () {
    var part = $(this).closest('.row').find('.comparableListFrequencyTest').data('part');
    var testId = $(this).closest('.row').find('.comparableListFrequencyTest').val();

    if (testId != '') {

        var programs = '';

        $('.content').find('.tableFrequencyTestСomparison'+part+' tbody tr').each(function () {
            var lineBl = $(this);

            var programId = lineBl.data('id');
            var programKey = lineBl.find('[name="FrequencyTest['+programId+'][key]"]').val();
            var programTitle = lineBl.find('[name="FrequencyTest['+programId+'][title]"]').val();

            var frequencies = '';
            lineBl.find('li').each(function () {
                let frequencyId = $(this).data('id');
                let frequencyTitle = $(this).find('[name="FrequencyTest['+programId+'][frequencies]['+frequencyId+'][title]"]').val();
                let frequencyDuration = $(this).find('[name="FrequencyTest['+programId+'][frequencies]['+frequencyId+'][duration]"]').val();

                frequencies += '<li data-duration="'+frequencyDuration+'" data-frequency-id="'+frequencyId+'" data-frequency-label="'+frequencyTitle+'">'+frequencyTitle+'</li>';
            });

            programs +=
                '<div class="ulFreqElement" data-program-id="'+programId+'" data-program-key="'+programKey+'">' +
                    '<div class="row">' +
                        '<div class="col-md-12">' +
                            '<span class="titleProgramFrequency">'+programTitle+'</span>' +
                            '<a href="javascript:void(0);" class="programRemove"><i class="fa fa-fw fa-close"></i></a>' +
                        '</div>' +
                    '</div>' +
                    '<div class="row">' +
                        '<div class="col-md-12">' +
                            '<ul class="list-inline ulFrequencies">' +
                            frequencies +
                            '</ul>' +
                        '</div>' +
                    '</div>' +
                '</div>';
        });

        $('.content').find('.freqArea').html(programs)
    }
});


$('.content').on('change', '#sliderScale', function (slideEvt) {

    pointsVegaFreq = slideEvt.value.newValue;

    // if ($(".typeVegaPlot").is(":checked")) {
    //     backgroundColorsPlot = {
    //         colors: ["#ffffff", "#ffffff"]
    //     }
    //     backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
    // }

    plotOptionVegaFreq.grid.backgroundColor = backgroundColorsPlot
    plotOptionVegaFreq.yaxis.ticks = backgroundTicksPlot

    if (interactive_plot_vega_frequencies === undefined) {
        interactive_plot_vega_frequencies = $.plot('#interactive_plot_vega_frequencies', [[0,0]], plotOptionVegaFreq);
    } else {

        var tmpAllVegaData = [];
        var tmpoldResVegaFreq = [];

        for (let i = 0; i < allDataVegaFreq.length; i++) {
            tmpAllVegaData.push([i, allDataVegaFreq[i][1]*pointsVegaFreq/100]);
        }

        if (oldResVegaFreq.length > 0) {

            for (let i = 0; i < oldResVegaFreq.length; i++) {
                tmpoldResVegaFreq.push([i, oldResVegaFreq[i][1]*pointsVegaFreq/100]);
            }

            interactive_plot_vega_frequencies.setData([{
                data: tmpoldResVegaFreq
            }, {
                data: tmpAllVegaData
            }]);
        } else {
            interactive_plot_vega_frequencies.setData([tmpAllVegaData]);
        }
        interactive_plot_vega_frequencies.draw();
    }

});

$('.content').on('click','.btnRecordComplexSelectionFrequencies',function () {

    btnDisabled($(this));

    var blRecipe = $('.content').find('.receiptsSelectionFrequencies');

    var data = [];
    if (blRecipe.find('.receipt1Selector input[name="r1"]').is(":checked")) {
        recipeNumber = 1;
    } else {
        recipeNumber = 2;
    }

    blRecipe.find('#receiptAreaFrequency' + recipeNumber + ' .item').each(function (e) {

        let frequencies = [];

        $(this).find('.frequenciesProgram li').each(function () {
            if(typeof $(this).data('frequency-id') != 'undefined'){
                frequencies.push({
                    id: $(this).data('frequency-id'),
                    frequency: $(this).data('frequency-label'),
                    duration: $(this).data('duration'),

                });
            }

        });

        data.push({
            id: $(this).find('.titleProgram').data('program-id'),
            key: $(this).find('.titleProgram').data('program-key'),
            title: $(this).find('.titleProgram').text(),
            frequencies: frequencies
        });
    });

    if(data.length != 0){
        if($('.content').find('#fromCloud').is(':checked')){
            recordMedicalComplexFSOnCloud(data)
        } else {
            recordMedicalComplexFSOnDevice(data);
        }
    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('not_data_for_write_in_recipe'),
            ''
        );
    }

});

function recordMedicalComplexFSOnDevice(data) {
    let flConnectDevice = checkConnectDevice('life-balance');
    if(flConnectDevice === false ){
        return false;
    }

    $.ajax({
        url: '/medical-complex/create-from-frequency-selection',
        type: 'POST',
        data: {
            dateTime:getDateTimeNow(),
            data:data
        },
        beforeSend: function () {
            modalAlert(
                'warning',
                t_js('processing_data'),
                '<div class="height250 paddingTop20"><p><img src="../images/complex-calculation.gif" class="img-responsive"></p></div>',
                ''
            );
        },
        complete: function () {

        },
        success: function (msg) {
            setTimeout(function(){

                //TODO:KAA delete old app
                if(msg.complex.callbackFunction == 'checkDeviceForRecordComplex'){
                    modalAlert(msg.state, msg.title, msg.content, msg.footer);

                    if(msg.state != 'danger') {
                        sendHash(msg.complex);
                        setLogger(JSON.stringify(msg.complex));

                        setFlPendingComplexRecording(true);

                        waitingDeviceResponse();

                    }
                } else {
                    if(msg.state != 'danger') {
                        sendHash(msg.complex);
                        setLogger(JSON.stringify(msg.complex));

                        setFlPendingComplexRecording(true);

                    } else {
                        modalAlert(msg.state, msg.title, msg.content, msg.footer);
                    }
                }

                setLogger(JSON.stringify(msg.complex));
            }, 5000);
        }
    });

}

function recordMedicalComplexFSOnCloud(data) {
    $.ajax({
        url: '/medical-complex/save-cloud-from-frequency-selection',
        type: 'POST',
        data: {
            dateTime:getDateTimeNow(),
            data:data
        },
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {

        },
        success: function (msg) {
            setTimeout(function(){
                $('#loading_box').hide();

                modalAlert(msg.state, msg.title, msg.content, msg.footer);

            }, 5000);
        }
    });
}

$('.content').on('click','.btnRecordReprinterSelectionFrequencies',function () {

    btnDisabled($(this));

    //$('#loading_box').show();

    var blRecipe = $('.content').find('.receiptsSelectionFrequencies');

    var data = [];
    if (blRecipe.find('.receipt1Selector input[name="r1"]').is(":checked")) {
        recipeNumber = 1;
    } else {
        recipeNumber = 2;
    }

    blRecipe.find('#receiptAreaFrequency' + recipeNumber + ' .item').each(function (e) {
        $(this).find('.frequenciesProgram li').each(function () {
            if(typeof $(this).data('frequency-id') != 'undefined'){
                data.push({
                    frequency: $(this).data('frequency-label'),
                    duration: $(this).data('duration')
                });
            }

        });
    });

    if(data.length != 0){
        setLogger(JSON.stringify({
            method:'reprinter',
            action: 'write-from-selection-frequency',
            callbackFunction: 'getDataReprinter',
            nameDevice:softwareMode,
            data: data
        }));
        sendHash({
            method:'reprinter',
            action: 'write-from-selection-frequency',
            callbackFunction: 'getDataReprinter',
            nameDevice:softwareMode,
            data: data
        });

        if (typeof timerWriteForReprinter !== 'undefined') {
            clearInterval(timerWriteForReprinter);
        }

        timerWriteForReprinter = setTimeout(function () {
            modalAlert(
                'danger',
                t_js('attention'),
                '<p>' + t_js('not_answer_from_device') + '<p>',
                ''
            );
        }, 5000);

    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('not_data_for_write_in_recipe'),
            ''
        );
    }
});

