$('.content').on('click','.showRemoveItemPWBtn',function () {
    let bl = $(this).closest('.blProbabilisticWeights');

    $(this).addClass('onlyHide');

    bl.find('[data-column="'+$(this).data('column')+'"] a').each(function () {
        $(this).removeClass('onlyHide')
    });
});

$('.content').on('click','.removeItemPW',function () {
    btnDisabled($(this));

    let testId = $(this).data('test-id');
    let parasiteId = $(this).data('parasite-id');
    let organId = $(this).data('organ-id');


    let bl = $(this).closest('span');
    if(organId == 0){
        bl = $(this).closest('.row');
    }

    $.ajax({
        url: '/probabilistic-weights/edit-test',
        method: 'POST',
        data: {
            diagnostic_test_id: testId,
            parasite_id: parasiteId,
            organ_id: organId
        },
        success: function (msg) {
            if(msg){
                bl.remove();
            }
        }
    });
});