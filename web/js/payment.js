$(document).ready(function() {
    // send order on payment
    $('.formPay').on('submit',function (e) {
        e.preventDefault();

        var form = $(this);
        var data = form.serialize();
        var url = '';
        var payment_method = form.find('input[name="payment_method"]:checked').val();
        if(payment_method === 'softpay'){
            url = '/soft-pay/pay';
        } else if(payment_method === 'advcash'){
            url = '/advcash/pay';
        } else if(payment_method === 'stripe'){
            url = '/stripe/pay';
        } else if(payment_method === 'from-personal-account') {
            url = '/bc/pay';
        }

        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){
                $('.content-wrapper .content').html(msg);
                $('#paymentPopup').modal('hide');
            }
        });
    });


});