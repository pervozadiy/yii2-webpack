$(document).ready(function() {
    $('.content').on('click','.blMenuUserGuide a',function () {

        contentId = $(this).data('id');

        changeActive(contentId);

        $.ajax({
            url: '/user-guide/get-content?id='+contentId,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){

                $('.content').find('.contentUserGuide').html(msg);

            }
        });
    });

});

function changeActive(contentId) {
    $('.content').find('.blMenuUserGuide li').each(function () {
       $(this).removeClass('active');

       if($(this).find('a').data('id') == contentId){
           $(this).addClass('active');
       }

    });
}