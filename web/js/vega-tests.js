var timerWriteForReprinter;

$('.content').on('change', '.selectedVegaAction', function () {

    let action = $(this).val();

    loadContent('/electropuncture/' + action);

});

$('.content').on('change', '.itemStorage', function () {
    sendSelectorInStorageOnDevice();
});

$('.content').on('keyup', '.searchDrugVegaSelector', function () {

    var val = $('.content').find('.searchDrugVegaSelector').val();

    if (val.length >= 3) {

        $.ajax({
            url: '/vega-selector/search-drug?s=' + val,
            method: 'get',
            success: function (msg) {
                $('#treeElectropuncture').tree('loadData', msg);
            }
        });


    } else {
        $('#treeElectropuncture').tree('loadDataFromUrl', $('#treeElectropuncture').data('url'));
    }


});

$('.content').on('click', '.takeOnAllStorageVegaTest', function () {
    $('.content').find('.ulFreqElement ul li input').prop('checked', true);
});

$('.content').on('click', '.takeOffAllStorageVegaTest', function () {
    $('.content').find('.ulFreqElement ul li input').prop('checked', false);
});

$('.content').on('click', '.clearStorageVegaTest', function () {
    $('.content').find('.ulFreqElement ul').html('');
    sendSelectorInStorageOnDevice();
});

function addSelectorOnIndividual(id) {
    $.ajax({
        url: '/vega-selector/add-selector-on-individual',
        method: 'POST',
        data: {
            id: id
        },
        success: function (msg) {
            modalAlert(msg.state, msg.title, msg.content, msg.footer);
        }
    });
}

function removeSelectorOnIndividual(id) {
    $.ajax({
        url: '/vega-selector/remove-selector-on-individual',
        method: 'POST',
        data: {
            id: id
        },
        success: function (msg) {
            $('#treeElectropuncture').tree('loadDataFromUrl', $('#treeElectropuncture').data('url'));
            modalAlert(msg.state, msg.title, msg.content, msg.footer);
        }
    });
}

function addSelectorOnStorage(id) {
    var list = {};

    var listInput = $('.content').find('.ulFreqElement');

    listInput.find('input').each(function (e) {
        list[e] = $(this).val();
    });

    $.ajax({
        url: '/vega-selector/add-selector-on-storage',
        method: 'POST',
        data: {
            id: id,
            list: list
        },
        success: function (msg) {
            if (msg.state == 'danger') {
                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            } else {
                listInput.find('ul').append('<li><input type="checkbox" class="itemStorage" value="' + msg.item.value + '" data-signals=' + JSON.stringify(msg.item.signals) + ' data-breeding="' + msg.item.breeding + '"> ' + msg.item.title + '</li>');
            }

        }
    });
}

function removeItemSelectorOnStorage(itemUse) {
    itemUse.remove();
    sendSelectorInStorageOnDevice();
}

function sendSelectorInStorageOnDevice(activeSelector) {

    var deviceUserId = $('meta[name="deviceNumber"]').attr("content");
    var device = $('meta[name="device"]').attr("content");

    var signals = [];
    var countActiveSignals = 0;
    $('.content').find('.itemStorage').each(function (e) {
        if ($(this).is(':checked')) {
            $(this).data('signals').forEach(function (item) {
                item.Selekted = 1;
                signals.push(item);
                countActiveSignals++;
            });
            $('#breeding').html($(this).data('breeding'));
        } else {
            $(this).data('signals').forEach(function (item) {
                item.Selekted = 0;
                signals.push(item);
            });
        }
    });

    if (typeof activeSelector !== "undefined") {
        signals.push(activeSelector);
    }

    $('#connectedDrugs').html(countActiveSignals);

    if (signals.length == 0) {
        $('#breeding').html('N/A');
    }

    var patientId = $('meta[name="patientId"]').attr("content");
    if (patientId === '3') {
        setLogger(JSON.stringify({
            action: 'start',
            method: 'vega-test',
            nameDevice: device,
            numberDevice: deviceUserId,
            data: signals,
            callbackFunction: 'getDataVegaTest'
        }));
        sendHash({
            action: 'start',
            method: 'vega-test',
            nameDevice: device,
            numberDevice: deviceUserId,
            data: signals,
            callbackFunction: 'getDataVegaTest'
        });
    }

}

// переменные на вегу
var oldResVega = [];
var allDataVega = [];
var interactive_plot_vega;
var pointsVega = 100;
var backgroundColorsPlot = {
    colors: ["#cf0801", "#e99f01", "#DDCF02", "#73c501", "#018310", "#0084a2", "#005ed4", "#0121d3", "#3c05da", "#5e09dd"]
}
var backgroundTicksPlot = [0, 20, 40, 60, 80, 100]

var plotOptionVega = {
    grid: {
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor: '#f3f3f3',
        backgroundColor: backgroundColorsPlot
    },
    series: {
        lines: {
            show: true,
            lineWidth: 3
        },
        shadowSize: 5
    },
    colors: ["#ffeb46", "#759ecd"],
    yaxis: {
        min: 0,
        max: pointsVega,
        font: {
            size: 10,
            color: "#333"
        },
        ticks: backgroundTicksPlot
    },
    xaxis: {
        show: null,
        font: {
            size: 1,
            color: "#FFF"
        },
        max: 250,
        min: 0
    }
};

var plotOptionOldVega = {
    grid: {
        borderColor: '#f3f3f3',
        borderWidth: 1,
        tickColor: '#f3f3f3',
        backgroundColor: backgroundColorsPlot
    },
    series: {
        lines: {
            show: true,
            lineWidth: 3
        },
        shadowSize: 5
    },
    colors: ["#759ecd", "#ffeb46"],
    yaxis: {
        min: 0,
        max: pointsVega,
        font: {
            size: 10,
            color: "#333"
        },
        ticks: backgroundTicksPlot
    },
    xaxis: {
        show: null,
        font: {
            size: 1,
            color: "#FFF"
        },
        max: 250,
        min: 0
    }
};

function updateGrad(arg) {

    var argArray = [];
    var average = 0;
    var averageIndex = 0;
    var argMax = 0;

    for (let index = 0; index < arg.length - 1; index++) {
        if (arg[index][1] > 0) {
            argArray.push(arg[index][1]);
            average += Math.round(arg[index][1]);
            averageIndex++;
            if (arg[index][1] > argMax) {
                argMax = Math.round(arg[index][1]);
            }
        }
    }

    var argMin = Math.round(average / averageIndex);
    var minValue = 188 - argMin * 1.88;
    var maxValue = 188 - argMax * 1.88;
    var diffValue = argMax - argMin;
    var diffValuePos = 188 - diffValue * 1.88;

    $('.blueGrade').css("top", maxValue + 'px')
    $('.redGrade').css("top", minValue + 'px')
    //  $('.centerGrade').css( "top",  ((minValue+maxValue)/2) +'px' )
    $('.centerGrade').css("top", diffValuePos + 'px')

    $('.leftLegend').html(Math.round(argMax));
    // $('.centerLegend').html(Math.round(((argMax+argMin)/2)));
    $('.centerLegend').html(Math.round(diffValue));
    $('.rightLegend').html(Math.round(argMin));


    $('#speedBlue').removeClass();
    $('#speedRed').removeClass();

    $('#speedBlue').addClass('speedBlue' + argMin);
    $('#speedRed').addClass('speedRed' + argMin);
    $('#speedText').html(argMin);


}

function getDataVegaTest(request) {
    setLogger(request);

    backgroundColorsPlot = {
        colors: ["#cf0801", "#e99f01", "#DDCF02", "#73c501", "#018310", "#0084a2", "#005ed4", "#0121d3", "#3c05da", "#5e09dd"]
    }
    backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
        
    if ($(".typeVegaPlot").is(":checked")) {
        backgroundColorsPlot = {
            colors: ["#ffffff", "#ffffff"]
        }
        backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
    }

    plotOptionOldVega.grid.backgroundColor = backgroundColorsPlot
    plotOptionVega.grid.backgroundColor = backgroundColorsPlot
    plotOptionOldVega.yaxis.ticks = backgroundTicksPlot
    plotOptionVega.yaxis.ticks = backgroundTicksPlot
    request = JSON.parse(request);

    var tmpAllVegaData = [];
    var tmpOldResVega = [];

    if (request.numberMeasure == 0) {

        if (allDataVega.length > 0) {
            oldResVega = allDataVega;
            interactive_plot_vega = $.plot('#interactive_plot_vega', [{
                data: oldResVega
            }, {
                data: [0, 0]
            }], plotOptionOldVega);
        } else {
            interactive_plot_vega = $.plot('#interactive_plot_vega', [0, 0], plotOptionVega);
        }
        allDataVega = [];
    }

    allDataVega.push([allDataVega.length, request.data[0]]);

    if ((request.numberMeasure > 0) && (request.numberMeasure < 250)) {

        for (let i = 0; i < allDataVega.length; i++) {
            tmpAllVegaData.push([i, allDataVega[i][1]*pointsVega/100]);
        }

        if (oldResVega.length > 0) {

            for (let i = 0; i < oldResVega.length; i++) {
                tmpOldResVega.push([i, oldResVega[i][1]*pointsVega/100]);
            }

            interactive_plot_vega.setData([{
                data: tmpOldResVega
            }, {
                data: tmpAllVegaData
            }]);
            interactive_plot_vega.draw();
        } else {
            interactive_plot_vega.setData([tmpAllVegaData]);
            interactive_plot_vega.draw();
        }
        updateGrad(tmpAllVegaData);
    }
}

function addItemFromStorageInArea(itemUse, area) {
    var content = itemUse.text();
    var value = itemUse.find('input').val();

    var fl_add = 1;
    var countItem = 0;
    $('.content').find('#' + area + ' input').each(function () {
        if ($(this).val() == value) {
            fl_add = 0;
        }

        countItem++;
    });

    if (countItem === 30) {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('it_is_possible_add_no_more_than_30_nosodes_recipe'),
            ''
        );
    } else if (fl_add == 1) {
        var item =
            '<span>' +
                content +
                '<a href="javascript:void(0);" class="addReceiptItemInStorage"  title="' + t_js('add_receipt_item_in_storage') + '">' +
                '<i class="fa fa-fw fa-mail-reply"></i>' +
                '</a>' +
                '<input type="hidden" class="' + area + '" name="' + area + '[]" value="' + value + '">' +
                '<a href="javascript:void(0);" class="removeItemInArea" title="' + t_js('remove') + '">' +
                '<i class="fa fa-fw fa-close"></i>' +
                '</a>' +
            '</span>';
        $('.content').find('#' + area).append(item);
    }

    if (area == 'receiptArea1' || area == 'receiptArea2') {
        updateRecipePatient();
    }
}

function addAllItemFromStorageInArea(itemUse, area) {
    var blItems = itemUse.closest('.ulFreqElement');

    blItems.find('li').each(function () {
        var content = $(this).text();
        var value = $(this).find('input').val();

        var fl_add = 1;

        if (!$(this).find('input').is(':checked')) {
            fl_add = 0;
        }

        $('.content').find('#' + area + ' input').each(function () {
            if ($(this).val() == value) {
                fl_add = 0;
            }
        });

        if (fl_add == 1) {
            var item =
                '<span>' +
                content +
                '<input type="hidden" class="' + area + '" name="' + area + '[]" value="' + value + '">' +
                '<a href="javascript:void(0);" class="removeItemInArea"><i class="fa fa-fw fa-close"></i></a>' +
                '</span>';
            $('.content').find('#' + area).append(item);
        }
    });

    if (area == 'receiptArea1' || area == 'receiptArea2') {
        updateRecipePatient();
    }
}

$('.content').on('click', '.removeItemInArea', function () {

    var bl = $(this).closest('div');
    var blItem = $(this).closest('span');

    blItem.remove();

    if (bl.hasClass('receiptArea')) {
        updateRecipePatient();
    }
});

function addSelectorsInChain() {
    var blItems = $('.content').find('.ulFreqElement');

    if (blItems.find('input:checked').length > 0) {
        modalAlert(
            'success',
            t_js('add_conversation_name'),
            '<input type="text" class="form-control input-sm titleChain">',
            '<div class="text-center"><a href="javascript:void(0);" class="btn btn-success saveSelectorInChain">' + t_js('add') + '</a></div>'
        );
    }
}

$(document).on('click', '.saveSelectorInChain', function () {

    var titleChain = $(document).find('.titleChain').val();
    if (titleChain != '') {
        var data = {};
        var blItems = $('.content').find('.ulFreqElement');

        data.title = titleChain;
        data.selector = [];


        blItems.find('input:checked').each(function () {
            data.selector.push($(this).val());
        });

        $.ajax({
            url: '/vega-selector/add-selector-on-chain',
            method: 'POST',
            data: data,
            success: function (msg) {
                console.log(msg);

                if (msg.state != 'danger') {
                    $('#treeChain').tree('loadDataFromUrl', $('#treeChain').data('url'));
                }
                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            }
        });
    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            t_js('chain_name_has_not_been_added'),
            ''
        );
    }
});

function removeItemChain(itemUse) {
    $.ajax({
        url: '/vega-selector/remove-item-chain',
        method: 'POST',
        data: {
            id: itemUse
        },
        success: function (msg) {
            if (msg.state != 'danger') {
                $('#treeChain').tree('loadDataFromUrl', $('#treeChain').data('url'));
            }

            modalAlert(msg.state, msg.title, msg.content, msg.footer);
        }
    });
}

function addItemChainOnStorage(itemUse) {
    $.ajax({
        url: '/vega-selector/add-item-chain-on-storage',
        method: 'POST',
        data: {
            id: itemUse
        },
        success: function (msg) {
            if (msg.state != 'danger') {
                msg.items.forEach(function (element) {
                    $('.content').find('.ulFreqElement ul').append(
                        '<li>' +
                        '<input type="checkbox" class="itemStorage" value="' + element.value + '" data-signals=' + JSON.stringify(element.signals) + '> ' +
                        element.title +
                        '</li>');
                });

                sendSelectorInStorageOnDevice();
            }

            modalAlert(msg.state, msg.title, msg.content, msg.footer);
        }
    });
}

function updateRecipePatient() {
    $.ajax({
        url: '/vega-recipe/update-recipe',
        method: 'POST',
        data: {
            recipe1: $('.content').find('.receiptArea1').map(function () {
                return $(this).val();
            }).get(),
            recipe2: $('.content').find('.receiptArea2').map(function () {
                return $(this).val();
            }).get()
        },
        success: function (msg) {
            console.log(msg);
        }
    });
}

$('.content').on('click', '.saveVegaTest', function () {

    let pathogens = $('.content').find('.pathogensELP').map(function () {
        return $(this).val();
    }).get();
    let process = $('.content').find('.processELP').map(function () {
        return $(this).val();
    }).get();
    let integral_indicators = $('.content').find('.integralELP').map(function () {
        return $(this).val();
    }).get();


    if(pathogens.length != 0 || process.length != 0 && integral_indicators.length != 0){
        $.ajax({
            url: '/vega-tests/add-test',
            method: 'POST',
            data: {
                dateTime: getDateTimeNow(),
                pathogens: pathogens,
                process: process,
                integral_indicators: integral_indicators
            },
            success: function (msg) {
                if (msg.state == 'success') {
                    $('.content').find('#pathogensELP,#processELP,#integralELP').html('');
                }

                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            }
        });
    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            '<p>' + t_js('no_container_is_full') + '<p>',
            ''
        );
    }
});

$('.content').on('change', '.flComparisonVegaTests', function () {
    if ($(this).is(':checked')) {
        $('.content').find('.comparableListVegaTest[data-part="2"]').removeAttr('disabled');
        setListVegaTest('.comparableListVegaTest[data-part="2"]');
    } else {
        $('.content').find('.comparableListVegaTest[data-part="2"]').attr({
            disabled: 'disabled'
        });
    }
});

function setListVegaTest(el) {
    $.ajax({
        url: '/vega-tests/get-dates-tests',
        success: function (msg) {
            var list = $('.content').find(el);
            list.empty().append($('<option>', {
                value: ''
            }).text(t_js('test_session')));
            for (var key in msg) {
                list.prepend($('<option>', {
                    value: key
                }).text(msg[key]));
            }
        }
    });
}

$('.content').on('change', '.comparableListVegaTest', function () {
    var testId = $(this).val();
    var part = $(this).data('part');

    $('.content').find('#pathogensELPСomparison'+part+',#processELPСomparison'+part+',#integralELPСomparison'+part).html('');

    if (testId != '') {
        $.ajax({
            url: '/vega-tests/get-test',
            method: 'POST',
            data: {
                id: testId
            },
            success: function (msg) {
                if (msg.pathogens != null) {
                    msg.pathogens.items.forEach(function (element) {
                        $('.content').find('#pathogensELPСomparison'+part).append(
                            '<span>' +
                                element.title +
                            '</span>');
                    });
                }

                if (msg.process != null) {
                    msg.process.items.forEach(function (element) {
                        $('.content').find('#processELPСomparison'+part).append(
                            '<span>' +
                                element.title +
                            '</span>');
                    });
                }

                if (msg.integralIndicators != null) {
                    msg.integralIndicators.items.forEach(function (element) {
                        $('.content').find('#integralELPСomparison'+part).append(
                            '<span>' +
                                element.title +
                            '</span>');
                    });
                }
            }
        });
    }
});

$('.content').on('click', '.addNowSession', function () {
    var part = $(this).data('part');
    var testId = $('.content').find('.comparableListVegaTest[data-part="'+part+'"]').val();

    if (testId != '') {
        $.ajax({
            url: '/vega-tests/get-test',
            method: 'POST',
            data: {
                id: testId
            },
            success: function (msg) {
                $('.content').find('#pathogensELP,#processELP,#integralELP').html('');

                if (msg.pathogens != null) {
                    msg.pathogens.items.forEach(function (element) {
                        $('.content').find('#pathogensELP').append(
                            '<span>' +
                                element.title +
                                '<a href="javascript:void(0);" class="addReceiptItemInStorage"  title="' + t_js('add_receipt_item_in_storage') + '">' +
                                '<i class="fa fa-fw fa-mail-reply"></i>' +
                                '</a>' +
                                '<input type="hidden" class="pathogensELP" name="pathogensELP[]" value="' + element.value + '">' +
                                '<a href="javascript:void(0);" class="removeItemInArea" title="' + t_js('remove') + '">' +
                                '<i class="fa fa-fw fa-close"></i>' +
                                '</a>' +
                            '</span>');

                    });
                }

                if (msg.process != null) {
                    msg.process.items.forEach(function (element) {
                        $('.content').find('#processELP').append(
                            '<span>' +
                                element.title +
                                '<a href="javascript:void(0);" class="addReceiptItemInStorage"  title="' + t_js('add_receipt_item_in_storage') + '">' +
                                '<i class="fa fa-fw fa-mail-reply"></i>' +
                                '</a>' +
                                '<input type="hidden" class="processELP" name="processELP[]" value="' + element.value + '">' +
                                '<a href="javascript:void(0);" class="removeItemInArea" title="' + t_js('remove') + '">' +
                                '<i class="fa fa-fw fa-close"></i>' +
                                '</a>' +
                            '</span>');
                    });
                }

                if (msg.integralIndicators != null) {
                    msg.integralIndicators.items.forEach(function (element) {
                        $('.content').find('#integralELP').append(
                            '<span>' +
                                element.title +
                                '<a href="javascript:void(0);" class="addReceiptItemInStorage"  title="' + t_js('add_receipt_item_in_storage') + '">' +
                                '<i class="fa fa-fw fa-mail-reply"></i>' +
                                '</a>' +
                                '<input type="hidden" class="integralELP" name="integralELP[]" value="' + element.value + '">' +
                                '<a href="javascript:void(0);" class="removeItemInArea" title="' + t_js('remove') + '">' +
                                '<i class="fa fa-fw fa-close"></i>' +
                                '</a>' +
                            '</span>');
                    });
                }
            }
        });
    }
});

$('.content').on('change', '#sliderScale', function (slideEvt) {

    pointsVega = slideEvt.value.newValue;

    if ($(".typeVegaPlot").is(":checked")) {
        backgroundColorsPlot = {
            colors: ["#ffffff", "#ffffff"]
        }
        backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
    }

    plotOptionVega.grid.backgroundColor = backgroundColorsPlot
    plotOptionVega.yaxis.ticks = backgroundTicksPlot

    if (interactive_plot_vega === undefined) {
        interactive_plot_vega = $.plot('#interactive_plot_vega', [[0,0]], plotOptionVega);
    } else {

        var tmpAllVegaData = [];
        var tmpOldResVega = [];

        for (let i = 0; i < allDataVega.length; i++) {
            tmpAllVegaData.push([i, allDataVega[i][1]*pointsVega/100]);
        }

        if (oldResVega.length > 0) {

            for (let i = 0; i < oldResVega.length; i++) {
                tmpOldResVega.push([i, oldResVega[i][1]*pointsVega/100]);
            }

            interactive_plot_vega.setData([{
                data: tmpOldResVega
            }, {
                data: tmpAllVegaData
            }]);
        } else {
            interactive_plot_vega.setData([tmpAllVegaData]);
        }
        interactive_plot_vega.draw();
    }

});

$('.content').on('change', '#changeSpeedometr', function () {

    $('.speedometrSelector').removeClass('hidden');

    var flIndicator = $(this).val();

    if (flIndicator === '1') {
        $('.speedometrSelector').show();
        $('.receipt1Selector').hide();
        $('.receipt2Selector').hide();
    } else {
        $('.speedometrSelector').hide();
        $('.receipt1Selector').show();
        $('.receipt2Selector').show();
    }

    $.ajax({
        url: '/device-user/change-vega-indicator?fl=' + flIndicator,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function () {

        }
    });
});

$('.content').on('change', '.typeVegaPlot', function () {

    var vegaPlot = 1;

    if ($(this).is(":checked")) {
        vegaPlot = 0;
        backgroundColorsPlot = {
            colors: ["#ffffff", "#ffffff"]
        }
        backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
    } else {
        backgroundColorsPlot = {
            colors: ["#cf0801", "#e99f01", "#DDCF02", "#73c501", "#018310", "#0084a2", "#005ed4", "#0121d3", "#3c05da", "#5e09dd"]
        }
        backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
    }

    $.ajax({
        url: '/device-user/change-vega-plot?vega_plot=' + vegaPlot,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function () {
            // update plot
            var plotTmp = (typeof interactive_plot_vega !== 'undefined') ? interactive_plot_vega : interactive_plot_vega_tmp

            plotTmp.getOptions().grid.backgroundColor = backgroundColorsPlot;
            plotTmp.getAxes().yaxis.options.ticks = backgroundTicksPlot;
            plotTmp.setupGrid();
            plotTmp.draw();
        }
    });
});

$('.content').on('click', '.btnLangSelector', function () {
    var lang = $(this).data('lang');
    var link = $(this).data('link');

    $.ajax({
        url: '/vega-selector/change-lang-selector?lang=' + lang,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function () {
            loadContent(link);
        }
    });
});

// write on reprinter
$('.content').on('click', '#writeOnReprinter', function () {

    btnDisabled($(this));

    var device = $('meta[name="device"]').attr("content");
    var blRecipe = $('.content').find('.receiptChange');

    var data = [];
    if (blRecipe.find('.receipt1Selector input[name="r1"]').is(":checked")) {
        recipeNumber = 1;
    } else {
        recipeNumber = 2;
    }

    blRecipe.find('.receiptArea' + recipeNumber).each(function (e) {
        data.push($(this).val());
    });

    if (data.length !== 0) {
        $.ajax({
            url: '/vega-selector/get-data-for-reprinter',
            method: 'POST',
            data: {
                data: data
            },
            success: function (msg) {
                if (msg.state == 'danger') {
                    modalAlert(msg.state, msg.title, msg.content, msg.footer);
                } else {
                    setLogger(JSON.stringify({
                        method:'reprinter',
                        nameDevice:device,
                        action: 'write-on-reprinter',
                        callbackFunction: 'getDataReprinter',
                        data: msg.data
                    }));
                    sendHash({
                        method:'reprinter',
                        nameDevice:device,
                        action: 'write-on-reprinter',
                        callbackFunction: 'getDataReprinter',
                        data: msg.data
                    });

                    modalAlert(
                        'warning',
                        t_js('attention'),
                        '<p>' + t_js('wait_data_is_being_prepared') + '<p>',
                        '&nbsp;'
                    );


                    if (typeof timerWriteForReprinter !== 'undefined') {
                        clearInterval(timerWriteForReprinter);
                    }

                    timerWriteForReprinter = setTimeout(function () {
                        modalAlert(
                            'danger',
                            t_js('attention'),
                            '<p>' + t_js('not_answer_from_device') + '<p>',
                            ''
                        );
                    }, 5000);
                }
            }
        });
    } else {
        modalAlert(
            'danger',
            t_js('attention'),
            '<p>' + t_js('not_data_for_write_in_recipe') + '<p>',
            ''
        );
    }
});

function getDataReprinter(request) {
    setLogger(request);

    request = JSON.parse(request);

    if (request.error === 'ok') {
        modalAlert(
            'success',
            t_js('attention'),
            '<p>' + t_js('data_successfully_written_to_reprinter') + '<p>',
            ''
        );
    } else if (request.error === 'loading') {
        modalAlert(
            'warning',
            t_js('attention'),
            '<p>' + t_js('data_loading_to_reprinter') + '<p>',
            '&nbsp;'
        );

        clearInterval(timerWriteForReprinter);
    } else if (request.error === 'notconnected' || request.error === 'deviceclosed') {
        modalAlert(
            'warning',
            t_js('attention'),
            '<p>' + t_js('device_not_connected') + '<p>',
            ''
        );

        clearInterval(timerWriteForReprinter);
    } else if (request.error === 'errorversiondevice') {
        modalAlert(
            'danger',
            t_js('attention'),
            '<p>' + t_js('device_does_not_support_writing_to_reprinter') + '<p>',
            ''
        );

        clearInterval(timerWriteForReprinter);
    } else if (request.error === 'deviceblocked' || request.error === 'errornumber') {
        modalAlert(
            'danger',
            t_js('attention'),
            '<p>' + t_js('incorrect_device_code') + '!</p>',
            ''
        );

        clearInterval(timerWriteForReprinter);
    } else if (request.error === 'errorgetpacket') {
        modalAlert(
            'danger',
            t_js('attention'),
            '<p>' + t_js('error_while_receiving_data_from_device') + '<p>',
            ''
        );

        clearInterval(timerWriteForReprinter);
    } else if (request.error === 'errorsendpacket') {
        modalAlert(
            'danger',
            t_js('attention'),
            '<p>' + t_js('error_while_transferring_data_to_device') + '<p>',
            ''
        );

        clearInterval(timerWriteForReprinter);
    } else if (request.error === 'error_wrong_code') {
        modalAlert(
            'danger',
            t_js('attention'),
            '<p>' + t_js('wrong_drug_code') + '<p>',
            ''
        );

        clearInterval(timerWriteForReprinter);
    } else if (request.error === 'error_wrong_potency') {
        modalAlert(
            'danger',
            t_js('attention'),
            '<p>' + t_js('wrong_number_potency') + '<p>',
            ''
        );

        clearInterval(timerWriteForReprinter);
    } else {
        modalAlert(
            'danger',
            '<p>' + t_js('attention') + '<p>',
            request.message,
            ''
        );

        clearInterval(timerWriteForReprinter);
    }
}

$('.content').on('click', '.addReceiptItemInStorage', function () {
    var id = $(this).closest('span').find('input').val();

    addSelectorOnStorage(id);
});

$('.content').on('click', '.addAllReceiptItemsInStorage', function () {

    var bl = $(this).closest('div.col-md-12');

    bl.find('.receiptArea span').each(function () {
        let id = $(this).find('input').val();

        addSelectorOnStorage(id);
    });
});

$('.content').on('click', '.btnСalibration', function () {

    var maxPointVega = 0
    var maxPointVegaScale = 0

    if ($(".typeVegaPlot").is(":checked")) {
        backgroundColorsPlot = {
            colors: ["#ffffff", "#ffffff"]
        }
        backgroundTicksPlot = [0, 20, 40, 60, 80, 100]
    }

    plotOptionVega.grid.backgroundColor = backgroundColorsPlot
    plotOptionVega.yaxis.ticks = backgroundTicksPlot

    if (interactive_plot_vega === undefined) {
        interactive_plot_vega = $.plot('#interactive_plot_vega', [[0,0]], plotOptionVega);
    } else {

        var tmpAllVegaData = [];
        var tmpOldResVega = [];

        for (let i = 0; i < allDataVega.length; i++) {
            if (maxPointVega < allDataVega[i][1]) {
                maxPointVega = allDataVega[i][1]
            }
        }
        pointsVega = (80 / maxPointVega) * 100;

        for (let i = 0; i < allDataVega.length; i++) {
            tmpAllVegaData.push([i, allDataVega[i][1]*pointsVega/100]);
        }

        if (oldResVega.length > 0) {

            for (let i = 0; i < oldResVega.length; i++) {
                tmpOldResVega.push([i, oldResVega[i][1]*pointsVega/100]);
            }

            interactive_plot_vega.setData([{
                data: tmpOldResVega
            }, {
                data: tmpAllVegaData
            }]);
        } else {
            interactive_plot_vega.setData([tmpAllVegaData]);
        }
        interactive_plot_vega.draw();
    }
});

// infoBlElectropuncture
$('.content').on('change', '.menuInfoBlElectropuncture', function () {

    var bl = $(this).val();

    getInfoBlElectropuncture(bl);
});

function getInfoBlElectropuncture(bl){
    $.ajax({
        url: '/electropuncture/get-info-bl-' + bl,
        method: 'POST',
        data: {},
        success: function (msg) {
            $('.content').find('.infoBlElectropuncture').html(msg);
        }
    });
}
