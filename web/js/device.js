/** check status all device */

// set global variable
var statusDevice = {
    status : false,
    time : Date.now()
};

// start the device polling timer
var timerCheckDevice = setInterval(checkStatusDevice, 5000);

// function the device polling timer
function checkStatusDevice() {

    var device = $('meta[name="device"]').attr("content");

    if((Date.now() - statusDevice.time) > 6000 && statusDevice.status == true){
        setStatusDeviceForTest(false);
    }

    sendHash({
        action:'check-device',
        nameDevice:device,//TODO:KAA delete
        time:Date.now(),//TODO:KAA delete
        callbackFunction:'getStatusDevice'
    });
    setLogger(JSON.stringify({action:'check-device',numberDevice:'',nameDevice:device,time:Date.now(),callbackFunction:'getStatusDevice'}));
}

// get answer from app
function getStatusDevice(request) {

    setLogger(request);

    var request = JSON.parse(request);

    let listDevice = '';

    if (request.hasOwnProperty('devices')) {
        if(Array.isArray(request.devices) && request.devices.length){

            let deviceForTest = '';

            request.devices.forEach(function(connectDevice) {
                if((softwareMode == connectDevice.name && deviceForTest != 'life-expert-profi') || (softwareMode == 'life-expert' && connectDevice.name == 'life-expert-profi')){
                    deviceForTest = connectDevice.name;
                }

                listDevice +=
                    `<li 
                        data-device="${connectDevice.name}" 
                        data-version="${connectDevice.firmwareVersion}">
                        ${connectDevice.name} (v - ${connectDevice.firmwareVersion})
                    </li>`;
            });

            if(deviceForTest != ''){
                setStatusDeviceForTest(true,deviceForTest);
            } else {
                setStatusDeviceForTest(false);
            }

        } else {
            setStatusDeviceForTest(false);
        }
    } else {
        //TODO:KAA delete
        if(request.status == '1'){
            setStatusDeviceForTest(true,request.realDevice);
        } else if(request.status == '0' && request.realDevice == 'life-expert-profi'){
            setStatusDeviceForTest(true,request.realDevice);
        } else {
            setStatusDeviceForTest(false);
        }
    }

    $('.blConnectDevice ul').html(listDevice);
}

// function set status
function setStatusDeviceForTest(status,device = ''){

    $('meta[name="deviceReal"]').attr({content:device});

    statusDevice = {
        status : status,
        time : Date.now()
    };

    if(status == true){
        $('#labelStatusDevice').removeClass().addClass('label bg-green pull-right').text('ready');
        $('meta[name="deviceState"]').attr({content:'1'});
    } else {
        $('#labelStatusDevice').removeClass().addClass('label bg-red pull-right').text('not ready');
        $('meta[name="deviceReal"]').attr({content:''});
        $('meta[name="deviceState"]').attr({content:'0'});
    }
}

/** check version app */
// check version
sendHash({action:'get-version-app',callbackFunction:'checkVersionApp'});
setLogger(JSON.stringify({action:'get-version-app',callbackFunction:'checkVersionApp'}));

// function check version
function checkVersionApp(request) {

    setLogger(request);

    request = JSON.parse(request);

    $.ajax({
        url: '/settings/check-version-app',
        type: 'POST',
        data: request,
        success: function (msg) {

            $('.blVersionApp span').html('('+msg.versionReal+')');

            if(msg.state === 'danger'){

                modalAlert(
                    'danger',
                    t_js('attention')+'!',
                    '<p style="font-size: 22px;color:#FF0000">'+t_js('you_have_outdated_version_program_')+'</p>',
                    // '<p>'+t_js('to_continue_we_recommend_make_')+'</p>' +
                    // '<p>'+t_js('this_will_save_all_your_data_')+'</p>' +
                    // '<p>'+t_js('this_operation_will_require_few_minutes_')+'</p>' +
                    // '<p>'+t_js('updates_may_include_')+':' +
                    // '<ul>' +
                    //     '<li> '+t_js('device_improvements_and_error_corrections')+
                    //     '<li> '+t_js('improving_algorithm_definition')+
                    //     '<li> '+t_js('new_and_or_improved_features')+
                    //     '<li> '+t_js('further_performance_improvements')+
                    // '</ul>' +
                    // '</p>' +
                    // '<p>'+t_js('for_stable_and_trouble_free_operation_')+'</p>',
                    '<div class="row">' +
                        '<div class="col-md-12"><a href="'+msg.appLink+'" class="btn btn-block btn-warning btn-rows loadAppInDefaultBrowser">'+t_js('download_update_from_website')+'</a></div>' +
                    '<div>'
                );
            }
        }
    });
}

$(document).on('click','.loadAppInDefaultBrowser',function (e) {
    e.preventDefault();

    setLogger(JSON.stringify({action:'transitionInDefaultBrowser',link:$(this).attr('href')}));
    sendHash({action:'transitionInDefaultBrowser',link:$(this).attr('href')});

    //TODO:KAA delete
    setTimeout(
        function() {
            setLogger(JSON.stringify({action:'closeApp'}));
            sendHash({action:'closeApp'});
        }, 5000);
});

/** update app **/
// show modal restart
function haveUpdateApp(request) {
    setLogger(request);

    request = JSON.parse(request);

    modalAlert(
        'warning',
        `${t_js("attention")}!`,
        `<p>${t_js("application_updated_to_version")} - ${request.version}</p>`,
        `<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">
            ${t_js("restart_later")}
        </button>
        <button type="button" class="btn btn-outline pull-right restartApp">
            ${t_js("restart_app")}
        </button>`
    );
}

$('#modalAlert').on('click','.restartApp',function () {
    sendHash({action:'restart-app'});
    setLogger(JSON.stringify({action:'restart-app'}));
    modalAlertHide();
});

//TODO:KAA
/** check version firmware */
function checkFirmwareVersion() {
    setLogger(JSON.stringify({
        action:'firmwareVersion',
        method:'get',
        nameDevice:getDeviceName(),
        callback:'getFirmwareVersion'
    }));
    sendHash({
        action:'firmwareVersion',
        method:'get',
        nameDevice:getDeviceName(),
        callback:'getFirmwareVersion'
    });
}

function getFirmwareVersion(request) {
    setLogger(request);
}

/** check new code for life-expert */
function getStatusDeviceNumber(request) {
    setLogger(request);

    request = JSON.parse(request);

    if(request.status == '1'){
        modalAlert('success',t_js('updating_device_code'),'<p>'+t_js('code_has_been_updated_and_it_is_correct')+'</p>','<div class="text-center"><a href="express-test/view" class="btn btn-success loadContent" data-dismiss="modal">Ok</a></div>');
    } else {
        modalAlert('danger',t_js('attention')+'!','<p>'+t_js('code_you_added_is_not_correct')+'</p>','');
    }
}

/** check connection device by name device */
function checkConnectDevice(deviceName) {

    if($('.blConnectDevice li').is('[data-device="' + deviceName + '"]')){
        return true;
    } else {
        modalAlert(
            'danger',
            t_js('attention')+'!',
            '<p>'+t_js('device_not_connected')+'</p>' +
            '<p><ul><li>'+deviceName+'</li></ul></p>',
            '');
        return false;
    }
}


function getListConnectDevices() {
    let listDevices = [];
    $('.blConnectDevice li').each(function(){
        listDevices.push({device:$(this).data('device'),version:$(this).data('version')})
    });

    return listDevices;
}

function getListConnectTherapeuticDevices() {
    let therapeuticDevices = [
        'life-animal',
        'life-balance'
    ];
    let listConnectDevices = getListConnectDevices();
    let listDevices = [];

    if(listConnectDevices.length > 0){
        for(let item of listConnectDevices){
            if(therapeuticDevices.indexOf(item.device) != -1){
                listDevices.push(item)
            }
        }
    }

    return listDevices;
}

// display modal with error
function displaysErrorFromDevices(error){
    switch (error.name) {
        case 'test_error':
            modalAlert(
                'danger',
                t_js('attention')+'!',
                '<p>'+t_js('test_error')+'</p>',
                ''
            );
            runAudio('error');
            break;
        case 'obrivelectr':
        case 'errsetotved':
            modalAlert(
                'danger',
                t_js('attention')+'!',
                '<p>' +
                    t_js('breakage_or_poor_contact_of_electrodes')+ ': ' +
                    '<b>' + getNameDanglingElectrode(error.numberMeasure) + '</b>' +
                '</p>' +
                '<p>' + t_js('try_moisten_electrodes_and_run_test') + '.</p>' +
                '<p>' +
                    t_js('if_it_does_not_help_then_check_breakages') + '. ' +
                    t_js('to_do_this_close_all_electrodes_and_run') + '. ' +
                    t_js('if_the_test_passes_then_ok') +
                '</p>',
                ''
            );
            runAudio('error');
            break;
        case 'notconnected':
        case 'deviceclosed':
            modalAlert(
                'danger',
                t_js('attention'),
                '<p>' + t_js('device_not_connected') + '<p>',
                ''
            );
            runAudio('error');
            break;
        case 'deviceblocked':
            modalAlert(
                'danger',
                t_js('attention'),
                '<p>' + t_js('device_blocked') + '<p>',
                ''
            );
            runAudio('error');
            break;
        case 'errornumber':
            modalAlert(
                'danger',
                t_js('attention')+'!',
                '<p>' + t_js('incorrect_device_code') + '!</p>' +
                '<p>' +
                    '<a href="javascript:void(0);" data-href="device-user/code-device" class="loadContent" data-dismiss="modal">' +
                        t_js('change_device_code') +
                    '</a>' +
                '</p>',
                ''
            );
            runAudio('error');
            break;
        case 'errorgetpacket':
            modalAlert(
                'danger',
                t_js('attention'),
                '<p>' + t_js('error_while_receiving_data_from_device') + '<p>',
                ''
            );
            break;
        case 'errorsendpacket':
            modalAlert(
                'danger',
                t_js('attention'),
                '<p>' + t_js('error_while_transferring_data_to_device') + '<p>',
                ''
            );
            break;
        case 'error_wrong_code':
            modalAlert(
                'danger',
                t_js('attention'),
                '<p>' + t_js('wrong_drug_code') + '<p>',
                ''
            );
            break;
        case 'error_wrong_potency':
            modalAlert(
                'danger',
                t_js('attention'),
                '<p>' + t_js('wrong_number_potency') + '<p>',
                ''
            );
            break;
        case 'manycomplex':
            modalAlert(
                'danger',
                t_js('attention'),
                '<p>' + t_js('many_complex_for_device') + '<p>',
                ''
            );
            break;
        default:
            modalAlert('danger',t_js('attention'),error.name,'');
    }
}


function getNameDanglingElectrode(num) {

    let breakageElectrodes = {
        1 : t_js('head'),
        2 : t_js('head'),
        3 : t_js('hand'),
        4 : t_js('hand'),
        5 : t_js('foot'),
        6 : t_js('foot'),
        7 : t_js('head') + ' / ' + t_js('hand'),
        8 : t_js('head') + ' / ' + t_js('hand'),
        9 : t_js('head') + ' / ' + t_js('hand'),
        10 : t_js('head') + ' / ' + t_js('hand'),
        11 : t_js('foot') + ' / ' + t_js('hand'),
        12 : t_js('foot') + ' / ' + t_js('hand'),
        13 : t_js('foot') + ' / ' + t_js('hand'),
        14 : t_js('foot') + ' / ' + t_js('hand'),
        15 : t_js('foot') + ' / ' + t_js('head'),
        16 : t_js('foot') + ' / ' + t_js('head'),
        17 : t_js('foot') + ' / ' + t_js('head'),
        18 : t_js('foot') + ' / ' + t_js('head'),
        19 : t_js('head') + ' / ' + t_js('hand'),
        20 : t_js('head') + ' / ' + t_js('hand'),
        21 : t_js('head') + ' / ' + t_js('hand'),
        22 : t_js('head') + ' / ' + t_js('hand'),
        23 : t_js('foot') + ' / ' + t_js('hand'),
        24 : t_js('foot') + ' / ' + t_js('hand'),
        25 : t_js('foot') + ' / ' + t_js('hand'),
        26 : t_js('foot') + ' / ' + t_js('hand'),
        27 : t_js('foot') + ' / ' + t_js('head'),
        28 : t_js('foot') + ' / ' + t_js('head'),
        29 : t_js('foot') + ' / ' + t_js('head'),
        30 : t_js('foot') + ' / ' + t_js('head'),
        31 : t_js('head') + ' / ' + t_js('hand'),
        32 : t_js('foot') + ' / ' + t_js('hand'),
        33 : t_js('head') + ' / ' + t_js('hand'),
        34 : t_js('foot') + ' / ' + t_js('head'),
        35 : t_js('head') + ' / ' + t_js('hand'),
        36 : t_js('head') + ' / ' + t_js('hand'),
        37 : t_js('foot') + ' / ' + t_js('head'),
        38 : t_js('foot') + ' / ' + t_js('head'),
        39 : t_js('foot') + ' / ' + t_js('hand'),
        40 : t_js('foot') + ' / ' + t_js('hand'),
        41 : t_js('foot') + ' / ' + t_js('hand'),
        42 : t_js('foot') + ' / ' + t_js('hand'),
        43 : t_js('foot') + ' / ' + t_js('head'),
        44 : t_js('foot') + ' / ' + t_js('hand'),
        45 : t_js('foot') + ' / ' + t_js('hand'),
        46 : t_js('foot') + ' / ' + t_js('head'),
        47 : t_js('foot') + ' / ' + t_js('hand'),
        48 : t_js('foot') + ' / ' + t_js('hand')
    };

    return breakageElectrodes[num];
}