$(document).ready(function() {
    $('.content-wrapper .content').on('submit','.formCodeDevice',function (e) {
        e.preventDefault();

        var form = $(this);
        var data = form.serialize();

        $.ajax({
            url: '/device-user/change-device-code',
            type: 'POST',
            data: data,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){
                if(msg.state == 'success'){
                    $('meta[name="deviceNumber"]').attr({content:msg.number});

                    sendHash({action:'check-device-number',nameDevice:'life-expert',numberDevice:msg.number});
                    setLogger(JSON.stringify({action:'check-device-number',nameDevice:'life-expert',numberDevice:msg.number}));

                } else {
                    modalAlert(msg.state,msg.title,msg.content,msg.footer);
                }
            }
        });
    });

    $('.content').on('click','.activateLicenceLifeExpertProfi',function () {
        modalAlert(
            'warning',
            t_js('attention')+'!',
            '<p>'+t_js('you_will_now_be_charged_from_your_account_activation_profi')+'</p><p>'+$(this).data('text')+'</p>',
            '<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">'+t_js('close')+'</button>' +
            '<button type="button" class="btn btn-outline pull-right btnSuccessActivateLicenceProfi">'+t_js('pay_now')+'</button>'
        );
    });

    $('#modalAlert').on('click','.btnSuccessActivateLicenceProfi',function () {

        $(this).attr({disabled:'disabled'});

        $.ajax({
            url: '/device-user/active-license-life-expert-profi',
            success: function(msg){
                if(msg.status != 'danger'){
                    loadContent('/device-user/balance');
                }
                modalAlert(msg.state,msg.title,msg.content,msg.footer);
            }
        });
    });

    $('.content').on('click', '.paymentChoose', function () {
        $(document).find('#pack_id').val($('select#initialPack').val());
    });

    $('.content').on('click','.btnSettingsAutomaticallyReplenish',function () {
        var radioBtnSetting = $(this).closest('.blAutomaticallyReplenish').find('.fieldAutomaticallyReplenish');
        $.ajax({
            method: 'POST',
            data:{
                field:radioBtnSetting.attr('name'),
                value:(radioBtnSetting.prop('checked') === true ? 1 : 0)
            },
            url: '/device-user/settings-automatically-replenish',
            success: function(msg){
                modalAlert(msg.state,msg.title,msg.content,msg.footer);
            }
        });
    });

    $('.content').on('submit','#formSensitivity',function (e) {
        e.preventDefault();

        var form = $(this);
        var data = form.serialize();

        $.ajax({
            url: '/device-user/save-sensitivity',
            type: 'POST',
            data: data,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){
                modalAlert(msg.state,msg.title,msg.content,msg.footer);
            }
        });
    });

});

function getBalance() {
    $.ajax({
        url: '/device-user/get-balance',
        type: 'GET',
        success: function (msg) {
            console.log(msg)
            if($("span").is(".balanceUserDevice")){
                $(".balanceUserDevice").html(msg);
            }
        }
    });
}

