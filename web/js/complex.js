// flag pending complex recording
var flPendingComplexRecording = false;

function getTestForComplex(selectedPatientId) {

    var device = $('meta[name="device"]').attr("content");

    $.ajax({
        url: '/tests/get-table-tests-for-complex?selectedPatientId='+selectedPatientId+'&device='+device,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(data){
            var table =  $('.content').find('#listSeanceComplex');

            table.dataTable().api().clear();
            table.dataTable().api().rows.add(data);
            table.dataTable().api().draw();
        }
    });
}

$('.content').on('click','.btnActionComplex.btn-primary',function () {

    var blTest = $(this).closest('tr');
    var idTest = $(this).data('test');
    var btn = $(this);

    $.ajax({
        url: '/calculation-test/calculation-diagnostic-test',
        type: 'POST',
        data: {id: idTest,dateTime:getDateTimeNow()},
        beforeSend: function () {
            $('#loading_box').show();
            $('.content').find('.complexDefault').hide();
            $('.content').find('.complexCalculation').show();
        },
        complete: function () {
            // $('#loading_box').hide();
            // $('.content').find('.complexDefault').show();
            // $('.content').find('.complexCalculation').hide();
        },
        success: function (msg) {
            setTimeout(function(){
                $('#loading_box').hide();
                $('.content').find('.complexDefault').show();
                $('.content').find('.complexCalculation').hide();

                modalAlert(msg.state, msg.title, msg.content, msg.footer);

                if(msg.state != 'danger') {
                    blTest.find('.statusTest').text(t_js('processed')).removeClass('label-danger').addClass('label-warning');
                    blTest.find('.btnActionComplex').text(t_js('get_complex')).removeClass('btn-primary').addClass('btn-info');

                    sendHash(msg.complex);
                    setLogger(JSON.stringify(msg.complex));
                }
            }, 5000);
        }
    });

});

$('.content').on('click','.btnActionComplex.btn-info,.btnActionComplex.btn-success',function () {
    var blTest = $(this).closest('tr');
    var idTest = $(this).data('test');
    var btn = $(this);

    if(btn.hasClass('btn-success')){
        let flConnectDevice = checkConnectDevice('life-balance');
        if(flConnectDevice === false ){
            return false;
        }
    }

    $.ajax({
        url: '/medical-complex/create',
        type: 'POST',
        data: {id: idTest,dateTime:getDateTimeNow()},
        beforeSend: function () {
            modalAlert(
                'warning',
                t_js('processing_data'),
                '<div class="height250 paddingTop20"><p><img src="../images/complex-calculation.gif" class="img-responsive"></p></div>',
                ''
            );

            $('.content').find('.complexDefault').hide();
            $('.content').find('.complexRecord').show();
        },
        success: function (msg) {
            setTimeout(function(){

                $('.content').find('.complexDefault').show();
                $('.content').find('.complexRecord').hide();

                if(btn.hasClass('btn-info')){
                    if(msg.state === 'danger') {
                        modalAlert(msg.state, msg.title, msg.content, msg.footer);
                    } else {
                        modalAlert(
                            'success',
                            t_js('attention'),
                            '<p>'+t_js('complex_composed')+'</p>',
                            ''
                        );

                        blTest.find('.statusTest').text(t_js('complex_is_received')).removeClass('label-warning').addClass('label-info');
                        blTest.find('.btnActionComplex').text(t_js('write_to_device')).removeClass('btn-info').addClass('btn-success');
                        blTest.find('.autoComplexRecordOnDeviceViaCloud').removeAttr('disabled');
                    }
                } else {

                    //TODO:KAA delete old app
                    if(msg.complex.callbackFunction == 'checkDeviceForRecordComplex'){
                        modalAlert(msg.state, msg.title, msg.content, msg.footer);

                        if(msg.state != 'danger') {
                            sendHash(msg.complex);
                            setLogger(JSON.stringify(msg.complex));

                            setFlPendingComplexRecording(true);

                            waitingDeviceResponse();

                        }
                    } else {
                        if(msg.state != 'danger') {
                            sendHash(msg.complex);
                            setLogger(JSON.stringify(msg.complex));

                            setFlPendingComplexRecording(true);

                        } else {
                            modalAlert(msg.state, msg.title, msg.content, msg.footer);
                        }
                    }

                }
            }, 5000);
        }
    });

});



function waitingDeviceResponse() {
    flWaitingDeviceResponse = setTimeout(function() {
        modalAlert('danger', t_js('attention'), '<p>'+t_js('complex_not_sent_device')+'</p>', '');
    }, (2*60*1000));
}

// function startProgressBarRecordComplex() {
//     var i = 2;
//     flStartProgressBarRecordComplex = setInterval(function() {
//
//         console.log(i);
//
//         if(i == 100){
//             clearInterval(flStartProgressBarRecordComplex);
//             modalAlert('success', t_js('attention'), '<p>'+t_js('complex_record_on_device')+'</p>', '');
//         } else {
//             $('#modalAlert').find('.progressBarRecordComplex .progress-bar').attr({'aria-valuenow':i}).css({'width':i+'%'});
//         }
//
//
//         i += 2;
//     }, 1000);
// }

function recordComplexOnBalance(request){


    setLogger(request);

    request = JSON.parse(request);

    if (request.hasOwnProperty('error')) {
        displaysErrorFromDevices({name:request.error});
        stopSlowlyProgress();
    } else if(request.hasOwnProperty('progress')){
        if(request.progress.status=='inprogress'){
            if(flPendingComplexRecording == true){
                modalAlert('success',t_js('recording_complex'),
                    '<div class="height250">' +
                        '<p><img src="../images/complex-record.gif" class="paddingLeft50 img-responsive"></p>' +
                        '<p>'+t_js('complex_sent_recording')+'.</p>' +
                        '<div class="progress progress-striped active progressBarRecordComplex">' +
                            '<div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">' +
                                '<span class="sr-only"></span>' +
                            '</div>' +
                        '</div>' +
                    '</div>','');

                setFlPendingComplexRecording(false);

                if (request.progress.percent > 20 && typeof progressStorage == 'undefined') {
                    progressStorage = [];
                    keyProgressStorage = 1;
                }
            }

            // make slowly progress
            if(typeof progressStorage != 'undefined'){

                setStepSlowlyProgress(request.progress.percent);

                slowlyProgress();

            } else {
                $('#modalAlert').find('.progressBarRecordComplex .progress-bar').attr({'aria-valuenow':request.progress.percent}).css({'width':request.progress.percent+'%'});
            }
        } else if(request.progress.status=='finished' && request.progress.percent==100){
            if(typeof progressStorage == 'undefined'){
                modalAlert('success', t_js('attention'), '<p>'+t_js('complex_record_on_device')+'</p>', '');
            } else {
                setStepSlowlyProgress(request.progress.percent);
            }
        }
    }
}

function slowlyProgress(){
    if(typeof intervalProgess == 'undefined') {

        intervalProgess = setInterval(function () {

            for (let key in progressStorage) {

                if (progressStorage[key] == '0') {
                    progressStorage[key] = '1';
                    $('#modalAlert').find('.progressBarRecordComplex .progress-bar').attr({'aria-valuenow': (key * 10)}).css({'width': (key * 10) + '%'});
                    break
                }

                if (key == 10) {
                    modalAlert('success', t_js('attention'), '<p>' + t_js('complex_record_on_device') + '</p>', '');
                    stopSlowlyProgress();
                }
            }

        }, 1000);

    }
}

function stopSlowlyProgress() {
    if(typeof intervalProgess != 'undefined') {
        clearInterval(intervalProgess);
        delete progressStorage;
        delete intervalProgess;
    }
}

function setStepSlowlyProgress(percent){
    let arrayStep = Math.trunc(percent/10);
    for(let i=1;i<=arrayStep;i++){
        if(typeof progressStorage[i] == 'undefined'){
            progressStorage[i] = '0';
        }
    }
}

function setFlPendingComplexRecording(status) {
    flPendingComplexRecording = status;
}


function setCodeDeviceBalance(typeComplex,data) {
    getLastSerialNumber();

    modalAlert(
        'warning',
        t_js('balance_code_for_record'),
        '<div class="row dataRecordComplexOnCloud">' +
            '<div class="form-group"><div class="col-md-12"><div class="blError"></div></div></div>' +
            '<input type="hidden" name="data" value=\''+JSON.stringify(data)+'\'" class="dataComplex">' +
            '<div class="form-group">' +
                '<div class="col-md-4">' +
                    '<label>' +
                        t_js('code_device') +
                    '</label>' +
                '</div>' +
                '<div class="col-md-8">' +
                    '<input type="text" class="form-control serialNumber" disabled="disabled"/>' +
                '</div>' +
            '</div>' +
        '</div>',
        '<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">'+t_js('close')+'</button>' +
        '<button type="button" class="btn btn-outline pull-right recordComplexOnCloud">' +
            t_js('write_to_device')+
        '</button>'
    );
}

$('.content').on('click','.autoComplexRecordOnDeviceViaCloud',function () {
    setCodeDeviceBalance('recordAutoComplexOnCloud',{'dateTime':getDateTimeNow(),'type':'auto-complex','testId':$(this).data('test')});
});

$('#modalAlert').on('click','.recordComplexOnCloud',function () {
    let blData = $('#modalAlert').find('.dataRecordComplexOnCloud');

    blData.find('.blError').html('');

    let serialNumber = blData.find('.serialNumber').val();
    let dataComplex = blData.find('.dataComplex').val();

    if(serialNumber == ''){
        blData.find('.blError').html(
            '<div class="alert alert-danger fade in">' +
                '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                t_js('serial_number_cannot_be_empty') +
            '</div>'
        );
        return false;
    }

    $.ajax({
        url: '/medical-complex/send-complex-on-cloud',
        type: 'POST',
        data: {
            data:dataComplex,
            serialNumber:serialNumber
        },
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {
            if(msg.state == 'warning'){
                blData.find('.blError').html(
                    '<div class="alert alert-danger fade in">' +
                        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>' +
                        msg.content +
                    '</div>'
                );
            } else {
                modalAlert(msg.state, msg.title, msg.content, msg.footer);
            }
        }
    });
});

function getLastSerialNumber() {
    $.ajax({
        url: '/device-user/get-last-serial-number',
        type: 'GET',
        success: function (msg) {
            $('#modalAlert').find('.serialNumber').val(msg.serialNumber).removeAttr('disabled');
        }
    });
}