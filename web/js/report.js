$(document).ready(function() {
    // brn for comparison
    $('.content').on('change','.flComparisonMode',function () {
        if($(this).prop('checked')){
            $(this).closest('.row').find('.withWhomToCompare').removeAttr('disabled');
        } else {
            let bl = $(this).closest('.row');
            bl.find('.withWhomToCompare').attr({disabled:'disabled'}).val('');
            let url = bl.find('.mainListTest').data('url');
            let mainTestId = bl.find('.mainListTest').val();
            loadContentReport(url,mainTestId,'0');
        }
    });

    //look short or full report
    $('.content').on('change','.mainListTest,.comparableListTest',function () {

        let bl = $(this).closest('.row');

        let url = bl.find('.mainListTest').data('url');
        let mainTestId = bl.find('.mainListTest').val();
        let comparableTestId = $('.comparableListTest').val();

        if(comparableTestId == ''){
            comparableTestId = '0';
        }

        loadContentReport(url,mainTestId,comparableTestId);

    });


    $('.content-wrapper .content').on('submit','.formMakeReport',function (e) {

        e.preventDefault();

        var form = $(this);
        var data = form.serialize();

        if(form.find('select[name="mainListTest"]').val() == null){
            modalAlert('danger',t_js('error'),'<p>'+t_js('no_test')+'!</p>','');
            return false;
        }

        $.ajax({
            url: '/report/for-print',
            type: 'POST',
            data: data,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){

                $('.content').html(msg);

            }
        });
    });

    $('.content').on('click','.checkAll', function (e) {
        e.preventDefault();
        $('input[type=checkbox]').each(function() {
            $(this).iCheck('check');
        });
    });

    $('.content').on('click','.uncheckAll', function (e) {
        e.preventDefault();
        $('input[type=checkbox]').each(function() {
            $(this).iCheck('uncheck');
        });
    });

    $('.content').on('click', '.sendReportWithForm', function () {

        var form = $(this).closest('form');
        var emailField = form.find('.inputEmail').val();

        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,6}\.)?[a-z]{2,6}$/i;
        if((emailField == '') || (!pattern.test(emailField))) {
            modalAlert('danger',t_js('error'),'<p>'+t_js('you_have_entered_an_incorrect_email')+'</p>','');
        } else{
            var data = form.serialize();

            $.ajax({
                url: '/report/send-report-email',
                type: 'POST',
                data: data,
                success: function(msg){
                    modalAlert(msg.state,msg.title,msg.content,msg.footer);
                }
            });
        }

    });

    $('.content').on('click','.btnPrint', function() {

        $('.container').printThis( {
            debug: false,               // show the iframe for debugging
            importCSS: true,            // import parent page css
            importStyle: true,         // import style tags
            printContainer: true,       // print outer container/$.selector
            removeInline: false,        // remove inline styles from print elements
            removeInlineSelector: "*",  // custom selectors to filter inline styles. removeInline must be true
            printDelay: 5333,            // variable print delay
            canvas: true,              // copy canvas content
            removeScripts: false,       // remove script tags from print content
            copyTagClasses: false,      // copy classes from the html & body tag
        });

    });

    $('.content').on('click', '.sendReportFull', function () {
        var blEmail = $(this).closest('form');
        var typeTest = $(this).data('type-test');
        var email = blEmail.find('input').val();
        var id = blEmail.find('input').data('test-id');

        var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,6}\.)?[a-z]{2,6}$/i;
        if((email == '') || (!pattern.test(email))) {
            modalAlert('danger',t_js('error'),'<p>'+t_js('you_have_entered_an_incorrect_email')+'</p>','');
        } else {
            $.ajax({
                url: '/report/send-report-email',
                type: 'POST',
                data: 'mainListTest='+id+'&email='+email+'&full=1&typeTest='+typeTest,
                beforeSend: function () {
                    $('#loading_box').show();
                },
                complete: function () {
                    $('#loading_box').hide();
                },
                success: function(msg){
                    modalAlert(msg.state,msg.title,msg.content,msg.footer);
                }
            });

        }
    });

    $('.content').on('click','.lookFullReport',function () {
        $.ajax({
            url: '/report/look-full-report',
            type: 'POST',
            data: {testId:$(this).data('test-id'),testType:$(this).data('test-type')},
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){
                $('.content-wrapper .content').html(msg);
            }
        });
    });

    $('.content').on('click','.checkAll', function (e) {
        e.preventDefault();
        $('.content').find('.checkBoxForReport input[type=checkbox]').each(function() {
            $(this).iCheck('check');
        });
    });

    $('.content').on('click', '.uncheckAll', function (e) {
        e.preventDefault();
        $('.content').find('.checkBoxForReport input[type=checkbox]').each(function() {
            $(this).iCheck('uncheck');
        });
    });

    $('.content').on('change', '.listReportData,.comparableListReportData', function () {
        checkListDataReport();
    });

    $('.content').on('change', '.listReportDataElectropuncture', function () {
        checkComparableListDataReportElectropuncture();
    });

    $('.content-wrapper .content').on('submit','.formMakeReportElectropuncture',function (e) {

        e.preventDefault();

        var form = $(this);
        var data = form.serialize();

        if(form.find('select[name="mainListTestElectropuncture"]').val() == null){
            modalAlert('danger',t_js('error'),'<p>'+t_js('no_test')+'!</p>','');
            return false;
        }


        $.ajax({
            url: '/report/for-print-electropuncture',
            type: 'POST',
            data: data,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){

                $('.content').html(msg);

            }
        });
    });
});

function checkListDataReport() {

    var dateFromList = $('.content').find('.listReportData option:selected').text();
    var dateComparableFromList = $('.content').find('.comparableListReportData option:selected').text();

    var flComparisonMode = $('.content').find('.flComparisonMode');
    var comparableListReportData = $('.content').find('.comparableListReportData');

    var listCheckboxHideDrug = ['dynamics','projection','ranging','unbalanceSystem','probabilisticWeights',
        'anticipatedChanges','biologicalAge', 'vitamins','weightsAndLoads','zodiacalDiet','selectionProduct',
        'sportsNutrition'];
    var listCheckboxHideProduct = ['generalState','statusDiagram','dynamics','projection','spine','ranging',
        'unbalanceSystem','powerSystem','aurogram','probabilisticWeights','anticipatedChanges','biologicalAge',
        'vitamins','weightsAndLoads','selectionDrugs','zodiacalDiet','sportsNutrition'];


    if (~dateFromList.indexOf("(*")) {

        $('.content').find('.checkBoxForReport input[type=checkbox]').each(function() {
            var inputId = $(this).attr('id');
            if(listCheckboxHideProduct.includes(inputId)){
                $(this).iCheck('uncheck').iCheck('disable');
            } else {
                $(this).iCheck('check').iCheck('enable');
            }
        });

        flComparisonMode.prop('checked',false).attr({disabled:'disabled'});
        comparableListReportData.val('').attr({disabled:'disabled'});

    } else if (~dateFromList.indexOf("(") || ~dateComparableFromList.indexOf("(")) {

        $('.content').find('.checkBoxForReport input[type=checkbox]').each(function() {
            var inputId = $(this).attr('id');
            if(listCheckboxHideDrug.includes(inputId)){
                $(this).iCheck('uncheck').iCheck('disable');
            } else {
                $(this).iCheck('check').iCheck('enable');
            }
        });

        flComparisonMode.removeAttr('disabled');

    } else {
        $('.content').find('.checkBoxForReport input[type=checkbox]').each(function() {
            var inputId = $(this).attr('id');
            if(inputId === 'selectionProduct') {
                $(this).iCheck('uncheck').iCheck('disable');
            } else {
                $(this).iCheck('check').iCheck('enable');
            }
        });

        flComparisonMode.removeAttr('disabled');
    }
}

function checkComparableListDataReportElectropuncture() {

    var optionComparable = $('.content').find('.comparableListReportDataElectropuncture');
    optionComparable.val('');

    var optionVal = $('.content').find('.listReportDataElectropuncture option:selected').val();

    if (typeof optionVal !== "undefined" && ~optionVal.indexOf("-vrt")) {
        optionComparable.find('option').each(function() {
            if(~$(this).val().indexOf("-vrt")){
                $(this).removeAttr('disabled');
            } else {
                $(this).attr({disabled:'disabled'});
            }
        });
    } else if(typeof optionVal !== "undefined" &&  ~optionVal.indexOf("-voll")){
        optionComparable.find('option').each(function() {
            if(~$(this).val().indexOf("-voll")){
                $(this).removeAttr('disabled');
            } else {
                $(this).attr({disabled:'disabled'});
            }
        });
    }
}

function loadContentReport(url,mainTestId,comparableTestId){
    $.ajax({
        url: url,
        type: 'GET',
        data: {mainTestId:mainTestId,comparableTestId:comparableTestId},
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function (msg) {
            $('.content-wrapper .content').html(msg);
        }
    });
}