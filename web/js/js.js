var softwareMode = $('meta[name="device"]').attr("content");

function getDeviceName(){
    return $('meta[name="deviceReal"]').attr("content");
}

$(document).ready(function() {

    $('.wrapper').on('click','.sidebar-menu a,.loadContent', function () {
        var linkUrl= $(this).data('href');
        var linkDisabled= $(this).attr('disabled');

        if(linkUrl !== '#' && typeof linkUrl !== "undefined"
            && linkDisabled!== 'disabled'){
            loadContent(linkUrl);
        }
    });

    $('#searchPatient').on('change',function(){
        var patientID = $(this).val();

        if(patientID != ''){

            setDiagnosticPatient(patientID);

            $(this).val('');
        }

    });

    $('#modalAlert').on('click','.listLang a',function () {

        var lang = $(this).data('code');

        $.ajax({
            url: '/device-user/change-language?language='+lang,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){
                window.location.reload(true);
            }
        });
    });

    $('.selectLanguage').on('click',function () {

        $.ajax({
            url: '/device-user/get-languages',
            type: 'GET',
            success: function (msg) {

                modalAlert(
                    'default',
                    t_js('available_languages'),
                    '<div class="row listLang"></div>',
                    ''
                );

                msg.forEach(function(item) {
                    $('#modalAlert').find('.listLang').append(
                        '<div class="col-md-4">' +
                            '<a href="javascript:void(0);" data-code="'+item.code+'">'+item.label+'</a>' +
                        '</div>'
                    )
                });

            }
        });
    });


    $('.changeMode a').on('click',function () {

        var lang = $(this).data('device');

        $.ajax({
            url: '/device-user/change-device?device='+lang,
            beforeSend: function () {
                $('#loading_box').show();
            },
            complete: function () {
                $('#loading_box').hide();
            },
            success: function(msg){
                window.location.reload(true);
            }
        });
    });

    $(document).on('click','.transitionInDefaultBrowser',function (e) {
        e.preventDefault();
        sendHash({action:'transitionInDefaultBrowser',link:$(this).attr('href')});
        setLogger(JSON.stringify({action:'transitionInDefaultBrowser',link:$(this).attr('href')}));
    });



    $('.clearLogger').on('click',function () {
        $('.logger').text('');
    });


    // block click for right button mouse
    $(this).bind("contextmenu", function(e) {
        e.preventDefault();
    });

});

function loadContent(link) {

    //TODO:KAA temp block old app
    sendHash({action:'get-version-app',callbackFunction:'checkVersionApp'});
    setLogger(JSON.stringify({action:'get-version-app',callbackFunction:'checkVersionApp'}));

    var oldLink = getCookie('activePage');

    $.ajax({
        url: link,
        beforeSend: function () {
            $('#loading_box').show();

            stopVegaOrVoll(oldLink);
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            $('.content-wrapper .content').html(msg);

            setActiveMenu(link);

            pageUp();
        }
    });
}

function pageUp() {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
}

function setActiveMenu(link) {
    $('.main-sidebar .sidebar-menu').find('li').each(function () {
        $(this).removeClass('active');
        if($(this).find('a').data('href') == link){
            $(this).addClass('active');
        }
    })
}

function stopVegaOrVoll(oldLink){
    switch (oldLink) {
        case '/electropuncture/vega-selector':
            setLogger(JSON.stringify({action:'stop', method:'vega-test'}));
            sendHash({action:'stop', method:'vega-test'});
            break;
        case '/electropuncture/vega-selector-individual':
            setLogger(JSON.stringify({action:'stop', method:'vega-test'}));
            sendHash({action:'stop', method:'vega-test'});
            break;
        case '/electropuncture/vega-selection-frequencies':
            setLogger(JSON.stringify({action:'stop', method:'selection-frequency'}));
            sendHash({action:'stop', method:'selection-frequency'});
            break;
        case '/electropuncture/voll':
            setLogger(JSON.stringify({action:'stop', method:'voll-test'}));
            sendHash({action:'stop', method:'voll-test'});
            break;
    }
}

function setDiagnosticPatient(patientID) {
    $.ajax({
        url: '/patients/set-diagnostic-patient?id='+patientID,
        beforeSend: function () {
            $('#loading_box').show();
        },
        complete: function () {
            $('#loading_box').hide();
        },
        success: function(msg){
            if(msg.error == ''){
                $('.use-patient .name-patient').text(msg.patientName).data({'patient-id':patientID});
                $('meta[name="patientId"]').attr({content:patientID});
                getPatientGender();

                if($('.content').find('table').is("#listSeanceExpress")){
                    getTestForExpressTest(patientID);
                } else if($('.content').find('table').is("#listSeanceRegistry")){
                    registry.getTestForRegistry(patientID);
                } else if($('.content').find('table').is("#listSeanceDrugSelection")){
                    getTestForDrugSelection(patientID);
                }

                loadContent(getCookie('activePage'))
            } else {

            }
        }
    });
}

function getPatientGender(){
    $.ajax({
        url: '/patients/get-patient',
        success: function(msg){
            if(msg.state === 'success') {
                $('meta[name="patientGender"]').attr({content: msg.Patient.gender});
                patientGender = msg.Patient.gender;
            } else {
                modalAlert(msg.state,msg.title,msg.content,msg.footer);
            }
        }
    });
}

$('.content').on('click','.pageInDeveloping',function () {
   modalAlert('danger',t_js('attention'),'<p>'+t_js('page_in_development')+'</p>','');
});

// function for animate progress
function loadProgress() {
    $('.bl-progress .progress-bar').each(function () {
        var progresBl = $(this);
        var progressValue = progresBl.data('progress-value');

        progresBl.animate({width:progressValue+'%'},{duration: 1000});


        if(progresBl.hasClass('progressValue')){
            $(this).closest('.bl-progress').find('.badgeValue').prop('Counter',0).animate({
                Counter: progressValue
            }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        }


        if(progresBl.hasClass('progressFall')){

            var progressRealValue = progresBl.data('progress-realvalue');

            $(this).closest('.bl-progress').find('.badgeFall').prop('Counter',0).animate({
                Counter: progressRealValue
            }, {
                duration: 1000,
                easing: 'swing',
                step: function (now) {
                    $(this).text(Math.ceil(now));
                }
            });
        }
    });
}


function viewedNews(number) {
    var labelViewedNews = $('.main-sidebar').find('.labelViewedNews');
    var OldNumber = parseInt(labelViewedNews.text());

    labelViewedNews.text(OldNumber-number);

    $('.main-header').find('.labelHeadViewedNews').text(OldNumber-number);
}

function modalAlert(state,title,content,footer) {

    var flShow = 1;
    if($("#modalAlert").hasClass('in') == true){
        flShow = 0;
    }

    $('#modalAlert').removeClass().addClass('modal modal-'+state+' fade');
    $('#modalAlert').find('.modal-title').text(title);
    $('#modalAlert').find('.modal-body').html(content);
    if(footer!=''){
        $('#modalAlert').find('.modal-footer').html(footer);
    } else {
        $('#modalAlert').find('.modal-footer').html('<button type="button" class="btn btn-outline pull-left" data-dismiss="modal">'+t_js('close')+'</button>');
    }


    if(flShow == 1){
        $("#modalAlert").modal("show");
    } else {
        $("#modalAlert").addClass('in');
    }


}

function modalAlertHide() {
    $("#modalAlert").modal("hide");
}

function sendHash(hash){
    hash = JSON.stringify(hash);
    if(history.pushState)
        history.pushState(null, null, '#'+hash);
    else
        location.hash = '#'+hash;
}


function setLogger(message) {
    if($("div").is(".logger")){
        $('.logger').prepend(message + "<br>");
    }
}

function getDateTimeNow(){
    var date = new Date();

    return date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2) + ' ' + ('0' + date.getHours()).slice(-2) + ':' + ('0' + date.getMinutes()).slice(-2) + ':' + ('0' + date.getSeconds()).slice(-2);
}


function t_js(key) {
    var value;

    if(typeof tData[key] == "undefined"){
        value = key;

        // $.ajax({
        //     url: '/translation/get-translation',
        //     type: 'POST',
        //     data: {key:key},
        //     async: false,
        //     success: function (msg) {
        //         tData = msg;
        //
        //         if(tData[key] === null){
        //             value = key;
        //         } else {
        //             value = tData[key];
        //         }
        //     }
        // });
    } else if(tData[key] == null){
        value = key;
    } else {
        value = tData[key];
    }

    return value;
}

/**
 *
 * disabled btn, time in sec
 * @param btn
 * @param timeDisable
 */
function btnDisabled(btn,timeDisable = 5){
    btn.attr({disabled:true});
    setTimeout(function () {
        btn.removeAttr('disabled');
    }, (timeDisable * 1000));
}

/**
 * read cookie by name
 *
 * @param name
 * @returns {any}
 */
function getCookie(name) {

    var r = document.cookie.match("(^|;) ?" + name + "=([^;]*)(;|$)");
    var result = '';
    if (r) {
        result = PHPUnserialize.unserialize(decodeURIComponent(r[2]).substr(64));
        result = result[1];
    }
    return result;
}

/**
 * version compare
 * < 0, if a < b
 * > 0, if a > b
 * = 0, if a = b
 *
 * @param a
 * @param b
 * @returns {*}
 */
function versionСompare (a, b) {
    var i, diff;
    var regExStrip0 = /(\.0+)+$/;
    var segmentsA = a.replace(regExStrip0, '').split('.');
    var segmentsB = b.replace(regExStrip0, '').split('.');
    var l = Math.min(segmentsA.length, segmentsB.length);

    for (i = 0; i < l; i++) {
        diff = parseInt(segmentsA[i], 10) - parseInt(segmentsB[i], 10);
        if (diff) {
            return diff;
        }
    }
    return segmentsA.length - segmentsB.length;
}

function formattingTime(time){
    time = parseInt(time);

    let formattingTime = {
        h:0,
        m:0,
        s:0,
        total:''
    };

    if(time >= 3600){
        formattingTime.h = parseInt(time/3600);

        time -= formattingTime.h * 3600;
    }

    if(time >= 60){
        formattingTime.m = parseInt(time/60);

        time -= formattingTime.m * 60;
    }

    if(time > 0){
        formattingTime.s = time;
    }

    formattingTime.total =
        (formattingTime.h > 0 ? pad(formattingTime.h,2) + ':' : '') +
        (formattingTime.m > 0 ? pad(formattingTime.m,2) + ':' : '00:') +
        (formattingTime.s > 0 ? pad(formattingTime.s,2) + '' : '00');

    return formattingTime;

}

function pad(num, size) {
    let s = num+"";
    while (s.length < size) s = "0" + s;
    return s;
}

function arrayMove(arr, fromIndex, toIndex) {
    let element = arr[fromIndex];
    arr.splice(fromIndex, 1);
    arr.splice(toIndex, 0, element);
}