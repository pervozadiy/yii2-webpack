const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');

module.exports = {
    resolve: {
        modules: [
            'node_modules',
            'vendor/yiisoft/yii2/assets',
        ],
        extensions: ['.ts', '.js'],
        alias: {
            resources: path.resolve(__dirname, 'resources'),
            vendor: path.resolve(__dirname, 'vendor')
        },
    },
    output: {
        path: path.resolve(__dirname, 'web/dist')
    },
    context: path.resolve(__dirname, 'resources'),
    module: {
        rules: [{
            test: require.resolve('jquery'),
            use: [{
                loader: 'expose-loader',
                options: 'jQuery'
            }, {
                loader: 'expose-loader',
                options: '$'
            }]
        }]
    },
    externals: {
        window: 'window',
        document: 'document'
    },
    plugins: [
        new CleanWebpackPlugin(),
    ]
};