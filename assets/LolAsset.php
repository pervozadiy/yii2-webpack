<?php

namespace app\assets;

use yii\web\AssetBundle;

class LolAsset extends AssetBundle
{
    public $basePath = '@webroot/dist';

    public $baseUrl = '@web/dist';

    public $js = [
        'js/lol.entry.js'
    ];
}
