<?php

namespace app\assets;

use yii\web\AssetBundle;

class WebpackAsset extends AssetBundle
{
    public $basePath = '@webroot/dist';

    public $baseUrl = '@web/dist';

    public $js = [
//        'js/manifest.js',
//        'js/vendor.js',
        'js/app.js',
    ];
}
