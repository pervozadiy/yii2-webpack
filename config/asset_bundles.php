<?php

use app\assets\LolAsset;

return [
    \yii\web\JqueryAsset::class => [
        'depends' => [
            \app\assets\WebpackAsset::class,
        ],
    ],
    \yii\web\YiiAsset::class => [],
    \yii\validators\ValidationAsset::class => [],
    \yii\widgets\ActiveFormAsset::class => [],
    \yii\grid\GridViewAsset::class => [],
    \yii\captcha\CaptchaAsset::class => [],
    \yii\widgets\PjaxAsset::class => [],
    LolAsset::class => [],
];
