<?php
/**
 * Configuration file for the "yii asset" console command.
 */

// In the console environment, some path aliases may not exist. Please define these:
 Yii::setAlias('@webroot', __DIR__ . '/../web');
 Yii::setAlias('@web', '../web');

$bundles = include 'asset_bundles.php';

return [
    // Adjust command/callback for JavaScript files compressing:
    'jsCompressor' => 'node_modules/.bin/uglifyjs {from} -o {to} -c',
//    'jsCompressor' => 'mv {from} {to}',
    // Adjust command/callback for CSS files compressing:
    'cssCompressor' => 'mv {from} {to}',
    // Whether to delete asset source after compression:
    'deleteSource' => false,
    'bundles' => array_keys($bundles),
    // Specify output bundles with groups:
    'targets' => [
        'all' => [
            'class' => \yii\web\AssetBundle::class,
            'basePath' => '@webroot/dist',
            'baseUrl' => '@web/dist',
            'js' => 'app-compressed.js',
            'css' => 'app-compressed.css',
        ],
    ],
    'assetManager' => [
        'basePath' => '@webroot/assets',
        'baseUrl' => '@web/assets',
        'bundles' => $bundles,
    ],
];