const fs = require('fs');
const path = require('path');

class Entry {
    constructor(dir = '') {
        this.dir = dir;
        this.entryRegexp = /^\w+\.(js|ts)/;
    }

    /**
     * @param {String} dir
     * @param {String[]} filelist
     * @returns {*|Array}
     */
    walkSync(dir, filelist = []) {
        let files = fs.readdirSync(dir);
        files.forEach(file => {
            if (fs.statSync(path.join(dir, file)).isDirectory()) {
                filelist = this.walkSync(path.join(dir, file), filelist);
            } else {
                file.match(this.entryRegexp) !== null && filelist.push(path.join(dir, file));
            }
        });

        return filelist;
    };

    find() {
        return this.walkSync(this.dir);
    }
}

module.exports = Entry;